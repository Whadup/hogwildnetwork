#pragma once
#include <httplib.h>

// httplib::Client hyperboardLogger("localhost", 5000);
httplib::Client hyperboardLogger("192.168.128.51", 5000);

std::string registerLogger(std::string name,std::string metric)
{
	std::string bodystring=std::string("json=%7B%22hyperparameters%22%3A%7B%22name%22%3A%22")+name+std::string("%22%7D%2C%22metric%22%3A%22")+metric+std::string("%22%2C%22overwrite%22%3Atrue%7D");//
	auto res = hyperboardLogger.get("register",bodystring);
	return std::string(res->body);
}

void logValue(std::string& name,int i,double d)
{
	std::string datapoint="json=%7B%22name%22%3A+%22";
	datapoint+=name;
	datapoint+="%22%2C+%22value%22%3A+";
		
	datapoint+=std::to_string(d);
	datapoint+="%2C+%22index%22%3A+";
	datapoint+=std::to_string(i);
	datapoint+="3%7D";
		// std::cout << datapoint << std::endl;
	auto res = hyperboardLogger.get("append",datapoint);
}
