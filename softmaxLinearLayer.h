#pragma once
// #ifndef _LINEARLAYER_
// #define _LINEARLAYER_
#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
#include <random>
using Eigen::Matrix;

template<int inputDimension,int hiddenDimension>
class SoftmaxLinearLayer: public ComputeUnit
{
public:
	MatrixXd* W = 0;
	MatrixXd* gW = 0;
	Matrix<double,hiddenDimension,1>* b = 0;
	Matrix<double,hiddenDimension,1>* gb = 0;
	SoftmaxLinearLayer();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();

	int sample(double d)
	{
		double s = 0;
		for(int i=0;i<hiddenDimension;i++)
		{
			if(d < s + outputs[0](i,0))
				return i;
			s+=outputs[0](i,0);
		}
		return hiddenDimension-1;
	}

	int map()
	{
		int max = 0;
		for(int i=1;i<hiddenDimension;i++)
			if(outputs[0](i,0) > outputs[0](max,0))
				max = i;
		return max;
	}
	
	int min()
	{
		int min = 0;
		for(int i=1;i<hiddenDimension;i++)
			if(outputs[0](i,0) < outputs[0](min,0))
				min = i;
		return min;
	}

	SoftmaxLinearLayer<inputDimension,hiddenDimension>* deepCopy()
	{
		SoftmaxLinearLayer<inputDimension,hiddenDimension>* l = new SoftmaxLinearLayer<inputDimension,hiddenDimension>();
		l->gb = new Matrix<double,hiddenDimension,1>;
		l->gb->setZero();
		l->gW = new MatrixXd(hiddenDimension,inputDimension);
		l->gW->setZero();
		l->W = new MatrixXd(hiddenDimension,inputDimension);
		*l->W = *W;
		l->b = new Matrix<double,hiddenDimension,1>;
		*l->b = *b;
		return l;
	}

};


template<int inputDimension,int hiddenDimension>
SoftmaxLinearLayer<inputDimension,hiddenDimension>::SoftmaxLinearLayer()  : ComputeUnit(1,0,1)
{
	outputs[0]=Matrix<double,hiddenDimension,1>::Zero();
	jacobians[0]=Matrix<double,inputDimension,1>::Zero();	
}

template<int inputDimension,int hiddenDimension>
void SoftmaxLinearLayer<inputDimension,hiddenDimension>::init()
{
	// if(W!=0) delete W;
	W = new MatrixXd(hiddenDimension,inputDimension);//::Zero();
	gW = new MatrixXd(hiddenDimension,inputDimension);//::Zero();
	b = new Matrix<double,hiddenDimension,1>;//::Zero();
	gb = new Matrix<double,hiddenDimension,1>;//::Zero();
	gW->setZero();
	gb->setZero();
	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<double> initializer(0,0.1);
	for(int i = 0;i<hiddenDimension;i++)
	{	for(int j=0;j<inputDimension;j++)
			(*W)(i,j) = initializer(gen);
		(*b)(i,0)=0;
	}
	*W = (W->array()/inputDimension).matrix();
	for(int i=0;i<hiddenDimension/2;i++)
	{
		if(i==0)
		{
			(*b)(rand()%hiddenDimension,0)=2;
		}
		else
		{
			int j = rand()%hiddenDimension;
			W->row(j).setZero();
			(*b)(j,0)=-2;
		}
	}
	
	
	// std::cout << "LINEAR LAYER W" << std::endl; 
	//std::cout << *W << std::endl;
	// std::cout << std::endl;
}

template<int inputDimension,int hiddenDimension>
ComputeUnit* SoftmaxLinearLayer<inputDimension,hiddenDimension>::duplicate()
{
	SoftmaxLinearLayer<inputDimension,hiddenDimension>* l = new SoftmaxLinearLayer();
	l->W = W;
	l->b = b;
	l->gW = gW;
	l->gb = gb;
	return (ComputeUnit*)l;
}

template<int inputDimension,int dimension>
void SoftmaxLinearLayer<inputDimension,dimension>::forward()
{
	outputs[0] = (*W) * (*inputs[0]) + *b;
	double maxTerm = outputs[0](0,0);
	for(int i=0; i< dimension; i++)
		if(outputs[0](i,0)>maxTerm)
			maxTerm=outputs[0](i,0);
	double logSum = 0;
	for(int i=0;i<dimension;i++)
		logSum += exp(outputs[0](i,0) - maxTerm);
	logSum = maxTerm + log(logSum);
	outputs[0] = (outputs[0] - Matrix<double,dimension,1>::Constant(logSum)).array().exp().matrix();
	// std::cout << outputs[0].transpose() << std::endl;
}

template<int inputDimension,int hiddenDimension>
void SoftmaxLinearLayer<inputDimension,hiddenDimension>::backward()
{
	MatrixXd* jac = &successorDeep->jacobians[0];
	MatrixXd g = -1.0 * (outputs[0]*outputs[0].transpose());
	for(int i=0;i<hiddenDimension;i++)
		g(i,i)=outputs[0](i,0)*(1-outputs[0](i,0));
	MatrixXd jacc = g * (*jac);
	jacobians[0] = this->W->transpose() * jacc;

	for(int j=0;j<hiddenDimension;j++)
	{		
		gW->row(j)+= (jacc(j,0)) * inputs[0]->transpose();
	}
	*gb += jacc;


	// std::cout << W << std::endl;
}

template<int inputDimension,int hiddenDimension>
void SoftmaxLinearLayer<inputDimension,hiddenDimension>::gradientStep(double alpha)
{
	*W -= alpha * *gW;
	*b -= alpha * *gb;
	gb->setZero();
	gW->setZero();
}

	
