#pragma once
// #ifndef _LINEARLAYER_
// #define _LINEARLAYER_
#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
#include <random>
#include "dataInterfaces.h"
using Eigen::Matrix;

template<int vocabularySize,int embeddingDimension,bool average>
class EmbedAndReduceLayer: public ComputeUnit, public SequenceInput
{
public:
	
	MatrixXd* W;//(embeddingDimension,vocabularySize);

	EmbedAndReduceLayer();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
};



template<int vocabularySize,int embeddingDimension,bool average>
EmbedAndReduceLayer<vocabularySize,embeddingDimension,average>::EmbedAndReduceLayer() : ComputeUnit(0,0,1), SequenceInput()
{
	outputs[0]=MatrixXd(embeddingDimension,1);
}

template<int vocabularySize,int embeddingDimension,bool average>
void EmbedAndReduceLayer<vocabularySize,embeddingDimension,average>::init()
{
	W = new MatrixXd(embeddingDimension,vocabularySize);
	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<double> initializer(0,1);
	for(int i = 0;i<embeddingDimension;i++)
		for(int j=0;j<vocabularySize;j++)
			(*W)(i,j) = initializer(gen)/(embeddingDimension/64);
	// W->setZero(); // OMG DONT DO THAT
	// gW = new MatrixXd(embeddingDimension,vocabularySize);
	// gW->setZero();
	// updates = new std::vector<int>;
	// mtx = new std::mutex;
}

template<int vocabularySize,int embeddingDimension,bool average>
ComputeUnit* EmbedAndReduceLayer<vocabularySize,embeddingDimension,average>::duplicate()
{
	EmbedAndReduceLayer<vocabularySize,embeddingDimension,average>* l = new EmbedAndReduceLayer();
	l->W = W;
	return (ComputeUnit*)l;
}

template<int vocabularySize,int embeddingDimension,bool average>
void EmbedAndReduceLayer<vocabularySize,embeddingDimension,average>::forward()
{
	// std::cout << input[0] <<std::endl;
	outputs[0]=this->W->col(input[0]);
	if(l>1)
		for(int i = 1;i < l;i++)
			outputs[0]+=this->W->col(input[i]);
	if(average)
		outputs[0]/=1.0*l;
}

template<int vocabularySize,int embeddingDimension,bool average>
void EmbedAndReduceLayer<vocabularySize,embeddingDimension,average>::backward()
{
	// GIBT ES NICHT, wobei eigentlich ja schon wenn man den input als 1-hot-code auffasst...
	// Braucht man jedenfalls normalerweise nicht.
	
	//MatrixXd* jac = &successors[0]->jacobians[0];
	// jacobians[0] = this->W.transpose() * this->successors[0]->jacobians[0];
	// std::cout << "linear layer "<<this << " " << jacobians[0].transpose()<<std::endl;
}


template<int vocabularySize,int embeddingDimension,bool average>
void EmbedAndReduceLayer<vocabularySize,embeddingDimension,average>::gradientStep(double alpha)
{
	MatrixXd* jac = &(successorDeep->jacobians[0]);
	// std::cout << W->rows() << " " << W->cols() << std::endl;
	// std::cout << jac->rows() << " " << jac->cols() << std::endl;
	if(average)
		*jac/=1.0*l;
	for(int i = 0;i<l;i++)
		W->col(input[i])-=alpha * *jac; 
	//Matrix<double,embeddingDimension,1>::Constant(alpha*delta);
	// for(int j=0;j<embeddingDimension;j++)
	// 	for(int i = 0;i<l;i++)
	// 		(*W)(j,input[i])-=alpha * delta;
}
