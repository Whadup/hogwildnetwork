#pragma once

template<typename T>
class DataSource
{
	static_assert(std::is_base_of<Example, T>::value, "T must derive from Example");
public:
		virtual int nextItem(int size,T** a)=0;
};
