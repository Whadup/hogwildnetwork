#pragma once

#include <iostream>
#include <Eigen/Dense>
using Eigen::MatrixXd;


//template<int ninputs, int noutputs>
class ComputeUnit
{
public:
	unsigned int const ninputs;
	unsigned int const nrecurrent;
	unsigned int const noutputs;
	unsigned int connectedToSlot = 0;
	ComputeUnit* successorDeep;
	ComputeUnit* successorTime;
	MatrixXd** inputs;
	MatrixXd* outputs;
	MatrixXd* jacobians;
	ComputeUnit(unsigned int const ninputs, unsigned int const nrecurrent, unsigned int const noutputs);
	virtual ~ComputeUnit();
	virtual void forward()=0;
	virtual void backward()=0;
	virtual void gradientStep(double size)=0;
	virtual ComputeUnit* duplicate()=0;
	virtual void init(){}
};


ComputeUnit::ComputeUnit(unsigned int const ninputs, unsigned int const nrecurrent, unsigned int const noutputs) : ninputs(ninputs),noutputs(noutputs),nrecurrent(nrecurrent)
{
	successorDeep = 0;
	successorTime = 0;
	outputs = new MatrixXd[noutputs+nrecurrent]();
	jacobians = new MatrixXd[ninputs+nrecurrent];
	inputs = new MatrixXd*[ninputs+nrecurrent];
}
ComputeUnit::~ComputeUnit()
{
	delete[] outputs;
	delete[] jacobians;
	delete[] inputs;
}


void connectDepth(ComputeUnit& a, ComputeUnit& b)
{
	a.successorDeep=&b;
	for(int i=0;i<b.ninputs;i++)
		b.inputs[i]=&a.outputs[i];

}

void connectDepth(ComputeUnit* a, ComputeUnit* b)
{
	a->successorDeep=b;
	a->connectedToSlot = 0;
	for(int i=0;i<b->ninputs;i++)
		b->inputs[i]=&a->outputs[i];
}

void connectDepth11(ComputeUnit* a, ComputeUnit* b)
{
	a->successorDeep=b;
	a->connectedToSlot = 0;
	// for(int i=0;i<b->ninputs;i++)
		b->inputs[0]=&a->outputs[0];
}

void connectDepth21(ComputeUnit* a, ComputeUnit* b)
{
	a->successorDeep=b;
	// for(int i=0;i<b->ninputs;i++)
		b->inputs[0]=&a->outputs[1];
}

void connectDepth12(ComputeUnit* a, ComputeUnit* b)
{
	a->successorDeep=b;
	a->connectedToSlot = 1;
	// for(int i=0;i<b->ninputs;i++)
		b->inputs[1]=&a->outputs[0];
}

void connectTime(ComputeUnit* a, ComputeUnit* b)
{
	a->successorTime=b;
	for(int i=0;i<b->nrecurrent;i++)
		b->inputs[b->ninputs + i]=&a->outputs[a->noutputs + i];
}