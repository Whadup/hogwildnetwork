#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;

#include "computeUnit.h" 
template <int dimension>
class DummyJacobian: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	DummyJacobian();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int dimension>
DummyJacobian<dimension>::DummyJacobian() : ComputeUnit(1,1,0) 
{
	jacobians[0] = Matrix<double,dimension,1>::Zero();
	jacobians[1] = Matrix<double,dimension,1>::Zero();
}

template<int dimension>
void DummyJacobian<dimension>::init() 
{
}
template<int dimension>
ComputeUnit* DummyJacobian<dimension>::duplicate() 
{
	DummyJacobian<dimension>* l = new DummyJacobian<dimension>();
	return (ComputeUnit*) l;
}
template<int dimension>
void DummyJacobian<dimension>::forward()
{
}
template<int dimension>
void DummyJacobian<dimension>::backward()
{
}
template<int dimension>
void DummyJacobian<dimension>::gradientStep(double alpha)
{
}
