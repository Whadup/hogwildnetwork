#include <iostream> 
#include <Eigen/Dense>
#include <cmath>
#include <random>
using Eigen::Matrix;
using Eigen::PermutationMatrix;

template<int dimension,int redundancy>
class Memory
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,dimension,redundancy> trace;
	Matrix<double,dimension,dimension> complex1;
	Matrix<double,dimension,dimension> complex2;
	Matrix<double,dimension,dimension> complex3;
	std::vector<PermutationMatrix<dimension,dimension>> permutations;
	Memory()
	{
		trace = Matrix<double,dimension,redundancy>::Zero();
		for(int i=0;i<redundancy;i++)
		{
			PermutationMatrix<dimension,dimension> perm;
			perm.setIdentity();
			std::random_shuffle(perm.indices().data(), perm.indices().data()+perm.indices().size());
			permutations.push_back(perm);
			//std::cout << perm.toDenseMatrix() << std::endl;
		}
		complex1 = Matrix<double,dimension,dimension>::Zero();
		complex2 = Matrix<double,dimension,dimension>::Zero();
		complex3 = Matrix<double,dimension,dimension>::Zero();
		complex1.topLeftCorner(dimension/2,dimension/2).setIdentity();
		complex1.bottomLeftCorner(dimension/2,dimension/2).setIdentity();
		complex2.bottomRightCorner(dimension/2,dimension/2).setIdentity();
		complex2.topRightCorner(dimension/2,dimension/2).setIdentity();
		complex2.topRightCorner(dimension/2,dimension/2) = -complex2.topRightCorner(dimension/2,dimension/2);
		complex3.topRightCorner(dimension/2,dimension/2).setIdentity();
		complex3.bottomLeftCorner(dimension/2,dimension/2).setIdentity();
	}

	void add(Matrix<double,dimension,1>& k,Matrix<double,dimension,1>&v){
		for(int i=0;i<redundancy;i++)
		{
			Matrix<double,dimension,1> key = permutations[i] * k;
			trace.col(i) += ((complex1 * key).array() * v.array()).matrix() + ((complex2 * key).array() * (complex3 * v).array()).matrix();
		}	
	}

	Matrix<double,dimension,1> retrieve(Matrix<double,dimension,1>& k)
	{
		Matrix<double,dimension,1> ret = Matrix<double,dimension,1>::Zero();
		for(int i=0;i<redundancy;i++)
		{
			Matrix<double,dimension,1> key = permutations[i] * k;
			ret += ((complex1 * key).array() * trace.col(i).array()).matrix() + ((-complex2 * key).array() * (complex3 * trace.col(i)).array()).matrix();
		}
		return ret/redundancy;
	}
};

int main()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<double> initializer(0,1);
	Memory<64,100> memory;
	std::vector<Matrix<double,64,1>> ks;
	std::vector<Matrix<double,64,1>> vs;

	int n = 2;

	for(int i=0;i<n;i++)
	{
		Matrix<double,64,1> k = Matrix<double,64,1>::Zero();
		Matrix<double,64,1> v = Matrix<double,64,1>::Zero();
		for(int j=0;j<32;j++)
		{
			k(j,0) = initializer(gen);
			k(32+j,0) = sqrt(1-k(j,0)*k(j,0));
			v(j,0) = initializer(gen);
			v(32+j,0) = sqrt(1-v(j,0)*v(j,0));
		}
		ks.push_back(k);
		vs.push_back(v);
		// std::cout << k.transpose() << std::endl;
		// std::cout << v.transpose() << std::endl;
		memory.add(k,v);
	}
	for(int i=0;i<n;i++)
	{
		Matrix<double,64,1> ret = memory.retrieve(ks[i]);
		// std::cout << ks[i].transpose() << std::endl;
		// std::cout << ret.transpose() << std::endl;
		// std::cout << vs[i].transpose() << std::endl;
		std::cout << (vs[i]-ret).norm() << std::endl;

	}
	std::cout << "done" << std::endl;
	return 0;
}

