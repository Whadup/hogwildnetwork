#pragma once
#include "dataSource.h"
#include "dataInterfaces.h"
#include <vector>
#include <mutex>

template<typename T>
class MemoryDataSource : public DataSource<T>
{
public:
	const std::vector<T*> data;
	std::mutex lock;
	unsigned int current;
	unsigned int max = 0;
	MemoryDataSource(std::vector<T*>& d,unsigned int max);
	void setNumberOfEpoches(unsigned int m);
	virtual int nextItem(int size,T** a);
};


template<typename T>
MemoryDataSource<T>::MemoryDataSource(std::vector<T*>& d,unsigned int max) : data(d)
{
	current = 0;
	max = max;
}

template<typename T>
void MemoryDataSource<T>::setNumberOfEpoches(unsigned int m)
{
	max = data.size()*m;
	current = 0;
	std::cout << "Number of SGD Steps: " << max << std::endl;
}


template<typename T>
int MemoryDataSource<T>::nextItem(int n, T** ret)
{
	if(current>=max)
		return 0;
	int c = 0;
	lock.lock();
	int start = current;
	current+=n;
	lock.unlock();
	for(int i=0;i<n && start<max;i++)
	{
		ret[i] = data[start % data.size()];
		start++;
		c++; 
	}
	return c;
}