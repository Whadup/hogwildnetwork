#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<double> initializer(0,0.1);

#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;

#include "computeUnit.h" 
template <int inputDimension, int outputDimension>
class LSTM: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,outputDimension,1> i;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> c;
	Matrix<double,outputDimension,1> ht;
	Matrix<double,outputDimension,1> ct;
	Matrix<double,outputDimension,1> tmp2;
	Matrix<double,outputDimension,1> out;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,outputDimension,1> u;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> h;
	Matrix<double,outputDimension,1> tmp1;
	Matrix<double,outputDimension,1> f;
	Matrix<double,outputDimension,1> o;
	Matrix<double,outputDimension,1> tmp3;
	Matrix<double,outputDimension,outputDimension>* Uih = 0;
	Matrix<double,outputDimension,outputDimension>* gUih = 0;
	Matrix<double,outputDimension,inputDimension>* Uox = 0;
	Matrix<double,outputDimension,inputDimension>* gUox = 0;
	Matrix<double,outputDimension,outputDimension>* Uoh = 0;
	Matrix<double,outputDimension,outputDimension>* gUoh = 0;
	Matrix<double,outputDimension,1>* bf = 0;
	Matrix<double,outputDimension,1>* gbf = 0;
	Matrix<double,outputDimension,outputDimension>* Ufc = 0;
	Matrix<double,outputDimension,outputDimension>* gUfc = 0;
	Matrix<double,outputDimension,1>* bo = 0;
	Matrix<double,outputDimension,1>* gbo = 0;
	Matrix<double,outputDimension,1>* bi = 0;
	Matrix<double,outputDimension,1>* gbi = 0;
	Matrix<double,outputDimension,1>* bu = 0;
	Matrix<double,outputDimension,1>* gbu = 0;
	Matrix<double,outputDimension,inputDimension>* Uix = 0;
	Matrix<double,outputDimension,inputDimension>* gUix = 0;
	Matrix<double,outputDimension,inputDimension>* Ufx = 0;
	Matrix<double,outputDimension,inputDimension>* gUfx = 0;
	Matrix<double,outputDimension,outputDimension>* Ufh = 0;
	Matrix<double,outputDimension,outputDimension>* gUfh = 0;
	Matrix<double,outputDimension,inputDimension>* Uux = 0;
	Matrix<double,outputDimension,inputDimension>* gUux = 0;
	Matrix<double,outputDimension,outputDimension>* Uuc = 0;
	Matrix<double,outputDimension,outputDimension>* gUuc = 0;
	Matrix<double,outputDimension,outputDimension>* Uoct = 0;
	Matrix<double,outputDimension,outputDimension>* gUoct = 0;
	Matrix<double,outputDimension,outputDimension>* Uic = 0;
	Matrix<double,outputDimension,outputDimension>* gUic = 0;
	LSTM();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension, int outputDimension>
LSTM<inputDimension, outputDimension>::LSTM() : ComputeUnit(1,2,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Zero();
	outputs[1] = Matrix<double,outputDimension,1>::Zero();
	outputs[2] = Matrix<double,outputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,outputDimension,1>::Zero();
	jacobians[2] = Matrix<double,outputDimension,1>::Zero();
}

template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::init() 
{
	Uih = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uih);
	gUih = new Matrix<double,outputDimension,outputDimension>();
	gUih->setZero();
	Uox = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uox);
	gUox = new Matrix<double,outputDimension,inputDimension>();
	gUox->setZero();
	Uoh = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uoh);
	gUoh = new Matrix<double,outputDimension,outputDimension>();
	gUoh->setZero();
	bf = new Matrix<double,outputDimension,1>();
	zeroInitialization(bf);
	gbf = new Matrix<double,outputDimension,1>();
	gbf->setZero();
	Ufc = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Ufc);
	gUfc = new Matrix<double,outputDimension,outputDimension>();
	gUfc->setZero();
	bo = new Matrix<double,outputDimension,1>();
	zeroInitialization(bo);
	gbo = new Matrix<double,outputDimension,1>();
	gbo->setZero();
	bi = new Matrix<double,outputDimension,1>();
	zeroInitialization(bi);
	gbi = new Matrix<double,outputDimension,1>();
	gbi->setZero();
	bu = new Matrix<double,outputDimension,1>();
	zeroInitialization(bu);
	gbu = new Matrix<double,outputDimension,1>();
	gbu->setZero();
	Uix = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uix);
	gUix = new Matrix<double,outputDimension,inputDimension>();
	gUix->setZero();
	Ufx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Ufx);
	gUfx = new Matrix<double,outputDimension,inputDimension>();
	gUfx->setZero();
	Ufh = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Ufh);
	gUfh = new Matrix<double,outputDimension,outputDimension>();
	gUfh->setZero();
	Uux = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uux);
	gUux = new Matrix<double,outputDimension,inputDimension>();
	gUux->setZero();
	Uuc = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uuc);
	gUuc = new Matrix<double,outputDimension,outputDimension>();
	gUuc->setZero();
	Uoct = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uoct);
	gUoct = new Matrix<double,outputDimension,outputDimension>();
	gUoct->setZero();
	Uic = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uic);
	gUic = new Matrix<double,outputDimension,outputDimension>();
	gUic->setZero();
}
template<int inputDimension, int outputDimension>
ComputeUnit* LSTM<inputDimension, outputDimension>::duplicate() 
{
	LSTM<inputDimension, outputDimension>* l = new LSTM<inputDimension, outputDimension>();
	l->Uih = Uih;
	l->gUih = gUih;
	l->Uox = Uox;
	l->gUox = gUox;
	l->Uoh = Uoh;
	l->gUoh = gUoh;
	l->bf = bf;
	l->gbf = gbf;
	l->Ufc = Ufc;
	l->gUfc = gUfc;
	l->bo = bo;
	l->gbo = gbo;
	l->bi = bi;
	l->gbi = gbi;
	l->bu = bu;
	l->gbu = gbu;
	l->Uix = Uix;
	l->gUix = gUix;
	l->Ufx = Ufx;
	l->gUfx = gUfx;
	l->Ufh = Ufh;
	l->gUfh = gUfh;
	l->Uux = Uux;
	l->gUux = gUux;
	l->Uuc = Uuc;
	l->gUuc = gUuc;
	l->Uoct = Uoct;
	l->gUoct = gUoct;
	l->Uic = Uic;
	l->gUic = gUic;
	return (ComputeUnit*) l;
}
template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::forward()
{
	x = *inputs[0];
	c = *inputs[1];
	h = *inputs[2];
	i = (1.0 + (-(((*Uix) * x) + ((*Uic) * c) + ((*Uih) * h) + (*bi))).array().exp()).inverse().matrix();;
	u = (((*Uux) * x) + ((*Uuc) * c) + (*bu)).array().tanh().matrix();
	f = (1.0 + (-(((*Ufx) * x) + ((*Ufc) * c) + ((*Ufh) * h) + (*bf))).array().exp()).inverse().matrix();;
	tmp2 = (i.array() * u.array()).matrix();
	tmp1 = (f.array() * c.array()).matrix();
	ct = (tmp1 + tmp2);
	outputs[1] = ct;
	o = (1.0 + (-(((*Uox) * x) + ((*Uoct) * ct) + ((*Uoh) * h) + (*bo))).array().exp()).inverse().matrix();;
	tmp3 = (ct).array().tanh().matrix();
	ht = (tmp3.array() * o.array()).matrix();
	outputs[2] = ht;
	outputs[0] = ht;
}
template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::backward()
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lht = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp3 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lo = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lct = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp1 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp2 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lf = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lu = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Li = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lh = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lc = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];}
	if(successorTime!=0) {Lct += this->successorTime->jacobians[1];}
	if(successorTime!=0) {Lht += this->successorTime->jacobians[2];}
	Lht += Lout;
	Ltmp3 += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(o) * Lht);
	Lo += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp3) * Lht);
	Lct += ((*Uoct).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo);
	Lct += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -tmp3.array().cwiseAbs2().matrix())) * Ltmp3);
	Ltmp1 += Lct;
	Ltmp2 += Lct;
	Lf += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(c) * Ltmp1);
	Lu += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(i) * Ltmp2);
	Li += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(u) * Ltmp2);
	Lh += ((*Uih).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lh += ((*Ufh).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf);
	Lh += ((*Uoh).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo);
	jacobians[2] = Lh;
	Lc += ((*Uic).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lc += ((*Uuc).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu);
	Lc += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(f) * Ltmp1);
	Lc += ((*Ufc).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf);
	jacobians[1] = Lc;
	Lx += ((*Uix).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lx += ((*Uux).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu);
	Lx += ((*Ufx).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf);
	Lx += ((*Uox).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo);
	jacobians[0] = Lx;
	*gUix += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li * x.transpose());
	*gUic += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li * c.transpose());
	*gUih += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li * h.transpose());
	*gbi += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li);
	*gUux += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu * x.transpose());
	*gUuc += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu * c.transpose());
	*gbu += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu);
	*gUfx += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf * x.transpose());
	*gUfc += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf * c.transpose());
	*gUfh += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf * h.transpose());
	*gbf += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf);
	*gUox += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo * x.transpose());
	*gUoct += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo * ct.transpose());
	*gUoh += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo * h.transpose());
	*gbo += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo);
}
template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::gradientStep(double alpha)
{
	*Uih -= alpha * (*gUih);
	gUih->setZero();
	*Uox -= alpha * (*gUox);
	gUox->setZero();
	*Uoh -= alpha * (*gUoh);
	gUoh->setZero();
	*bf -= alpha * (*gbf);
	gbf->setZero();
	*Ufc -= alpha * (*gUfc);
	gUfc->setZero();
	*bo -= alpha * (*gbo);
	gbo->setZero();
	*bi -= alpha * (*gbi);
	gbi->setZero();
	*bu -= alpha * (*gbu);
	gbu->setZero();
	*Uix -= alpha * (*gUix);
	gUix->setZero();
	*Ufx -= alpha * (*gUfx);
	gUfx->setZero();
	*Ufh -= alpha * (*gUfh);
	gUfh->setZero();
	*Uux -= alpha * (*gUux);
	gUux->setZero();
	*Uuc -= alpha * (*gUuc);
	gUuc->setZero();
	*Uoct -= alpha * (*gUoct);
	gUoct->setZero();
	*Uic -= alpha * (*gUic);
	gUic->setZero();
}
