
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<double> initializer(0,0.1);

#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;

#include "computeUnit.h" 
template <int inputDimension, int d>
class LSTM: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,outputDimension,1> h;
	Matrix<double,outputDimension,1> y;
	Matrix<double,d,1> f;
	Matrix<double,outputDimension,inputDimension>* Uhx = 0;
	Matrix<double,outputDimension,1>* bh = 0;
	Matrix<double,d,outputDimension>* Ufh = 0;
	Matrix<double,d,1>* bf = 0;
	LSTM();
	virtual void init();
	virtual void forward();
	virtual void backwardGradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension, int d>
LSTM<inputDimension, d>::LSTM() : ComputeUnit(1,0,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
}

template<int inputDimension, int d>
void LSTM<inputDimension, d>::init() 
{
	Uhx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uhx);
	bh = new Matrix<double,outputDimension,1>();
	zeroInitialization(bh);
	Ufh = new Matrix<double,d,outputDimension>();
	gaussianInitialization(Ufh);
	bf = new Matrix<double,d,1>();
	zeroInitialization(bf);
}
template<int inputDimension, int d>
ComputeUnit* LSTM<inputDimension, d>::duplicate() 
{
	LSTM<inputDimension, d>* l = new LSTM<inputDimension, d>();
	l->Uhx = Uhx;
	l->bh = bh;
	l->Ufh = Ufh;
	l->bf = bf;
	return (ComputeUnit*) l;
}
template<int inputDimension, int d>
void LSTM<inputDimension, d>::forward()
{
	x = *inputs[0];
	h = (1.0 + (-(((*Uhx) * x) + (*bh))).array().exp()).inverse().matrix();;
	f = (1.0 + (-(((*Ufh) * h) + (*bf))).array().exp()).inverse().matrix();;
	outputs[0] = f;
}
template<int inputDimension, int d>
void LSTM<inputDimension, d>::backwardGradientStep(double alpha)
{
	Matrix<double,outputDimension,1> Ly = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,d,1> Lf = Matrix<double,d,1>::Zero();
	Matrix<double,outputDimension,1> Lh = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Ly += this->successorDeep->jacobians[0];};
	Lf += Ly;
	Lh += ((*Ufh).transpose() * Eigen::DiagonalMatrix<double,d,d>((f.array() * (Matrix<double,d,1>::Constant(1) +  -f).array()).matrix()) * Lf);
	Lx += ((*Uhx).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((h.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -h).array()).matrix()) * Lh);
	jacobians[0] = Lx;
	*Uhx-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((h.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -h).array()).matrix()) * Lh * x.transpose());
	*bh-= alpha * Lh;
	*Ufh-= alpha * (Eigen::DiagonalMatrix<double,d,d>((f.array() * (Matrix<double,d,1>::Constant(1) +  -f).array()).matrix()) * Lf * h.transpose());
	*bf-= alpha * Lf;
}
