#pragma once
#include <iostream>
#include <Eigen/Dense>
using Eigen::Matrix;
#include "computeUnit.h"

template <int inputDimension, int hiddenDimension>
class LSTM2 : public ComputeUnit
{
public:
	//Matrix<double,hiddenDimension,1> h;
	Matrix<double,hiddenDimension,inputDimension>* Uq = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Vq = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Wq = 0;
	Matrix<double,hiddenDimension,1>* bq = 0;

	Matrix<double,hiddenDimension,inputDimension>* Uf = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Vf = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Wf = 0;
	Matrix<double,hiddenDimension,1>* bf = 0;

	Matrix<double,hiddenDimension,inputDimension>* Ug = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Vg = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Wg = 0;
	Matrix<double,hiddenDimension,1>* bg = 0;
	
	Matrix<double,hiddenDimension,inputDimension>* U = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* V = 0;
	Matrix<double,hiddenDimension,1>* b = 0;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,hiddenDimension,1> f;
	Matrix<double,hiddenDimension,1> g;
	Matrix<double,hiddenDimension,1> u;
	Matrix<double,hiddenDimension,1> q;

	Matrix<double,hiddenDimension,1> Lf;
	Matrix<double,hiddenDimension,1> Lg;
	Matrix<double,hiddenDimension,1> Lu;
	Matrix<double,hiddenDimension,1> Lq;
	Matrix<double,hiddenDimension,1> Lh;
	Matrix<double,hiddenDimension,1> Lc;

	LSTM2();
	virtual void init();
	virtual void forward();
	virtual void backwardGradientStep(double alpha);
	virtual ComputeUnit* duplicate();
	
};


template <int inputDimension, int hiddenDimension>
LSTM2<inputDimension,hiddenDimension>::LSTM2()  : ComputeUnit(1,2,1)
{
	outputs[0]=Matrix<double,hiddenDimension,1>::Zero();
	outputs[1]=Matrix<double,hiddenDimension,1>::Zero();
	outputs[2]=Matrix<double,hiddenDimension,1>::Zero();
	//h = Matrix<double,hiddenDimension,1>::Zero();
	jacobians[0]=Matrix<double,inputDimension,1>::Zero();	
	jacobians[1]=Matrix<double,hiddenDimension,1>::Zero();
	jacobians[2]=Matrix<double,inputDimension,1>::Zero();	
	
	f = Matrix<double,hiddenDimension,1>::Zero();
	g = Matrix<double,hiddenDimension,1>::Zero();
	u = Matrix<double,hiddenDimension,1>::Zero();
	q = Matrix<double,hiddenDimension,1>::Zero();
}

template<int inputDimension,int hiddenDimension>
void LSTM2<inputDimension,hiddenDimension>::init()
{
	//std::cout << "init" << std::endl;
	U = new Matrix<double,hiddenDimension,inputDimension>();
	V = new Matrix<double,hiddenDimension,hiddenDimension>();
	b = new Matrix<double,hiddenDimension,1>();

	Uf = new Matrix<double,hiddenDimension,inputDimension>();
	Vf = new Matrix<double,hiddenDimension,hiddenDimension>();
	Wf = new Matrix<double,hiddenDimension,hiddenDimension>();
	bf = new Matrix<double,hiddenDimension,1>();

	Ug = new Matrix<double,hiddenDimension,inputDimension>();
	Vg = new Matrix<double,hiddenDimension,hiddenDimension>();
	Wg = new Matrix<double,hiddenDimension,hiddenDimension>();
	bg = new Matrix<double,hiddenDimension,1>();

	Uq = new Matrix<double,hiddenDimension,inputDimension>();
	Vq = new Matrix<double,hiddenDimension,hiddenDimension>();
	Wq = new Matrix<double,hiddenDimension,hiddenDimension>();
	bq = new Matrix<double,hiddenDimension,1>();

	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<double> initializer(0,0.1);
	for(int i = 0;i<hiddenDimension;i++)
	{	
		(*b)(i,0)  =  0.0;
		(*bf)(i,0) =  0.0; //2.0;
		(*bg)(i,0) =  0.0; //-1.0;
		(*bq)(i,0) =  0.0; //-2.0;
		for(int j=0;j<inputDimension;j++)
		{
			(*U)(i,j)  = initializer(gen);
			(*Uf)(i,j) = initializer(gen);
			(*Ug)(i,j) = initializer(gen);
			(*Uq)(i,j)  = initializer(gen);
		}
		for(int j=0;j<hiddenDimension;j++)
		{
			(*V)(i,j)  = initializer(gen);
			(*Vf)(i,j) = initializer(gen);
			(*Vg)(i,j) = initializer(gen);
			(*Vq)(i,j) = initializer(gen);	
			(*Wf)(i,j) = initializer(gen);
			(*Wg)(i,j) = initializer(gen);
			(*Wq)(i,j) = initializer(gen);	
		}
	}
}

template<int inputDimension,int hiddenDimension>
ComputeUnit* LSTM2<inputDimension,hiddenDimension>::duplicate()
{
	// std::cout << "duplicate LSTM2 "<< std::endl;
	LSTM2<inputDimension,hiddenDimension>* l = new LSTM2<inputDimension,hiddenDimension>();
	l->U = U;
	l->V = V;
	l->b = b;

	l->Uf = Uf;
	l->Vf = Vf;
	l->Wf = Wf;
	l->bf = bf;

	l->Ug = Ug;
	l->Vg = Vg;
	l->Wg = Wg;
	l->bg = bg;

	l->Uq = Uq;
	l->Vq = Vq;
	l->Wq = Wq;
	l->bq = bq;

	return (ComputeUnit*)l;
}

template<int inputDimension,int hiddenDimension>
void LSTM2<inputDimension,hiddenDimension>::forward()
{

	f = ((*bf) + (*Uf) * (*inputs[0]) + (*Vf) * (*inputs[1]) + (*Wf) * (*inputs[2]));
	g = ((*bg) + (*Ug) * (*inputs[0]) + (*Vg) * (*inputs[1]) + (*Wg) * (*inputs[2]));
	u = ((*b ) + (*U ) * (*inputs[0]) + (*V ) * (*inputs[1]));
	

	// std::cout << z.transpose()<<" => ";

	f = (Matrix<double,hiddenDimension,1>::Constant(1) + (-f).array().exp().matrix()).array().cwiseInverse().matrix();
	g = (Matrix<double,hiddenDimension,1>::Constant(1) + (-g).array().exp().matrix()).array().cwiseInverse().matrix();
	u = u.array().tanh().matrix();
	

	// std::cout << z.transpose() << std::endl;
	outputs[2] = (f.array() * inputs[2]->array()).matrix()
				+(g.array() * u.array()).matrix();
	
	q = ((*bq) + (*Uq) * (*inputs[0]) + (*Vq) * (*inputs[1]) + (*Wq) * outputs[2]);
	q = (Matrix<double,hiddenDimension,1>::Constant(1) + (-q).array().exp().matrix()).array().cwiseInverse().matrix();
	
	outputs[0] = (outputs[2].array().tanh() * q.array()).matrix();
	outputs[1] = outputs[0];
	// std::cout << "lstm layer "<<this << " " << outputs[0].transpose()
	// 	<< " | " << outputs[1].transpose()
	// 	<< " | " << f.transpose()
	// 	<< " | " << g.transpose()
	// 	<< " | " << q.transpose()
	// 	<< " | " << u.transpose()
	// 	<<std::endl;
}

template<int inputDimension,int hiddenDimension>
void LSTM2<inputDimension,hiddenDimension>::backwardGradientStep(double alpha)
{
	//Backward Dynamics for output
	Lh = Matrix<double,hiddenDimension,1>::Zero();
	if(successorDeep!=0)
		Lh += successorDeep->jacobians[0];
	if(successorTime!=0)
		Lh += successorTime->jacobians[1];
	//Backward Dynamics for output gate
	{	
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(outputs[2].array().tanh().matrix());
		Lq = diag * Lh;
	}
	//Backward Dynamics for Cell
	{
		//successor 
		Lc = Matrix<double,hiddenDimension,1>::Zero();
		if(successorTime!=0)
			Lc += successorTime->jacobians[2];
		// h Path
		{
			Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[2].array().tanh().cwiseAbs2().matrix());
			Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag2(q);
			Lc += diag2 * (diag * Lh);
		}
		// q path
		{
			Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((q.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-q).array()).matrix());
			Lc += Vq->transpose() * diag * Lq;
		}
	}
	//Backward Dynamics for f
	Lf = Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension>(*inputs[2]) * Lc;
	//Backward Dynamics for g
	Lg = Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension>(u) * Lc;
	//Backward Dynamics for u
	Lu = Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension>(g) * Lc;
	//Backward Dynamics for xt   = inputs[0]
	//Backward Dynamics for ht-1 = inputs[1]
	//Backward Dynamics for ct-1 = inputs[2]
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((g.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-g).array()).matrix());
		jacobians[0] = Ug->transpose() * diag * Lg;
		jacobians[1] = Vg->transpose() * diag * Lg;
		jacobians[2] = Wg->transpose() * diag * Lg;
	}
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((f.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-f).array()).matrix());
		jacobians[0] += Uf->transpose() * diag * Lf;
		jacobians[1] += Vf->transpose() * diag * Lf;
		jacobians[2] += Wf->transpose() * diag * Lf;
	}
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-u.array().cwiseAbs2().matrix());
		jacobians[0] += U->transpose() * diag * Lu;
		jacobians[1] += V->transpose() * diag * Lu;
	}
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((q.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-q).array()).matrix());	
		jacobians[0] += Uq->transpose() * diag * Lq;
		jacobians[1] += Vq->transpose() * diag * Lq;	
	}
	{
		jacobians[2] += Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension>(f) * Lc;
	}

	double d = jacobians[1].norm();
	if(d>1)
	{
		// std::cout << "C";// << std::endl;
		jacobians[1]*=1/d;
	}
	d = jacobians[0].norm();
	if(d>1)
	{
		// std::cout << "C";// << std::endl;
		jacobians[0]*=1/d;
		
	}
	d = jacobians[2].norm();
	if(d>1)
	{
		// std::cout << "C";// << std::endl;
		jacobians[2]*=1/d;
		
	}
	
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-u.array().cwiseAbs2().matrix());
		*U -= alpha * diag * Lu * inputs[0]->transpose();
		*V -= alpha * diag * Lu * inputs[1]->transpose();
		*b -= alpha * diag * Lu;
	}
	
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((f.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-f).array()).matrix());
		*Uf -= alpha * diag * Lf * inputs[0]->transpose();
		*Vf -= alpha * diag * Lf * inputs[1]->transpose();
		*Wf -= alpha * diag * Lf * inputs[2]->transpose();
		*bf -= alpha * diag * Lf;
	}

	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((g.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-g).array()).matrix());
		*Ug -= alpha * diag * Lg * inputs[0]->transpose();
		*Vg -= alpha * diag * Lg * inputs[1]->transpose();
		*Wg -= alpha * diag * Lg * inputs[2]->transpose();
		*bg -= alpha * diag * Lg;
	}

	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((q.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-q).array()).matrix());
		*Uq -= alpha * diag * Lq * inputs[0]->transpose();
		*Vq -= alpha * diag * Lq * inputs[1]->transpose();
		*Wq -= alpha * diag * Lq * outputs[2].transpose();
		*bq -= alpha * diag * Lq;
	}
}


// #endif

// #endif outputs[0]=(this->W * (*inputs[0])).cwiseMax(0);