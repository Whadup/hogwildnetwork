
#pragma once
#include <iostream> 
#include <vector>
#include <Eigen/Dense>
using Eigen::Matrix;
using Eigen::MatrixXd;
using Eigen::VectorXd;

#ifndef NORMAL
#define NORMAL
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<> d(0,1/10.0);
std::normal_distribution<> initializer(0,0.1);
#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;


#endif 


#include "computeUnit.h" 
#include "holographic/holographic.h"
template <int inputDimension, int outputDimension>
class ANN: public ComputeUnit
{
public:
	bool first = 1;

	std::vector<VectorXd>* ks = 0;
	std::vector<VectorXd>* ws = 0;
	
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	VectorXd vraw;
	VectorXd v;
	VectorXd k2;
	VectorXd v2;
	Matrix<double,outputDimension,1> y;
	VectorXd k;
	Matrix<double,outputDimension,1> out;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	MatrixXd* Uk2x = 0;
	Matrix<double,outputDimension,1>* bk2 = 0;
	MatrixXd* Uv2v = 0;
	MatrixXd* Uyk2 = 0;
	MatrixXd* Ukx = 0;
	Matrix<double,outputDimension,1>* bk = 0;
	MatrixXd* Uyk = 0;
	MatrixXd* Uyv2 = 0;
	MatrixXd* Uyv = 0;
	Matrix<double,outputDimension,1>* bv2 = 0;
	MatrixXd* Uk2v = 0;
	MatrixXd* Uv2k = 0;
	Matrix<double,outputDimension,1>* by = 0;
	MatrixXd* Uk2k = 0;

	MatrixXd* gUk2x = 0;
	Matrix<double,outputDimension,1>* gbk2 = 0;
	MatrixXd* gUv2v = 0;
	MatrixXd* gUyk2 = 0;
	MatrixXd* gUkx = 0;
	Matrix<double,outputDimension,1>* gbk = 0;
	MatrixXd* gUyk = 0;
	MatrixXd* gUyv2 = 0;
	MatrixXd* gUyv = 0;
	Matrix<double,outputDimension,1>* gbv2 = 0;
	MatrixXd* gUk2v = 0;
	MatrixXd* gUv2k = 0;
	Matrix<double,outputDimension,1>* gby = 0;
	MatrixXd* gUk2k = 0;

	Memory<outputDimension,512>* memory = 0;

	ANN();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension, int outputDimension>
ANN<inputDimension, outputDimension>::ANN() : ComputeUnit(1,0,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
}

template<int inputDimension, int outputDimension>
void ANN<inputDimension, outputDimension>::init() 
{
	Uk2x = new MatrixXd(outputDimension,inputDimension);
	gaussianInitialization(Uk2x);
	bk2 = new Matrix<double,outputDimension,1>();
	zeroInitialization(bk2);
	Uv2v = new MatrixXd(outputDimension,inputDimension);
	gaussianInitialization(Uv2v);
	Uyk2 = new MatrixXd(outputDimension,outputDimension);
	gaussianInitialization(Uyk2);
	Ukx = new MatrixXd(outputDimension,inputDimension);
	gaussianInitialization(Ukx);
	bk = new Matrix<double,outputDimension,1>();
	zeroInitialization(bk);
	Uyk = new MatrixXd(outputDimension,outputDimension);
	gaussianInitialization(Uyk);
	Uyv2 = new MatrixXd(outputDimension,outputDimension);
	gaussianInitialization(Uyv2);
	Uyv = new MatrixXd(outputDimension,inputDimension);
	gaussianInitialization(Uyv);
	bv2 = new Matrix<double,outputDimension,1>();
	zeroInitialization(bv2);
	Uk2v = new MatrixXd(outputDimension,inputDimension);
	gaussianInitialization(Uk2v);
	Uv2k = new MatrixXd(outputDimension,outputDimension);
	gaussianInitialization(Uv2k);
	by = new Matrix<double,outputDimension,1>();
	zeroInitialization(by);
	Uk2k = new MatrixXd(outputDimension,outputDimension);
	gaussianInitialization(Uk2k);

	gUk2x = new MatrixXd(outputDimension,inputDimension);
	gUk2x->setZero();
	gbk2 = new Matrix<double,outputDimension,1>();
	gbk2->setZero();
	gUv2v = new MatrixXd(outputDimension,inputDimension);
	gUv2v->setZero();
	gUyk2 = new MatrixXd(outputDimension,outputDimension);
	gUyk2->setZero();
	gUkx = new MatrixXd(outputDimension,inputDimension);
	gUkx->setZero();
	gbk = new Matrix<double,outputDimension,1>();
	gbk->setZero();
	gUyk = new MatrixXd(outputDimension,outputDimension);
	gUyk->setZero();
	gUyv2 = new MatrixXd(outputDimension,outputDimension);
	gUyv2->setZero();
	gUyv = new MatrixXd(outputDimension,inputDimension);
	gUyv->setZero();
	gbv2 = new Matrix<double,outputDimension,1>();
	bv2->setZero();
	gUk2v = new MatrixXd(outputDimension,inputDimension);
	gUk2v->setZero();
	gUv2k = new MatrixXd(outputDimension,outputDimension);
	gUv2k->setZero();
	gby = new Matrix<double,outputDimension,1>();
	gby->setZero();
	gUk2k = new MatrixXd(outputDimension,outputDimension);
	gUk2k->setZero();
	memory = new Memory<outputDimension,512>;
	memory->init();

}
template<int inputDimension, int outputDimension>
ComputeUnit* ANN<inputDimension, outputDimension>::duplicate() 
{
	ANN<inputDimension, outputDimension>* l = new ANN<inputDimension, outputDimension>();
	l->Uk2x = Uk2x;
	l->bk2 = bk2;
	l->Uv2v = Uv2v;
	l->Uyk2 = Uyk2;
	l->Ukx = Ukx;
	l->bk = bk;
	l->Uyk = Uyk;
	l->Uyv2 = Uyv2;
	l->Uyv = Uyv;
	l->bv2 = bv2;
	l->Uk2v = Uk2v;
	l->Uv2k = Uv2k;
	l->by = by;
	l->Uk2k = Uk2k;
	
	l->gUk2x =	gUk2x;
	l->gbk2 = 	gbk2;
	l->gUv2v =	gUv2v;
	l->gUyk2 =	gUyk2;
	l->gUkx = 	gUkx;
	l->gbk = 	gbk;
	l->gUyk = 	gUyk;
	l->gUyv2 =	gUyv2;
	l->gUyv = 	gUyv;
	l->gbv2 = 	gbv2;
	l->gUk2v =	gUk2v;
	l->gUv2k =	gUv2k;
	l->gby = 	gby;
	l->gUk2k =	gUk2k;
	l->memory = new Memory<outputDimension,512>;
	l->memory->basis = memory->basis;
	l->memory->inver = memory->inver;

	return (ComputeUnit*) l;
}
template<int inputDimension, int outputDimension>
void ANN<inputDimension, outputDimension>::forward()
{
	if(first && ks==0)
	{
		// memory = new Memory<outputDimension,512>;
		ks = new std::vector<VectorXd>;
		ws = new std::vector<VectorXd>;
	}
	else if(first)
	{
		memory->setZero();
		ks->clear();
		ws->clear();
	}

	x = *inputs[0];
	k = x;// (((*Ukx) * x) + (*bk)).array().tanh().matrix();
	vraw = memory->read(k);
	double d = vraw.norm();

	if(d>0.0000000001)
		v = vraw/d;
	else
		v = vraw;
	// std::cout << "x  " << x.transpose() << std::endl;
	// std::cout << "v1 " << v.transpose() << std::endl;
	// std::cout << "k1 " << k.transpose() << std::endl;
	k2 = (((*Uk2k) * k) + ((*Uk2v) * v)  + (*bk2)).array().tanh().matrix(); //+ ((*Uk2x) * x)
	v2 = (((*Uv2k) * k) + ((*Uv2v) * v)  + (*bv2)).array().tanh().matrix();

	ks->push_back(k2);
	ws->push_back(v2);

	// std::cout << "v2 " << v2.transpose() << std::endl;
	// std::cout << "k2 " << k2.transpose() << std::endl;
	y = (((*Uyk) * k) + ((*Uyv) * v)  + ((*Uyv2) * v2) + (*by)); //+ ((*Uyk2) * k2)
	// std::cout << "y " << y.transpose() << std::endl;
	outputs[0] = y;
	memory->write(k2,v2);
	if(successorTime!=0)
	{
		((ANN*)successorTime)->first = 0;
		((ANN*)successorTime)->ks = ks;
		((ANN*)successorTime)->ws = ws;
		// ((ANN*)successorTime)->memory.reset(memory.traceFreq);
		if(((ANN*)successorTime)->memory != memory)
		{
			delete ((ANN*)successorTime)->memory; 
			//lalala den hätte man eigentlich nichtmal erstellen dürfen, 
			//aber ich lösche ihn einfach als wäre das nie passiert
		}
		((ANN*)successorTime)->memory = memory;
	}

}
template<int inputDimension, int outputDimension>
void ANN<inputDimension, outputDimension>::backward()
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ly = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lv2 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lk2 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lk = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lv = Matrix<double,inputDimension,1>::Zero();
	MatrixXd Lvraw = MatrixXd::Zero(inputDimension,1);
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];}

	// if(successorTime!=0)
	// 	memory.LtraceFreq = ((ANN*)successorTime)->memory.LtraceFreq;
	// else
	// 	memory.LtraceFreq = Matrix<Complex,Eigen::Dynamic,1>::Zero(1024);
	
	// std::cout << "deltas: " << memory.LtraceFreq.norm() << " ";

	//regularizer code

	double closest = 10000000;
	int closest_i = -1;
	int clostest_korw = 0;

	for(int i=0;i<ws->size();i++)
	{
		VectorXd d = (*ks)[i];
		double dist = (d-x).norm();
		if(dist< closest)
		{
			closest = dist;
			closest_i = i;
			clostest_korw = 0;
		}
		d = (*ws)[i];
		dist = (d-x).norm();
		if(dist< closest)
		{
			closest = dist;
			closest_i = i;
			clostest_korw = 1;
		}
	}	
#ifdef DEBUG
	std::cout << closest_i << "-";
	if(first) std::cout << std::endl;
#endif
	//end regularizer code


	Ly += Lout;
	// if(Ly.norm()>1)
	// 	Ly = Ly/Ly.norm();
	auto write1 = memory->deltaWrite1(k2,v2);
	Lk2 += write1;
	//Lk2 += ((*Uyk2).transpose() * Ly);
	
	auto write2 = memory->deltaWrite2(k2,v2);
	Lv2 += write2;
	Lv2 += ((*Uyv2).transpose() * Ly);
	
	Lv += ((*Uk2v).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k2.array().cwiseAbs2().matrix())) * Lk2);
	Lv += ((*Uv2v).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -v2.array().cwiseAbs2().matrix())) * Lv2);
	Lv += ((*Uyv).transpose() * Ly);
	
	double d = vraw.norm();
	if(d>0.0000000001)
		Lvraw += ((Matrix<double,outputDimension,outputDimension>::Identity() - v * v.transpose()).array() / d).matrix().transpose() * Lv; //Todo Normalization
	else
		Lvraw += Lv;
	
	Lk += ((*Uk2k).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k2.array().cwiseAbs2().matrix())) * Lk2);
	Lk += ((*Uv2k).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -v2.array().cwiseAbs2().matrix())) * Lv2);
	Lk += ((*Uyk).transpose() * Ly);
	auto read = memory->deltaRead(k,Lvraw);
	Lk += read;
	
	Lx = Lk;

	//Lx += Lv;
	//Lx += ((*Uk2x).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k2.array().cwiseAbs2().matrix())) * Lk2);
	//Lx += ((*Ukx).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k.array().cwiseAbs2().matrix())) * Lk);
	
	// std::cout << Ly.norm() << " ";
	// std::cout << write1.norm() << " ";
	// std::cout << write2.norm() << " ";
	// std::cout << Lk2.norm() << " ";
	// std::cout << Lv2.norm() << " ";
	// std::cout << Lv.norm() << " ";
	// std::cout << Lvraw.norm() << " ";
	// std::cout << Lk.norm() << " ";
	// std::cout << read.norm() << " ";
	// std::cout << Lx.norm() << " " << std::endl;

	jacobians[0] = Lx;
	// if(jacobians[0].norm()>1)
	// 	jacobians[0]/=jacobians[0].norm();
	// //std::cout << Ly.norm() << " " << Lx.norm() << "\t";

	*gUk2k += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k2.array().cwiseAbs2().matrix())) * Lk2 * k.transpose());
	*gUk2v += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k2.array().cwiseAbs2().matrix())) * Lk2 * v.transpose());
	*gUk2x += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k2.array().cwiseAbs2().matrix())) * Lk2 * x.transpose());
	*gbk2 += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k2.array().cwiseAbs2().matrix())) * Lk2);
	*gUv2k += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -v2.array().cwiseAbs2().matrix())) * Lv2 * k.transpose());
	*gUv2v += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -v2.array().cwiseAbs2().matrix())) * Lv2 * v.transpose());
	*gbv2 += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -v2.array().cwiseAbs2().matrix())) * Lv2);
	*gUyk += (Ly * k.transpose());
	*gUyv += (Ly * v.transpose());
	*gUyk2 += (Ly * k2.transpose());
	*gUyv2 += (Ly * v2.transpose());
	*gby += Ly;
	*gUkx +=  (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k.array().cwiseAbs2().matrix())) * Lk * x.transpose());
	*gbk +=  (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -k.array().cwiseAbs2().matrix())) * Lk);
}

template<int inputDimension,int outputDimension>
void ANN<inputDimension,outputDimension>::gradientStep(double alpha)
{
	*Uk2k -= alpha * *gUk2k;
	*Uk2v -= alpha * *gUk2v;
	*Uk2x -= alpha * *gUk2x;
	*bk2 -= alpha * *gbk2;
	*Uv2k -= alpha * *gUv2k;
	*Uv2v -= alpha * *gUv2v;
	*bv2 -= alpha * *gbv2;
	*Uyk -= alpha * *gUyk;
	*Uyv -= alpha * *gUyv;
	*Uyk2 -= alpha * *gUyk2;
	*Uyv2 -= alpha * *gUyv2;
	*by -= alpha * *gby;
	*Ukx -= alpha * *gUkx;
	*bk -= alpha * *gbk;

	gUk2x->setZero();
	gbk2->setZero();
	gUv2v->setZero();	
	gUyk2->setZero();
	gUkx->setZero();
	gbk->setZero();
	gUyk->setZero();
	gUyv2->setZero();
	gUyv->setZero();
	bv2->setZero();
	gUk2v->setZero();
	gUv2k->setZero();
	gby->setZero();
	gUk2k->setZero();
}