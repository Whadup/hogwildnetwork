#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <utility>
#include <sys/time.h>

inline double getTime(){
	struct timeval tv;
	if (gettimeofday(&tv, NULL)==0)
		return tv.tv_sec+((double)(tv.tv_usec))/1e6;
	else
		return 0.0;
}

unsigned int toDigit(const char& c){
	if(c==49) return 1;
	else if(c==50) return 2;
	else if(c==51) return 3;
	else if(c==52) return 4;
	else if(c==53) return 5;
	else if(c==54) return 6;
	else if(c==55) return 7;
	else if(c==56) return 8;
	else if(c==57) return 9;
	return 0;
}

int toInt(const char* c, const int& n){
	int r = 0;
	int b = 1;
	for(int i=n-1; i>=0; --i){
		r += b * toDigit(c[i]);
		b *= 10;
	}
	return r;
}

template<bool labeled>
int* parseSparse(const std::string& in,std::vector<int>& ref,int& largest,int& l){

	const char* raw = in.c_str();
	const int n  = in.size();

	int app   = 0;
	int count = 0;

	int pos = 0;
	int state = 0;
	int gender = 0;
	ref.clear();
	for(unsigned int i=0; i<n; ++i){

		if(raw[i] == ':'){
			app = toInt(raw+pos, i-pos);
			if(app>largest) largest = app;
			ref.push_back(app);
			// std::cout << app << std::endl;
			pos = i+1;

		}else if(raw[i] == '\t' || raw[i]==' '){
			if(labeled && state == 0)
			{
				gender = toInt(raw+pos, i-pos);
				// std::cout << gender << std::endl;
				state = 1;
			}
			else
			{
				count = toInt(raw+pos,i-pos);
				for(int i=1;i<count;i++)
					ref.push_back(app);
			}
			pos = i + 1;
		}
	}
	if(pos<n)
	{
		count = toInt(raw+pos, n-pos);
		for(int i=1;i<count;i++)
			ref.push_back(app);
	}

	int labeledOffset = labeled ? 1 : 0;
	int* a = new int[ref.size() + labeledOffset];

	l = ref.size();
	// if(gender<=3)
	// 	a[1] = 0;
	// else
	// 	a[1] = 1; 
	if(labeled)
		a[0]=gender;
	// std::cout << a[1] << std::endl;
	for(int i=0;i<ref.size();i++)
		a[labeledOffset+i] = ref[i];
	return a;
	
}

int* parseIntegerTokens(const std::string& in,std::vector<int>& ref, int& length){

	const char* raw = in.c_str();
	const int n  = in.size();


	int pos = 0;
	ref.clear();
	for(unsigned int i=0; i<n; ++i){

		if(raw[i] == ' ' || raw[i]=='\n'){
			if(raw[i]=='\n')
				std::cerr << "SUCCESS";
			ref.push_back(toInt(raw+pos, i-pos));
			// std::cout << app << std::endl;
			pos = i+1;

		}
	}
	if(pos<n)
	{
		// std::cerr <<  << " " << in << std::endl;
		ref.push_back(toInt(raw+pos, n-pos));
	}
	int* a = new int[ref.size()+1];
	length = ref.size() + 1;
	a[ref.size()] = 0;
	// if(gender<=3)
	// 	a[1] = 0;
	// else
	// 	a[1] = 1; 
	// std::cout << a[1] << std::endl;
	for(int i=0;i<ref.size();i++)
		a[i] = ref[i];
	return a;
	
}