#pragma once
// #ifndef _L2LOSS_
// #define _L2LOSS_
#include "computeUnit.h"
#include "loss.h"
#include <Eigen/Dense>
#include <iostream>
using Eigen::Matrix;

template<int dimension>
class L2Loss: public ComputeUnit, public Loss
{
public:
	L2Loss();
	Matrix<double,dimension,1>* y;
	double l;
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha){};
	virtual ComputeUnit* duplicate();
	double loss();
	double acc(){return 0;}
};

template<int dimension>
L2Loss<dimension>::L2Loss() : ComputeUnit(1,0)
{
	jacobians[0]=Matrix<double,dimension,1>::Zero();
	std::cout << jacobians[0] << std::endl;
}
template<int dimension>
ComputeUnit* L2Loss<dimension>::duplicate()
{
	return (ComputeUnit*)new L2Loss();
}

template<int dimension>
void L2Loss<dimension>::forward()
{
	Matrix<double,dimension,1> blub = *y-*inputs[0];
	l = 0.5 * blub.dot(blub);
}

template<int dimension>
double L2Loss<dimension>::loss()
{
	return l;
}

template<int dimension>
void L2Loss<dimension>::backward()
{
	jacobians[0] = *inputs[0]-*y;
	// std::cout << jacobians[0] << std::endl;
	// std::cout << "loss layer "<<this << " " << jacobians[0].transpose()<<std::endl;
	//std::cout << jacobians[0].rows() << "x" << jacobians[0].cols() << std::endl;
	//jacobians[0]=Matrix<double,dimension,1>::Zero();
}

// #endif