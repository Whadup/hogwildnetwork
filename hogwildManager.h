#pragma once
#include <thread>
#include <mutex>
#include <string>
#include "networkExecuter.h"
#include "dataSource.h"
#include "dataInterfaces.h"
// #include <hyperboard.h>
template<class T,int numberOfThreads>
class HogwildManagerBase
{
	static_assert(std::is_base_of<Example, T>::value, "T must derive from Example");
public:
	NetworkExecuter** networkCopies;
	DataSource<T>* dataSource;
	std::thread* threads;
	double lastLoss = 0;
	double lastAcc = 0;
	unsigned int const batchSize;
	double stepSize;
	double w = 1.0;
	int it[numberOfThreads] = {0};
	std::string loggerNames[numberOfThreads];

	std::mutex output;
	HogwildManagerBase(NetworkExecuter* e,DataSource<T>* d);
	virtual ~HogwildManagerBase();
	virtual void feedData(int i,T* d) {};
	void endlessLoop(int i);
	void go(double);
};

template<class T,int numberOfThreads>
class HogwildManager : public HogwildManagerBase<T,numberOfThreads>
{
	public:
		HogwildManager(NetworkExecuter* e,DataSource<T>* d) : HogwildManagerBase<T,numberOfThreads>(e,d) {}
		void feedData(int i,T* d);
};

template<int numberOfThreads>
class HogwildManager<SeqSeqExample,numberOfThreads> : public HogwildManagerBase<SeqSeqExample,numberOfThreads>
{
	public:
		HogwildManager(NetworkExecuter* e,DataSource<SeqSeqExample>* d) : HogwildManagerBase<SeqSeqExample,numberOfThreads>(e,d) {}
		void feedData(int i,SeqSeqExample* d);
};
template<int numberOfThreads>
class HogwildManager<SeqExample,numberOfThreads> : public HogwildManagerBase<SeqExample,numberOfThreads>
{
	public:
		HogwildManager(NetworkExecuter* e,DataSource<SeqExample>* d) : HogwildManagerBase<SeqExample,numberOfThreads>(e,d) {}
		void feedData(int i,SeqExample* d);
};
template<int numberOfThreads>
class HogwildManager<SeqBowSeqExample,numberOfThreads> : public HogwildManagerBase<SeqBowSeqExample,numberOfThreads>
{
	public:
		HogwildManager(NetworkExecuter* e,DataSource<SeqBowSeqExample>* d) : HogwildManagerBase<SeqBowSeqExample,numberOfThreads>(e,d) {}
		void feedData(int i,SeqBowSeqExample* d);
};
template<int numberOfThreads>
class HogwildManager<IntBowExample,numberOfThreads> : public HogwildManagerBase<IntBowExample,numberOfThreads>
{
	public:
		HogwildManager(NetworkExecuter* e,DataSource<IntBowExample>* d) : HogwildManagerBase<IntBowExample,numberOfThreads>(e,d) {}
		void feedData(int i,IntBowExample* d);
};
template<int numberOfThreads>
class HogwildManager<IntIntExample,numberOfThreads> : public HogwildManagerBase<IntIntExample,numberOfThreads>
{
	public:
		HogwildManager(NetworkExecuter* e,DataSource<IntIntExample>* d) : HogwildManagerBase<IntIntExample,numberOfThreads>(e,d) {}
		void feedData(int i,IntIntExample* d);
};
template<int numberOfThreads>
class HogwildManager<WeightedIntIntExample,numberOfThreads> : public HogwildManagerBase<WeightedIntIntExample,numberOfThreads>
{
	public:
		HogwildManager(NetworkExecuter* e,DataSource<WeightedIntIntExample>* d) : HogwildManagerBase<WeightedIntIntExample,numberOfThreads>(e,d) {}
		void feedData(int i,WeightedIntIntExample* d);
};
//thread::hardware_concurrency();
template<class T,int numberOfThreads>
HogwildManagerBase<T,numberOfThreads>::HogwildManagerBase(NetworkExecuter* e,DataSource<T>* d) : batchSize(1)
{
	dataSource = d;
	networkCopies = new NetworkExecuter*[numberOfThreads*batchSize];
	networkCopies[0]=e;
	//e->init();
	for(int i=0;i<numberOfThreads;i++)
	{
		// loggerNames[i] = registerLogger("hogwildManager"+std::to_string(i),"loss");
	}
	// std::cout << "doing shit " << e << std::endl;
	for(int i=1;i<numberOfThreads*batchSize;i++)
	{

		networkCopies[i] = e->duplicate();
	}
}

template<class T,int numberOfThreads>
HogwildManagerBase<T,numberOfThreads>::~HogwildManagerBase()
{
	delete[] threads;
	for(int i=1;i<numberOfThreads*batchSize;i++)
		delete networkCopies[i];
	delete[] networkCopies;
}

template<class T,int numberOfThreads>
void HogwildManagerBase<T,numberOfThreads>::endlessLoop(int i)
{
	double loss = 0;
	double acc = 0;
	double total = 0;

	int a = 0;
	T** data = new T*[256];
	int dada = 0;
	int dadb =  dataSource->nextItem(256,data);;
	Loss* lossUnit = dynamic_cast<Loss*>(networkCopies[i]->layers[networkCopies[i]->numLayers-1]);
	
	while(true)
	{

		if(dada>=dadb)
		{
			// std::cout << i<<"lade 100 items"<<std::endl;
			dadb = dataSource->nextItem(256,data);
			dada = 0;
		}
		a++;
		if(dadb==0)
			break;
		//std::cout << "feed ";
		// std::cout << data[dada] << std::endl;
		for(int j=0;j<batchSize && dada+j<dadb;j++)
		{
			feedData(i+j,data[dada+j]);
		}
		// std::cout << "forward "<<i << std::endl;
		int numlayers = networkCopies[i]->numLayers;
		for(int k=0;k<numlayers;k++)
			for(int j=0;j<batchSize && dada+j<dadb;j++)
				{
					// std::cout << networkCopies[i+j]->layers[k] << std::endl;
					networkCopies[i+j]->layers[k]->forward();
				}
		//std::cout << "backward ";
		//networkCopies[i]->backward();
		

		loss += lossUnit->loss()*w;
		acc += lossUnit->acc();
		total += lossUnit->weight();
		it[i]++;
		if(it[i] % 100000 == 99999) // && total>1000)
		{
			output.lock();
			// stepSize *= 0.99;
			// logValue(loggerNames[i],it[i],loss/total);
			std::cout << "Train: " << loss/total << "\t" << std::pow(2,loss/total) << "\t" << std::endl;
			// std::cout << stepSize << std::endl;
			output.unlock();
		}
		// acc += ((CrossEntropyLoss<2>*)networkCopies[i]->layers[networkCopies[i]->numLayers-1])->acc;
		
		//std::cout << "gradient ";
		//for(int k=numlayers-1;k>=0;k--)
		for(int j=0;j<batchSize && dada+j<dadb;j++)
		{
			networkCopies[i+j]->backward();
		}
		//for(int k=numlayers-1;k>=0;k--)
			// for(int j=0;j<batchSize && dada+j<dadb;j++)
		//	{
		
		networkCopies[i]->gradientStep(w * stepSize);
		//	}
		dada+=batchSize;
		//std::cout << "done "<< std::endl;
	}
	std::cout << std::endl;
	lastLoss = loss/total;
	lastAcc =  acc /it[i];

	output.lock();
			// stepSize *= 0.99;
	std::cout << lastLoss << "\t" << std::pow(2,lastLoss) << "\t" << std::endl;
	// logValue(loggerNames[i],it[i],lastLoss);
			// std::cout << stepSize << std::endl;
	output.unlock();
			// std::cout<<(loss/1000)<<std::endl;
			// if(lastAcc>=1)
			// 	break;
		
}

template<class T,int numberOfThreads>
void HogwildManagerBase<T,numberOfThreads>::go(double s)
{
	stepSize = s;
	threads = new std::thread[numberOfThreads];
	for(int i=0;i<numberOfThreads;i++)
		threads[i] = std::thread(&HogwildManager<T,numberOfThreads>::endlessLoop,this,i*batchSize);
	for(int i=0;i<numberOfThreads;i++)
		threads[i].join();
}

template<class T,int numberOfThreads>
void HogwildManager<T,numberOfThreads>::feedData(int i,T* d)
{
	std::cerr << "TEMPLATE NOT SPECIFIED IN HOGWILD MANAGER" << std::endl;
}



template<int numberOfThreads>
void HogwildManager<SeqSeqExample,numberOfThreads>::feedData(int i,SeqSeqExample* y)
{
	// std::cout << "TEMPLATE MAGIC" << std::endl;
	SequenceInput* inputUnit = dynamic_cast<SequenceInput*>(this->networkCopies[i]->layers[0]);
	inputUnit->l = y->l;
	inputUnit->input = y->input;
	SequenceLabel* outputUnit = dynamic_cast<SequenceLabel*>(this->networkCopies[i]->layers[this->networkCopies[i]->numLayers-1]);
	outputUnit->ll = y->ll;
	outputUnit->y = y->y;
}
template<int numberOfThreads>
void HogwildManager<SeqExample,numberOfThreads>::feedData(int i,SeqExample* y)
{
	SequenceInput* inputUnit = dynamic_cast<SequenceInput*>(this->networkCopies[i]->layers[0]);
	inputUnit->l = y->l;
	inputUnit->input = y->input;
	IntegerLabel* outputUnit = dynamic_cast<IntegerLabel*>(this->networkCopies[i]->layers[this->networkCopies[i]->numLayers-1]);
	outputUnit->y = y->y;
}
template<int numberOfThreads>
void HogwildManager<SeqBowSeqExample,numberOfThreads>::feedData(int i,SeqBowSeqExample* y)
{
	SequenceInput* inputUnit = dynamic_cast<SequenceInput*>(this->networkCopies[i]->layers[0]);
	inputUnit->l = y->l;
	inputUnit->input = y->input;
	BowSequenceLabel* outputUnit = dynamic_cast<BowSequenceLabel*>(this->networkCopies[i]->layers[this->networkCopies[i]->numLayers-1]);
	outputUnit->y = y->y;
	outputUnit->lll = y->lll;
	outputUnit->ll = y->ll;
}

template<int numberOfThreads>
void HogwildManager<IntBowExample,numberOfThreads>::feedData(int i,IntBowExample* y)
{
	// std::cout << "TEMPLATE MAGIC" << std::endl;
	IntegerInput* inputUnit = dynamic_cast<IntegerInput*>(this->networkCopies[i]->layers[0]);
	inputUnit->input = y->input;
	SequenceLabel* outputUnit = dynamic_cast<SequenceLabel*>(this->networkCopies[i]->layers[this->networkCopies[i]->numLayers-1]);
	outputUnit->ll = y->ll;
	outputUnit->y = y->y;
}

template<int numberOfThreads>
void HogwildManager<IntIntExample,numberOfThreads>::feedData(int i,IntIntExample* y)
{
	// std::cout << "TEMPLATE MAGIC" << std::endl;
	IntegerInput* inputUnit = dynamic_cast<IntegerInput*>(this->networkCopies[i]->layers[0]);
	inputUnit->input = y->input;
	IntegerLabel* outputUnit = dynamic_cast<IntegerLabel*>(this->networkCopies[i]->layers[this->networkCopies[i]->numLayers-1]);
	// outputUnit->ll = y->ll;
	outputUnit->y = y->y;
}

template<int numberOfThreads>
void HogwildManager<WeightedIntIntExample,numberOfThreads>::feedData(int i,WeightedIntIntExample* y)
{
	// std::cout << "TEMPLATE MAGIC" << std::endl;
	IntegerInput* inputUnit = dynamic_cast<IntegerInput*>(this->networkCopies[i]->layers[0]);
	inputUnit->input = y->input;
	IntegerLabel* outputUnit = dynamic_cast<IntegerLabel*>(this->networkCopies[i]->layers[this->networkCopies[i]->numLayers-1]);
	// outputUnit->ll = y->ll;
	this->w = 1;//y->w;
	outputUnit->y = y->y;
}
