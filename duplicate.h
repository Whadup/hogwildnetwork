// <unit name="Duplicate">
// 	<parameter name="dimension" type="int"/>
// 	<input id="x" dimension="dimension" />
// 	<output id="out1" dimension="dimension">
// 		<in key="x" />
// 	</output>
// 	<output id="out2" dimension="dimension">
// 		<in key="x" />
// 	</output>
// </unit>
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;

#include "computeUnit.h" 
template <int dimension>
class Duplicate: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,dimension,1> out1;
	Matrix<double,dimension,1> out2;
	Duplicate();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	ComputeUnit* successorDeep2;
	virtual ComputeUnit* duplicate();
		
};
			
template<int dimension>
Duplicate<dimension>::Duplicate() : ComputeUnit(1,0,2) 
{
	outputs[0] = Matrix<double,dimension,1>::Zero();
	outputs[1] = Matrix<double,dimension,1>::Zero();
	jacobians[0] = Matrix<double,dimension,1>::Zero();
}

template<int dimension>
void Duplicate<dimension>::init() 
{
}
template<int dimension>
ComputeUnit* Duplicate<dimension>::duplicate() 
{
	Duplicate<dimension>* l = new Duplicate<dimension>();
	return (ComputeUnit*) l;
}
template<int dimension>
void Duplicate<dimension>::forward()
{
	x = *inputs[0];
	outputs[0] = x;
	outputs[1] = x;
}
template<int dimension>
void Duplicate<dimension>::backward()
{
	Matrix<double,dimension,1> Lout2 = Matrix<double,dimension,1>::Zero();
	Matrix<double,dimension,1> Lout1 = Matrix<double,dimension,1>::Zero();
	Matrix<double,dimension,1> Lx = Matrix<double,dimension,1>::Zero();
	#warning "Duplicate from duplicate.h has a bug. Never connect the first copy to a different input port than the second copy!"
	if(successorDeep!=0) {Lout1 += this->successorDeep->jacobians[connectedToSlot];}
	if(successorDeep2!=0) {Lout2 += this->successorDeep2->jacobians[connectedToSlot];} //THIS IS SHIT, see #warning
	Lx += Lout1;
	Lx += Lout2;
	jacobians[0] = Lx;
}
template<int dimension>
void Duplicate<dimension>::gradientStep(double alpha)
{
}
