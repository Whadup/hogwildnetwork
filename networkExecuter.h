#pragma once

#include <vector>
#include <iostream>
#include "computeUnit.h"

class NetworkExecuter
{
public:
	ComputeUnit** layers;
	unsigned int numLayers;
	unsigned int numStartNodes;

	NetworkExecuter();
	~NetworkExecuter();
	NetworkExecuter(ComputeUnit* startNode);
	NetworkExecuter(ComputeUnit** startNodes,int numStartNodes);
	NetworkExecuter(ComputeUnit* startNode,ComputeUnit* endNode);
	NetworkExecuter* duplicate();

	void init();
	void forward();
	void backward();
	void gradientStep(double alpha);
};

NetworkExecuter::NetworkExecuter()
{

}

NetworkExecuter::~NetworkExecuter()
{
	for(int i=0;i<numLayers;i++)
		delete layers[i];
	delete[] layers;
}

NetworkExecuter::NetworkExecuter(ComputeUnit* startNode) : numStartNodes(1)
{
	std::vector<ComputeUnit*> tmp;
	tmp.push_back(startNode);
	int i=0;
	while(tmp[i]->noutputs>0)
	{
		ComputeUnit* suc = tmp[i]->successorDeep;
		if(suc!=0)
			tmp.push_back(suc);
		i++;
	}
	layers = new ComputeUnit*[tmp.size()];
	numLayers = tmp.size();
	for(int i=0;i<tmp.size();i++)
		layers[i]=tmp[i];
	std::cout << "network executer created with "<< numLayers << " nodes" << std::endl;
}

NetworkExecuter::NetworkExecuter(ComputeUnit* startNode,ComputeUnit* endNode) : numStartNodes(1)
{
	std::vector<ComputeUnit*> tmp;
	tmp.push_back(startNode);
	int i=0;
	while(tmp[i]->noutputs>0 && tmp[i]!=endNode)
	{
		if(tmp[i]->noutputs)
		{
			ComputeUnit* suc = tmp[i]->successorDeep;
			if(suc!=0)
				tmp.push_back(suc);
		}
		i++;
	}
	layers = new ComputeUnit*[tmp.size()];
	numLayers = tmp.size();
	for(int i=0;i<tmp.size();i++)
		layers[i]=tmp[i];
	std::cout << "network executer created with "<< numLayers << " nodes" << std::endl;
}

NetworkExecuter::NetworkExecuter(ComputeUnit** startNodes,int numStartNodes) : numStartNodes(numStartNodes)
{
	std::vector<ComputeUnit*> tmp;
	for(int i=0;i<numStartNodes;i++)
		tmp.push_back(startNodes[i]);
	int i=0;
	while(tmp[i]->noutputs>0)
	{
		
		if(tmp[i]->noutputs)
		{
			ComputeUnit* suc = tmp[i]->successorDeep;
			if(suc!=0)
				tmp.push_back(suc);
		}
		i++;
	}
	layers = new ComputeUnit*[tmp.size()];
	numLayers = tmp.size();
	for(int i=0;i<tmp.size();i++)
		layers[i]=tmp[i];
}

NetworkExecuter* NetworkExecuter::duplicate()
{
	// std::cout << "duplicating "<<numLayers << " nodes" << std::endl;
	NetworkExecuter* e = new NetworkExecuter();
	e->numLayers = numLayers;
	e->numStartNodes = numStartNodes;
	e->layers = new ComputeUnit*[numLayers];
	std::vector<ComputeUnit*> tmp;
	for(int i=0;i<numStartNodes;i++)
		tmp.push_back(layers[i]->duplicate());
	int i=0;
	while(i<numLayers && tmp[i]->noutputs>0)
	{
		if(tmp[i]->noutputs && i<numLayers-1)//for(int j=0;j<tmp[i]->noutputs;j++)
		{
			ComputeUnit* suc = layers[i]->successorDeep->duplicate();
			connectDepth(tmp[i],suc);
			if(suc!=0)
				tmp.push_back(suc);
		}
		i++;
	}
	for(int i=0;i<tmp.size();i++)
		e->layers[i]=tmp[i];
	return e;
}

void NetworkExecuter::init()
{
	for(int i=0;i<numLayers;i++)
		layers[i]->init();
}

void NetworkExecuter::forward()
{
	for(int i=0;i<numLayers;i++)
	{
		// std::cout << i << std::endl;
		layers[i]->forward();
	}
}

void NetworkExecuter::backward()
{	
	for(int i=numLayers-1;i>=0;i--)	
	{
		// std::cout << i << std::endl;
		layers[i]->backward();
	}
}

void NetworkExecuter::gradientStep(double alpha)
{	
	for(int i=numLayers-1;i>=0;i--)	
	{
		// std::cout << i << std::endl;
		layers[i]->gradientStep(alpha);
	}
}
