#must have
sudo mkdir -p /data/pfahler
sudo chown pfahler /data/pfahler
#sync the data that is needed
# mkdir -p /data/pfahler/classification/MNIST
# mkdir -p /data/pfahler/classification/decisiontreedatasets
# mkdir -p /data/pfahler/corpora/penn/processed
# mkdir -p /data/pfahler/wordEmbeddings/googleNews/
# mkdir -p /data/pfahler/relnet/processed_data/ahlusunnah/
mkdir -p /data/pfahler/relnet/processed_data/jesus/
# mkdir -p /data/pfahler/relnet/processed_data/jesus/subforum_185
# mkdir -p /data/pfahler/corpora/DeepmindQA
# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/corpora/penn/processed/ /data/pfahler/corpora/penn/processed
# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/classification/MNIST/ /data/pfahler/classification/MNIST
# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/classification/decisiontreedatasets/ /data/pfahler/classification/decisiontreedatasets

# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/wordEmbeddings/googleNews/ /data/pfahler/wordEmbeddings/googleNews
# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/relnet/processed_data/ahlusunnah/subforum_346-Fragen-zum-islamischen-Recht-und-ihre-Antworten.json.int /data/pfahler/relnet/processed_data/ahlusunnah/subforum_346-Fragen-zum-islamischen-Recht-und-ihre-Antworten.json.int
# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/relnet/processed_data/ahlusunnah/posts.json.int /data/pfahler/relnet/processed_data/ahlusunnah/posts.json.int
# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/corpora/DeepmindQA/dailymail.int /data/pfahler/corpora/DeepmindQA/dailymail.int
rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/relnet/processed_data/jesus/posts.json_paragraphEmbedding* /data/pfahler/relnet/processed_data/jesus/
# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/relnet/processed_data/jesus/subforum_185/subforum_185.json_paragraphEmbedding.int /data/pfahler/relnet/processed_data/jesus/subforum_185/subforum_185.json_paragraphEmbedding.int
# rsync -aP datajunkie@kimba.cs.tu-dortmund.de:/data/pfahler/relnet/processed_data/jesus/subforum_185.json_paragraphEmbedding.int /data/pfahler/relnet/processed_data/jesus/subforum_185.json_paragraphEmbedding.int
#run the software

# for dimension in 1 3 5 10 20 50
# # do
g++ -std=c++11 -g -O3 -fopenmp -lpthread -mtune=native -march=native -I libs/eigen/ -I . tests/skipgram.cpp -o main
# 	# echo $dimension
./main #| grep --line-buffered ^[0-9] | cut -f 1 | paste  - - | tail -n 1
# done
#sudo cp embeddings.csv /data/pfahler/relnet/processed_data/jesus/posts.json_embeddings.csv
#cp embeddings.csv /home/pfahler/posts.json_embeddings.csv
# sudo cp embeddings.csv /data/pfahler/corpora/DeepmindQA/
# cp embeddings.csv /home/pfahler/embeddings_dailymail.csv
# rsync -aP /data/pfahler/relnet/processed_data/jesus/subforum_185/embeddings.csv datakunkie@kimba.cs.tu-dortmund.de:/data/pfahler/relnet/processed_data/jesus/subforum_185/embeddings.csv

# for numtrees in 1 5 10 25; do
# 	for height in 5 6 7 8 9 10; do
# 		for dataset in MNIST LETTER USPS; do
# 			for n in 16 32 64 128; do
# 				for zeroone in true false; do
					
# 					b=`mktemp logXXXX`
# 					echo $numtrees>>"${b}1"
# 					echo $dataset>>"${b}1"
# 					echo $n >> "${b}1"
# 					echo $zeroone >> "${b}1"
# 					echo $height >> "${b}1"

# 					echo $numtrees>>"${b}2"
# 					echo $dataset>>"${b}2"
# 					echo $n >> "${b}2"
# 					echo $zeroone >> "${b}2"
# 					echo $height >> "${b}2"

# 					(
# 						a=`mktemp mainXXXX`
# 						macros="-DNSAMPLES=$n -DHEIGHT=$height -DZEROONE=$zeroone -DNUMTREES=$numtrees -D${dataset}"
# 						echo $macros
# 						g++ -std=c++11 -g -O3 $macros -lpthread -mtune=native -march=native -I libs/eigen/ -I . tests/reinforceDecisionTree.cpp -o $a
# 						./$a >> "${b}1"
# 					)&
# 					(
# 						a=`mktemp mainXXXX`
# 						macros="-DNSAMPLES=$n -DHEIGHT=$height -DZEROONE=$zeroone -DNUMTREES=$numtrees -D${dataset}"
# 						echo $macros
# 						g++ -std=c++11 -g -O3 $macros -lpthread -mtune=native -march=native -I libs/eigen/ -I . tests/qlearnDecisionTree.cpp -o $a
# 						./$a >> "${b}2"
# 					)&
# 				done
# 			done
# 		done
# 		wait
# 	done
# done
# cp log* /home/pfahler/
# docker exec s876cn07/pfahler-gridsearch4 sh -c "cd /experiment/;ls log*[12]|xargs -I{} sh -c 'echo {} && head -5 {} && tail -2 {}'" | grep --no-group-separator -B 7 "TEST-LOSS 0." | paste -d, - - - - - - - - | sed "s/log....1/reinforce/g" | sed "s/log....2/qlearning/g"  | sort -r -t, -k 8 > results.csv