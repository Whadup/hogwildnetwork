#pragma once
// #ifndef _LINEARLAYER_
// #define _LINEARLAYER_
#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
#include <random>
#include "dataInterfaces.h"
using Eigen::Matrix;

template<int vocabularySize>
class OneHotLayer: public ComputeUnit, public IntegerInput
{
public:
	OneHotLayer();
	virtual void forward();
	virtual void backwardGradientStep(double alpha);
	virtual void init();
	virtual ComputeUnit* duplicate();
	
};


template<int vocabularySize>
OneHotLayer<vocabularySize>::OneHotLayer() : ComputeUnit(0,0,1), IntegerInput()
{
	outputs[0]=MatrixXd(vocabularySize,1);
}

template<int vocabularySize>
void OneHotLayer<vocabularySize>::init()
{
}

template<int vocabularySize>
ComputeUnit* OneHotLayer<vocabularySize>::duplicate()
{
	// std::cout << "duplicating EmbeddingLayer" << std::endl;
	OneHotLayer<vocabularySize>* l = new OneHotLayer();
	return (ComputeUnit*)l;
}

template<int vocabularySize>
void OneHotLayer<vocabularySize>::forward()
{
	outputs[0]=Matrix<double,vocabularySize,1>::Zero();
	outputs[0](this->input,0)=1;
}

template<int vocabularySize>
void OneHotLayer<vocabularySize>::backwardGradientStep(double alpha)
{
	// MatrixXd* jac = &successors[0]->jacobians[0];	
	// // double delta = (*jac).sum();//jacobianW * (*jac);
	// W->col(input)-=alpha * (*jac);
	// for(int j=0;j<embeddingDimension;j++)
	// {
	// 	(*W)(j,input)-=alpha * (*jac)(j,0);
	// }

}
