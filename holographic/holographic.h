#include <Eigen/Dense>
#include <iostream>
#include <random>
#include <vector>
#include <stack>
#include "fft.h"
using Eigen::Matrix;
using Eigen::MatrixXd;
using Eigen::VectorXd;
using std::vector;


#define CHECK_TRACE 0

#ifndef NORMAL
#define NORMAL
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<> d(0,1/10.0);
std::normal_distribution<> initializer(0,0.1);
#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;
#endif 

template<int inputDimension,int internalDimension>
class Memory
{
public:
	//MatrixXd trace;
	Matrix<Complex,Eigen::Dynamic,1> traceFreq;
	MatrixXd basis;
	MatrixXd inver;
	Matrix<Complex,Eigen::Dynamic,1> LtraceFreq;
	std::stack<Matrix<Complex,Eigen::Dynamic,1>> traces;
	Memory()
	{
		//trace = MatrixXd::Zero(internalDimension,1);
		traceFreq = Matrix<Complex,Eigen::Dynamic,1>::Zero(internalDimension,1);
		traces.push(traceFreq);
		LtraceFreq = Matrix<Complex,Eigen::Dynamic,1>::Zero(internalDimension,1);
	}

	void init()
	{
		basis = MatrixXd::Random(internalDimension,inputDimension);
		for(int i=0;i<internalDimension;i++)
			for(int j = 0;j<inputDimension;j++)
				basis(i,j) = d(gen);
		basis.topLeftCorner(inputDimension,inputDimension).setIdentity();
		inver = (basis.transpose() * basis).inverse() * basis.transpose();
	}

	void reset(Matrix<Complex,Eigen::Dynamic,1>& tf)
	{
		traceFreq = tf;
		traces.pop();
		if(!traces.empty())
			std::cerr<<"stack non-empty at beginning of timestep" << std::endl;
		traces.push(traceFreq);
		LtraceFreq = Matrix<Complex,Eigen::Dynamic,1>::Zero(internalDimension,1);
	}

	void setZero()
	{
		traceFreq = Matrix<Complex,Eigen::Dynamic,1>::Zero(internalDimension,1);
		traces.pop();
		if(!traces.empty())
			std::cerr<<"stack non-empty at beginning of timestep" << std::endl;
		traces.push(traceFreq);
		LtraceFreq = Matrix<Complex,Eigen::Dynamic,1>::Zero(internalDimension,1);
	}

	Eigen::VectorXd read(MatrixXd key)
	{
		MatrixXd kk = (basis * key);
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> k = kk.cast<Complex>();
		fft(k);
		k = k.conjugate();
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> readOut;
		readOut = (k.array() * traceFreq.array()).matrix();
		readOut = readOut.conjugate();
		fft(readOut);
		readOut = (readOut.conjugate().array()/(1.0*internalDimension)).matrix();
		return inver * readOut.real();
	}

	void write(VectorXd& key, VectorXd& value)
	{
		MatrixXd kk = (basis * key);
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> k = kk.cast<Complex>();
		MatrixXd vv = basis * value;
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> v = vv.cast<Complex>();
		fft(k);
		fft(v);
		MatrixXd s2(internalDimension,1);
		k = (k.array() * v.array()).matrix();
		traceFreq += k;
		traces.push(traceFreq);
	}

	VectorXd deltaRead(VectorXd& key,MatrixXd& LLv)
	{
		MatrixXd kk = (basis * key);
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> k = kk.cast<Complex>();
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> L = inver.transpose() * LLv.cast<Complex>();
		fft(k);
		fft(L);
		LtraceFreq += (L.array() * k.array()).matrix();
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> Lkk = (L.array() * traces.top().array()).matrix();
		Lkk = Lkk.conjugate();
		fft(Lkk);
		Lkk = (Lkk.conjugate().array()/(1.0*internalDimension)).matrix();
		
		return basis.transpose() * Lkk.real();

	}

	VectorXd deltaWrite1(VectorXd& key, VectorXd& value)
	{
		MatrixXd vv = basis * value;
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> v = vv.cast<Complex>();
		fft(v);
		v.conjugate();
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> Lvv = (LtraceFreq.array() * v.array()).matrix();
		//inverse fft
		Lvv = Lvv.conjugate();
		fft(Lvv);
		Lvv = (Lvv.conjugate().array()/(1.0*internalDimension)).matrix();
		//end inverse fft
		return basis.transpose() * Lvv.real();
	}

	VectorXd deltaWrite2(VectorXd& key, VectorXd& value)
	{
		// note this line must only be in write2, do not copy into both deltawrite methods
		traces.pop();
		MatrixXd kk = basis * key;
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> k = kk.cast<Complex>();
		fft(k);
		k.conjugate();
		Matrix<Complex,Eigen::Dynamic,Eigen::Dynamic> Lkk = (LtraceFreq.array() * k.array()).matrix();
		Lkk = Lkk.conjugate();
		fft(Lkk);
		Lkk = (Lkk.conjugate().array()/(1.0*internalDimension)).matrix();
		return basis.transpose() * Lkk.real();
	}
};


// int main()
// {
// 	int const dim = 64;
// 	int const cap = 5;
// 	Memory<dim,4096> memory;
	
	
// 	vector<VectorXd> ks;
// 	vector<VectorXd> vs;

// 	for (int i=0;i<cap;i++)
// 	{
// 		VectorXd k = VectorXd::Random(dim,1);
// 		VectorXd v = VectorXd::Random(dim);
// 		// k = MatrixXd::Random(dim,1);
// 		k /= k.norm();
// 		// v = MatrixXd::Random(dim,1);
// 		v/=v.norm();
// 		ks.push_back(k);
// 		vs.push_back(v);
// 		// for(int i=0;i<dim;i++)
// 		// {
// 		// 	k(i,0) = d(gen);
// 		// 	v(i,0) = d(gen);
// 		// }
// //		std::cout << std::endl << v.transpose() << std::endl;
// 		memory.write(k,v);
// 	}

// 	for(int j=0;j<cap;j++)
// 	{
// 		MatrixXd k = ks[j];
// 		// std::cout << memory.trace.transpose() << std::endl;
// 		// MatrixXd e = memory.read(k).transpose();
// 		// std::cout << e/e.norm() << std::endl << e << std::endl;
// 		for(int i=0;i<cap;i++)
// 		{
// 			VectorXd v = vs[i];
// 			VectorXd vv = memory.read(k);
// 			vv/=vv.norm();
// 			std::cout << v.dot(vv) << "/" << (v-vv).lpNorm<2>() << "\t";
// 		}
// 		std::cout << std::endl;
// 	}
	
// 	return 0;
// }


