
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<double> initializer(0,0.1);

#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);

#include "computeUnit.h" 
template <int inputDimension, int outputDimension>
class LSTM: public ComputeUnit
{
public:
EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,outputDimension,1> i;
	Matrix<double,outputDimension,inputDimension>* Uix = 0;
	Matrix<double,outputDimension,outputDimension>* Uic = 0;
	Matrix<double,outputDimension,outputDimension>* Uih = 0;
	Matrix<double,outputDimension,1>* bi = 0;
	Matrix<double,outputDimension,1> tmp2;
	Matrix<double,outputDimension,1> u;
	Matrix<double,outputDimension,inputDimension>* Uux = 0;
	Matrix<double,outputDimension,outputDimension>* Uuc = 0;
	Matrix<double,outputDimension,1>* bu = 0;
	Matrix<double,outputDimension,1> f;
	Matrix<double,outputDimension,inputDimension>* Ufx = 0;
	Matrix<double,outputDimension,outputDimension>* Ufc = 0;
	Matrix<double,outputDimension,outputDimension>* Ufh = 0;
	Matrix<double,outputDimension,1>* bf = 0;
	Matrix<double,outputDimension,1> o;
	Matrix<double,outputDimension,inputDimension>* Uox = 0;
	Matrix<double,outputDimension,outputDimension>* Uoct = 0;
	Matrix<double,outputDimension,outputDimension>* Uoh = 0;
	Matrix<double,outputDimension,1>* bo = 0;
	Matrix<double,outputDimension,1> out;
	Matrix<double,outputDimension,1> tmp3;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> h;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> c;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,outputDimension,1> tmp1;
	Matrix<double,outputDimension,1> ht;
	Matrix<double,outputDimension,1> ct;
	LSTM();
	virtual void init();
	virtual void forward();
	virtual void backwardGradientStep(double alpha);
	virtual ComputeUnit* duplicate();
	
};
		
template<int inputDimension, int outputDimension>
LSTM<inputDimension, outputDimension>::LSTM() : ComputeUnit(1,2,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Zero();
	outputs[1] = Matrix<double,outputDimension,1>::Zero();
	outputs[2] = Matrix<double,outputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,outputDimension,1>::Zero();
	jacobians[2] = Matrix<double,outputDimension,1>::Zero();
}

template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::init() 
{
	Uic = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uic);
	Uoh = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uoh);
	bu = new Matrix<double,outputDimension,1>();
	gaussianInitialization(bu);
	Uox = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uox);
	Uix = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uix);
	Ufx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Ufx);
	Uih = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uih);
	bo = new Matrix<double,outputDimension,1>();
	gaussianInitialization(bo);
	Ufh = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Ufh);
	Ufc = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Ufc);
	bf = new Matrix<double,outputDimension,1>();
	gaussianInitialization(bf);
	bi = new Matrix<double,outputDimension,1>();
	gaussianInitialization(bi);
	Uuc = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uuc);
	Uoct = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uoct);
	Uux = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uux);
}
template<int inputDimension, int outputDimension>
ComputeUnit* LSTM<inputDimension, outputDimension>::duplicate() 
{
	LSTM<inputDimension, outputDimension>* l = new LSTM<inputDimension, outputDimension>();
	l->Uic = Uic;
	l->Uoh = Uoh;
	l->bu = bu;
	l->Uox = Uox;
	l->Uix = Uix;
	l->Ufx = Ufx;
	l->Uih = Uih;
	l->bo = bo;
	l->Ufh = Ufh;
	l->Ufc = Ufc;
	l->bf = bf;
	l->bi = bi;
	l->Uuc = Uuc;
	l->Uoct = Uoct;
	l->Uux = Uux;
	return (ComputeUnit*) l;
}
template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::forward()
{
	x = *inputs[0];
	c = *inputs[1];
	h = *inputs[2];
	i = (*Uix) * x + (*Uic) * c + (*Uih) * h + *bi;
	i = (Matrix<double,outputDimension,1>::Constant(1) + (-i).array().exp().matrix()).array().cwiseInverse().matrix();
	u = (*Uux) * x + (*Uuc) * c + *bu;
	u = u.array().tanh().matrix();
	f = (*Ufx) * x + (*Ufc) * c + (*Ufh) * h + *bf;
	f = (Matrix<double,outputDimension,1>::Constant(1) + (-f).array().exp().matrix()).array().cwiseInverse().matrix();
	tmp1 = (f.array() * c.array()).matrix();
	tmp2 = (i.array() * u.array()).matrix();
	ct = tmp1 + tmp2;
	outputs[1] = ct;
	o = (*Uox) * x + (*Uoct) * ct + (*Uoh) * h + *bo;
	o = (Matrix<double,outputDimension,1>::Constant(1) + (-o).array().exp().matrix()).array().cwiseInverse().matrix();
	tmp3 = ct;
	tmp3 = tmp3.array().tanh().matrix();
	ht = (tmp3.array() * o.array()).matrix();
	outputs[2] = ht;
	outputs[0] = ht;
}
template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::backwardGradientStep(double alpha)
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lct = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lht = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp2 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lu = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lo = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp3 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp1 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Li = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lf = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lh = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lc = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) Lout += successorDeep->jacobians[0]; // TODO!!!
	if(successorTime!=0) Lct += successorTime->jacobians[1]; // TODO!!!
	if(successorTime!=0) Lht += successorTime->jacobians[2]; // TODO!!!
	Lht += Lout;
	Ltmp1 += Lct;
	Ltmp2 += Lct;
	{
		Matrix<double,outputDimension,1> tmp = o;
		Ltmp3 += Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp) * Lht;
	}
	{
		Matrix<double,outputDimension,1> tmp = tmp3;
		Lo += Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp) * Lht;
	}
	{
		Matrix<double,outputDimension,1> tmp = u;
		Li += Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp) * Ltmp2;
	}
	{
		Matrix<double,outputDimension,1> tmp = i;
		Lu += Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp) * Ltmp2;
	}
	Eigen::DiagonalMatrix<double,outputDimension,outputDimension> tmpu(Matrix<double,outputDimension,1>::Constant(1)-u.array().cwiseAbs2().matrix());
	Lx += Uux->transpose() * tmpu * Lu;
	Lc += Uuc->transpose() * tmpu * Lu;
	Eigen::DiagonalMatrix<double,outputDimension,outputDimension> tmpo((o.array() * (Matrix<double,outputDimension,1>::Constant(1)-o).array()).matrix());
	Lx += Uox->transpose() * tmpo * Lo;
	Lct += Uoct->transpose() * tmpo * Lo;
	Lh += Uoh->transpose() * tmpo * Lo;
	Eigen::DiagonalMatrix<double,outputDimension,outputDimension> tmptmp3(Matrix<double,outputDimension,1>::Constant(1)-tmp3.array().cwiseAbs2().matrix());
	Lct += tmptmp3* Ltmp3;
	{
		Matrix<double,outputDimension,1> tmp = c;
		Lf += Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp) * Ltmp1;
	}
	{
		Matrix<double,outputDimension,1> tmp = f;
		Lc += Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp) * Ltmp1;
	}
	Eigen::DiagonalMatrix<double,outputDimension,outputDimension> tmpi((i.array() * (Matrix<double,outputDimension,1>::Constant(1)-i).array()).matrix());
	Lx += Uix->transpose() * tmpi * Li;
	Lc += Uic->transpose() * tmpi * Li;
	Lh += Uih->transpose() * tmpi * Li;
	Eigen::DiagonalMatrix<double,outputDimension,outputDimension> tmpf((f.array() * (Matrix<double,outputDimension,1>::Constant(1)-f).array()).matrix());
	Lx += Ufx->transpose() * tmpf * Lf;
	Lc += Ufc->transpose() * tmpf * Lf;
	Lh += Ufh->transpose() * tmpf * Lf;
	jacobians[2] = Lh;
	jacobians[1] = Lc;
	jacobians[0] = Lx;
	*Uux -= alpha * tmpu * Lu * x.transpose();
	*Uuc -= alpha * tmpu * Lu * c.transpose();
	*bu -= alpha * tmpu * Lu;
	*Uox -= alpha * tmpo * Lo * x.transpose();
	*Uoct -= alpha * tmpo * Lo * ct.transpose();
	*Uoh -= alpha * tmpo * Lo * h.transpose();
	*bo -= alpha * tmpo * Lo;
	*Uix -= alpha * tmpi * Li * x.transpose();
	*Uic -= alpha * tmpi * Li * c.transpose();
	*Uih -= alpha * tmpi * Li * h.transpose();
	*bi -= alpha * tmpi * Li;
	*Ufx -= alpha * tmpf * Lf * x.transpose();
	*Ufc -= alpha * tmpf * Lf * c.transpose();
	*Ufh -= alpha * tmpf * Lf * h.transpose();
	*bf -= alpha * tmpf * Lf;
}
