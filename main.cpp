// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include "linearlayer.h"
// #include "relulayer.h"
#include "oneHotLayer.h"
#include "embeddingLayer.h"
// #include "embedAndReduceLayer.h"
#include "crossEntropyLoss.h"
#include "softmaxLayer.h"
#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"
#include "recurrentWrapper.h"
#include <stdlib.h>
#include <vector>
#include <string>
//#include "lstmgen2.h"
#include "loadBinarySparse.h"
#include "associativeMemory.h"
#include "lstm2.h"

using Eigen::MatrixXd;
//#define EIGEN_DONT_PARALLELIZE

#include <fenv.h>

int main()
{	
	feraiseexcept(FE_ALL_EXCEPT); 
	Eigen::initParallel();
	std::cout << std::thread::hardware_concurrency() << std::endl;
	
	const int numberOfLabels = 2;
	const int hiddenDimension = 4;
	
	const int outputDimension = 6;
	Matrix<double,outputDimension,outputDimension> complex1 = Matrix<double,outputDimension,outputDimension>::Zero();
	complex1.topLeftCorner(outputDimension>>1,outputDimension>>1).setIdentity();
	complex1.bottomLeftCorner(outputDimension>>1,outputDimension>>1).setIdentity();
	Matrix<double,outputDimension,outputDimension> complex2 = Matrix<double,outputDimension,outputDimension>::Zero();
	complex2.topRightCorner(outputDimension>>1,outputDimension>>1) = - Matrix<double,(outputDimension>>1),(outputDimension>>1)>::Identity();
	complex2.bottomRightCorner(outputDimension>>1,outputDimension>>1).setIdentity();
	std::cout << complex2 << std::endl;	
	// LSTM<hiddenDimension,hiddenDimension> lstm2;
	EmbeddingLayer<1618816,32> em;
	LSTM2<32,16> lstm;
	// LSTM2<24,16> lstm2;
	//LSTM<16,16> lstm3;
	connectDepth((ComputeUnit&)em, 		(ComputeUnit&)lstm);
	// connectDepth((ComputeUnit&)lstm, 		(ComputeUnit&)lstm2);
	//connecty((ComputeUnit&)lstm2, 		(ComputeUnit&)lstm3);
	//connecty((ComputeUnit&)rnn, 		(ComputeUnit&)rnn2);


	RecurrentWrapper<16> wrapper(new NetworkExecuter(&em,&lstm));
	
	//ReLuLayer<32,128> relu;
	// ReLuLayer<64,32> relu2;
	LinearLayer<16,numberOfLabels> lin2;
	SoftmaxLayer<numberOfLabels> softmax;
	CrossEntropyLoss<numberOfLabels> loss;
	std::cout << "Netzwerk erstellt" << std::endl;
	
	
	//connecty((ComputeUnit&)wrapper,	(ComputeUnit&)relu);
	//connecty((ComputeUnit&)relu,	(ComputeUnit&)lin2);
	// connecty((ComputeUnit&)relu2,	(ComputeUnit&)lin2);
	// connecty((ComputeUnit&)lin2,	(ComputeUnit&)softmax);
	connectDepth((ComputeUnit&)wrapper,	(ComputeUnit&) lin2);
	connectDepth((ComputeUnit&)lin2,    (ComputeUnit&)softmax);
	connectDepth((ComputeUnit&)softmax,	(ComputeUnit&)loss);
	std::cout << "Netzwerk erstellt" << std::endl;
	
	std::cout << (1.0 + Matrix<double,3,3>::Zero().array()).matrix() << std::endl;

	
	

	// std::cout << &wrapper << std::endl << &em << std::endl << &rnn << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&wrapper);
	
	
	const std::string file = "/data/d5/pfahler/movies/moviesLabeled.sparse";

	std::stringstream ws(file);


	int iter  = 0;
	double bytes = 0;


	double start = getTime();
	std::vector<int> tmp;
	std::vector<SeqExample*> dataset;
	int vocSize = 0;
	std::ifstream docFile(file);
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		//std::cout << line << std::endl;
		//std::cout << iter << std::endl;
		if(line.size())
		{
			int* a = count(line, tmp,vocSize);
			SeqExample* e = new SeqExample();
			e->y = a[1];
			e->l = a[0];
			e->input = &a[2];
			dataset.push_back(e);
		}

		if(iter%10000 == 0) 
		{
				// std::cout << vocSize << std::endl;
				std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ") " << std::flush;
				if(iter>=1000000) break;
		}
		iter++;
	}
	docFile.close();
	std::cout << "DONE" << vocSize <<  std::endl;
	std::cout << vocSize << std::endl;
//	std::cout << "SUM = " << sum << std::endl;

//	std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ")" << std::endl;

	MemoryDataSource<SeqExample> d(dataset,1);
	d.setNumberOfEpoches(1);
	
	const double stepsize = 0.00100;

	std::cout << "StepSize: "<<stepsize << std::endl;

	// ((RecurrentWrapper<32>*)f->layers[0])->l = 6;
	// ((RecurrentWrapper<32>*)f->layers[0])->input = new int[6]{1,2,3,4,5,6};
	// ((CrossEntropyLoss<5>*)f->layers[f->numLayers-1])->y = 1;
	// f->forward(); f->backward(); f->gradientStep(0.0001);

	
	HogwildManager<SeqExample,1>* h = new HogwildManager<SeqExample,1>(e,&d);
	h->go(stepsize);
	
}