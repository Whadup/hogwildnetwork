// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include "linearlayer.h"
#include "relulayer.h"
#include "embeddingLayer.h"
#include "embedAndReduceLayer.h"
#include "crossEntropyLoss.h"
#include "softmaxLayer.h"
#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"
#include "recurrentWrapper.h"
#include "rnn.h"
#include <stdlib.h>
#include <vector>
#include <string>
#include "lstm.h"
#include "loadBinarySparse.h"

using Eigen::MatrixXd;
#define EIGEN_DONT_PARALLELIZE



int main()
{	
	Eigen::initParallel();
	std::cout << std::thread::hardware_concurrency() << std::endl;
	EmbeddingLayer<1618816,32> em;

	RNN<32,16,16> rnn;
	RNN<16,16,5> rnn2;
	connecty((ComputeUnit&)em, 		(ComputeUnit&)rnn);
	connecty((ComputeUnit&)rnn, 		(ComputeUnit&)rnn2);

	RecurrentWrapper<32> wrapper(new NetworkExecuter(&em,&rnn2));
	//ReLuLayer<32,128> relu;
	// ReLuLayer<64,32> relu2;
	//LinearLayer<128,5> lin2;
	SoftmaxLayer<5> softmax;
	CrossEntropyLoss<5> loss;
	std::cout << "Netzwerk erstellt" << std::endl;
	
	
	//connecty((ComputeUnit&)wrapper,	(ComputeUnit&)relu);
	//connecty((ComputeUnit&)relu,	(ComputeUnit&)lin2);
	// connecty((ComputeUnit&)relu2,	(ComputeUnit&)lin2);
	// connecty((ComputeUnit&)lin2,	(ComputeUnit&)softmax);
	connecty((ComputeUnit&)wrapper,	(ComputeUnit&)softmax);
	connecty((ComputeUnit&)softmax,	(ComputeUnit&)loss);

	// std::cout << &wrapper << std::endl << &em << std::endl << &rnn << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&wrapper);
	e->init();
	// wrapper.l = 6;
	// wrapper.input = new int[6]{1,2,3,4,5,6};
	// loss.y=1;
	// e->forward();
	// e->backward();
	// e->gradientStep(0.01);
	// e->forward();
	// e->backward();
	// e->gradientStep(0.01);
// 	// std::cout << e << std::endl;
	const std::string file = "/data/d5/pfahler/movies/moviesLabeled.sparse";

	std::stringstream ws(file);


	int iter  = 0;
	double bytes = 0;


	double start = getTime();
	std::vector<int> tmp;
	std::vector<int*> dataset;
	int vocSize = 0;
	std::ifstream docFile(file);
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		//std::cout << line << std::endl;
		//std::cout << iter << std::endl;
		if(line.size())
			dataset.push_back(count(line, tmp,vocSize));

		if(iter%10000 == 0) 
		{
				// std::cout << vocSize << std::endl;
				std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ") " << std::flush;
				//if(iter>0) break;
		}
		iter++;
	}
	docFile.close();
	std::cout << "DONE" << vocSize <<  std::endl;
//	std::cout << "SUM = " << sum << std::endl;

//	std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ")" << std::endl;

	MemoryDataSource<int*> d(dataset,1);
	d.setNumberOfEpoches(1);
	
	// ((RecurrentWrapper<32>*)f->layers[0])->l = 6;
	// ((RecurrentWrapper<32>*)f->layers[0])->input = new int[6]{1,2,3,4,5,6};
	// ((CrossEntropyLoss<5>*)f->layers[f->numLayers-1])->y = 1;
	// f->forward(); f->backward(); f->gradientStep(0.0001);

	HogwildManager<1>* h = new HogwildManager<1>(e,&d);
	h->go(0.01);
	
	
}