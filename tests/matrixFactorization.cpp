#include "embeddingLayer.h"
#include "sigmoid2ndOrder.h"
#include <stdlib.h>
#include "loadBinarySparse.h"
#include "loadData.h"
#include "dummyJacobian.h"
#include "duplicate.h"
#include "hierarchicalSoftmaxCrossEntropy.h"
#include "bowWrapper.h"
#include "sum.h"
// #include <omp.h>
#include <vector>

#include <signal.h>
#include <stdlib.h>
#include <stdio.h>


bool stopTraining = 0;

void my_handler(int s){
	stopTraining = 1;
	std::cout << std::endl;
}


#define JESUS

#ifdef AMAZONFINEFOODS
	#define USERDIMENSION 256060
	#define THREADDIMENSION 74259
	#define WORDDIMENSION 10036
#endif
#ifdef JESUS
	#define USERDIMENSION 11559
	#define THREADDIMENSION 22838
	#define WORDDIMENSION 5002
#endif

int main()
{
	signal (SIGINT,my_handler);
	#ifdef AMAZONFINEFOODS
	const int dimension = 5;
	#endif
	#ifdef JESUS
	const int dimension = 32;
	#endif

	const int predictDocuments = true;
	const int predictActions = true;
	// HierarchicalSoftmaxCrossEntropy<dimension,5002> softmax;
	HierarchicalSoftmaxCrossEntropy<dimension,WORDDIMENSION> softmax;
	BowWrapper<dimension,5> bow(new NetworkExecuter(&softmax));
	Sum<dimension> sum;


	EmbeddingLayer<USERDIMENSION,dimension> users;
	// EmbeddingLayer<11559,dimension> users;
	EmbeddingLayer<THREADDIMENSION,dimension> threads;
	// EmbeddingLayer<22838,dimension> threads;
	Duplicate<dimension> mp1;
	Duplicate<dimension> mp2;

	Sigmoid2ndOrder<dimension> predictor;
	DummyJacobian<1> loss;

	users.init();
	threads.init();
	predictor.init();
	bow.init();
	loss.jacobians[0] = MatrixXd::Zero(1,1);

	connectDepth11((ComputeUnit*) 	&users,		(ComputeUnit*) 	&mp1);
	connectDepth11((ComputeUnit*) 	&threads,	(ComputeUnit*) 	&mp2);
	connectDepth11((ComputeUnit*)	&mp1,		(ComputeUnit*)	&predictor);
	connectDepth12((ComputeUnit*)	&mp2,		(ComputeUnit*)	&predictor);
	connectDepth11((ComputeUnit*)	&predictor,	(ComputeUnit*)	&loss);
	connectDepth11((ComputeUnit*)	&sum,		(ComputeUnit*)	&bow);
	std::vector<int> data_users;
	std::vector<int> data_threads;
	std::vector<int> data_y;
	std::vector<int> data_reference;
	std::vector<IntBowExample*> data_documents;
	if(predictDocuments)
	{
		int iter  = 0;
		double bytes = 0;
		double start = getTime();
		std::vector<int> tmp;
		int vocSize;
		
		int ii=0;
		#ifdef AMAZONFINEFOODS
		std::ifstream docFile("/data/pfahler/classification/AmazonFineFoods/documents.sparse");
		#endif
		#ifdef JESUS
		std::ifstream docFile("../relnetprocessing/jesusposts.sparse");
		#endif
		while(!docFile.eof()){
			std::string line;
			getline(docFile, line);
			bytes += line.size();
			if(line.size())
			{		
				//std::cout << line << std::endl;
				IntBowExample* e = new IntBowExample;
				e->y = parseSparse<false>(line,tmp,vocSize,e->ll);
				// for(int i=0;i<e->ll;i++)
				// 	std::cout << e->y[i] << " ";
				e->input = ii++;
				//std::cout << e->input << std::endl;
				
				// for(int i=0;i<100;i++)
				data_documents.push_back(e);
				// if(ii>10000)
				//  	break;
			}
			else
			{
				IntBowExample* e = new IntBowExample;
				e->y = 0;
				e->ll = 0;//parseSparse<false>(line,tmp,vocSize,e->ll);
				// for(int i=0;i<e->ll;i++)
				// 	std::cout << e->y[i] << " ";
				// e->input = ii++;
				//std::cout << e->input << std::endl;
				
				// for(int i=0;i<100;i++)
				data_documents.push_back(e);
			}
		}
		docFile.close();
	}
	{
		#ifdef AMAZONFINEFOODS
		std::ifstream is("/data/pfahler/classification/AmazonFineFoods/actions.csv");
		#endif
		#ifdef JESUS
		std::ifstream is("../relnetprocessing/test.txt");
		#endif
		int d;

		while (!is.eof())
		{
			is >> d;
			is.ignore(1);
			data_users.push_back(d);
			is >> d;
			is.ignore(1);
			data_threads.push_back(d);
			is >> d;
			is.ignore(1);
			data_y.push_back(d);
			is >> d;
			is.ignore(1);
			data_reference.push_back(d);
		}
		is.close();
	}
	std::cout << data_y.size() << " " << data_documents.size() << std::endl;
	int c = 0;
	double avg = 0;
	double acc = 0;
	double perplexity=0;
	double total = 0;
	double ratio = 0;

	double stepSize = 0.1;

	for(int i=0;i<500;i++)
	{
		if(stopTraining) break;
		for(int jj=0;jj<data_users.size();jj++)
		{
			if(stopTraining) break;
			int j = rand() % data_users.size();
			if(j%10 == 9)
				continue;
			users.input = data_users[j];
			users.forward();
			threads.input = data_threads[j];
			threads.forward();
			mp1.forward();
			mp2.forward();
			if(predictActions)
				predictor.forward();



			if(data_y[j]>0)
			{
				ratio += 1;
				if(predictDocuments)
				{
					mp1.successorDeep2 = (ComputeUnit*) &sum;
					mp2.successorDeep2 = (ComputeUnit*) &sum;
					sum.inputs[0] = &mp1.outputs[1];
					sum.inputs[1] = &mp2.outputs[1];
					sum.forward();
					bow.y = data_documents[data_reference[j]]->y;
					bow.ll = data_documents[data_reference[j]]->ll;
					bow.forward();
					// bow.jacobians[0]*4;
					perplexity+=bow.loss();
					total += bow.weight();
					sum.backward();
					bow.gradientStep(stepSize);
				}
				if(predictActions)
				{
					if(predictor.outputs[0](0,0)>0.5)
					{
						acc+=1;
					}
					if(predictor.outputs[0](0,0)<1e-5)
						predictor.outputs[0](0,0)=1e-5;
					avg += -log(predictor.outputs[0](0,0));
					loss.jacobians[0](0,0) = -1.0/predictor.outputs[0](0,0);
				}
			}
			else
			{
				if(predictActions)
				{
					if(predictor.outputs[0](0,0)<0.5)
					{
						acc+=1;
					}
					mp1.successorDeep2 = 0;
					mp2.successorDeep2 = 0;
					if(1-predictor.outputs[0](0,0)<1e-5)
						predictor.outputs[0](0,0)=1-1e-5;
					avg += -log(1-predictor.outputs[0](0,0));
					loss.jacobians[0](0,0) = 1.0/(1-predictor.outputs[0](0,0));
				}
			}
			if(predictActions)
			{
				predictor.backward(); 
				predictor.gradientStep(stepSize);
			}
			
			mp1.backward();
			mp2.backward();
			users.backward();
			threads.backward();

			
			users.gradientStep(stepSize);
			threads.gradientStep(stepSize);
			

			if(c % 100000 == 99999)
			{
				std::cout << (avg / 100000.0) << " " << (acc / 100000.0) << " " << (ratio / 100000.0) << " " << perplexity/total << std::endl;
				
				avg = 0;
				acc = 0;
				ratio = 0;
				total = 0;
				perplexity = 0;
			}
			c++;
		}
		if(i%10==9)
		{
			avg = 0.0;
			acc = 0;
			ratio = 0;
			int a=0;
			int b=0;
			int tp=0;
			int tn=0;
			int fp=0;
			int fn=0;

			c = 0;
			for(int j=0;j<data_users.size();j++)
			{
				if(j%10 == 9)
				{		
					users.input = data_users[j];
					users.forward();
					threads.input = data_threads[j];
					threads.forward();
					mp1.forward();
					mp2.forward();
					predictor.forward();

					if(data_y[j])
					{
						ratio++;
						a++;
						if(predictor.outputs[0](0,0)<1e-5)
							predictor.outputs[0](0,0)=1e-5;
						if(predictor.outputs[0](0,0)>0.5)
						{	
							acc+=1;
							tp++;
						}
						else
							fp++;
						avg += -log(predictor.outputs[0](0,0));
					}
					else
					{
						b++;
						if(1-predictor.outputs[0](0,0)<1e-5)
							predictor.outputs[0](0,0)=1-1e-5;
						if(predictor.outputs[0](0,0)<0.5)
						{
							acc+=1;
							tn++;
						}
						else
							fn++;
						avg += -log(1-predictor.outputs[0](0,0));
					}
					c++;
				}
			}
			std::cout << "TEST-LOSS " << (avg / c) << std::endl;
			std::cout << "TEST-ACCURACY " << (acc / c) << std::endl;
			std::cout << "TEST-CLASS-RATIO " << (ratio / c) << " " << a << ":" << b << std::endl;
			std::cout << tp <<"\t" << fn  << std::endl;
			std::cout << fp <<"\t" << tn <<  std::endl;
			if(i>250)
				stepSize = 0.01;
		}
		std::cout << "END OF EPOCHE ("<< i << ")" << std::endl;
		std::ofstream userEmbeddings ("user_embeddings.csv");
		for(int i=0;i<11559;i++)
		{	
			userEmbeddings << i << ",";
			for(int j=0;j<dimension;j++)
			{
				if(j>0)
					userEmbeddings << ",";	
				userEmbeddings << (*users.W)(j,i);
			}
			userEmbeddings<<std::endl;
		} 
		userEmbeddings.close();

		std::ofstream threadEmbeddings ("thread_embeddings.csv");
		for(int i=0;i<22838;i++)
		{
			threadEmbeddings << i << ",";
			for(int j=0;j<dimension;j++)
			{
				if(j>0)
					threadEmbeddings << ",";	
				threadEmbeddings << (*threads.W)(j,i);
			}
			threadEmbeddings<<std::endl;
		}
		threadEmbeddings.close();
	}
	avg = 0.0;
	acc = 0;
	ratio = 0;
	int a=0;
	int b=0;
	int tp=0;
	int tn=0;
	int fp=0;
	int fn=0;

	c = 0;
	for(int j=0;j<data_users.size();j++)
	{
		if(j%10 == 9)
		{		
			users.input = data_users[j];
			users.forward();
			threads.input = data_threads[j];
			threads.forward();
			mp1.forward();
			mp2.forward();
			predictor.forward();

			if(data_y[j])
			{
				ratio++;
				a++;
				if(predictor.outputs[0](0,0)<1e-5)
					predictor.outputs[0](0,0)=1e-5;
				if(predictor.outputs[0](0,0)>0.5)
				{	
					acc+=1;
					tp++;
				}
				else
					fp++;
				avg += -log(predictor.outputs[0](0,0));
			}
			else
			{
				b++;
				if(1-predictor.outputs[0](0,0)<1e-5)
					predictor.outputs[0](0,0)=1-1e-5;
				if(predictor.outputs[0](0,0)<0.5)
				{
					acc+=1;
					tn++;
				}
				else
					fn++;
				avg += -log(1-predictor.outputs[0](0,0));
			}
			c++;
		}
	}
	std::cout << "TEST-LOSS " << (avg / c) << std::endl;
	std::cout << "TEST-ACCURACY " << (acc / c) << std::endl;
	std::cout << "TEST-CLASS-RATIO " << (ratio / c) << " " << a << ":" << b << std::endl;
	std::cout << tp <<"\t" << fn  << std::endl;
	std::cout << fp <<"\t" << tn <<  std::endl;

	

}