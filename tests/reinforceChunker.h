#pragma once
#include <random>
#include <vector>
#include <tuple>
#include "embeddingLayer.h"
#include "computeUnit.h"
#include "hierarchicalSoftmaxCrossEntropy.h"
#include "linearLayer2.h"
#include "softmaxLayer.h"
#include "rnn2.h"
#include "duplicate.h"
#include "bowWrapper.h"
#include "networkExecuter.h"
#include "holographic/holographic.h"
#include "dummyJacobian.h"
#include "dataInterfaces.h"

std::mt19937 samplergen;
std::uniform_real_distribution<> uniform(0,1);

class ReinforceChunker
{
public:
	static const int voc = 10000;
	static const int dimension = 100;
	Memory<dimension,256>* memory;
	RNN<dimension,dimension>* rnn;
	EmbeddingLayer<voc,dimension>* em;
	Duplicate<dimension>* dup;
	LinearLayer2<dimension,dimension,2>* lin;
	SoftmaxLayer<2>* softmax;
	HierarchicalSoftmaxCrossEntropy<dimension,voc>* output; 
	BowWrapper<dimension>* bow;
	vector<VectorXd> keys;
	vector<VectorXd> gkeys;



	void init()
	{
		memory = new Memory<dimension,256>;
		dup = new Duplicate<dimension>;
		em = new EmbeddingLayer<voc,dimension>;
		rnn = new RNN<dimension,dimension>;
		lin = new LinearLayer2<dimension,dimension,2>;
		softmax = new SoftmaxLayer<2>;
		output = new HierarchicalSoftmaxCrossEntropy<dimension,voc>;
		bow = new BowWrapper<dimension>(new NetworkExecuter(output));
		bow->init();
		em->init();
		memory->init();
		rnn->init();
		lin->init();
		softmax->init();
		output->init();
		for(int i=0;i<50;i++)
		{
			VectorXd k;
			for(int j=0;j<50;j++)
			{
				k = VectorXd::Random(dimension);
				k /= k.norm();
				double sim = -1;
				for(int ii = 0;ii<i;ii++)
				{
					double s = (keys[ii].transpose() * k)(0,0);
					if(s>sim)
						sim=s;
				}
				if(sim<0.2)
					break;
			}
			keys.push_back(k);
			gkeys.push_back(VectorXd::Zero(dimension));
		}
	}

	struct Trajectory
	{
		EmbeddingLayer<voc,dimension>** ems;// = new EmbeddingLayer<voc,dimension>*[T];
		Duplicate<dimension>** dups;// = new Duplicate<dimension>*[T];
		LinearLayer2<dimension,dimension,2>** lins;//= new LinearLayer2<dimension,dimension,2>*[T];
		SoftmaxLayer<2>** sms;//= new SoftmaxLayer<2>*[T];
		RNN<dimension,dimension>** rnns;// = new RNN<dimension,dimension>*[T];
		RNN<dimension,dimension>* zeroTime;// = (RNN<dimension,dimension>*) rnn->duplicate();
		std::vector<std::tuple<int,int>> intervals;
		bool* decisions;//[T];
		Memory<dimension,256> memory;
		vector<BowWrapper<dimension>*> outputs;
		std::vector<DummyJacobian<dimension>*> jacs;
		std::vector<DummyJacobian<2>*> jacs2;
		MatrixXd* hs;
		double L;
		int T;
	};

	Trajectory* sampleTrajectory(SeqExample* seq)
	{
		// std::random_device sampler;
    	
		int T = seq->l;
		int * sequence = seq->input;
		// std::srand(example);
		// for(int i=0;i<T;i++)
		// 	sequence[i] = 1+std::rand() % 99;
		// sequence[T-1] = 0;

		
		// for(int iter=0;iter<100;iter++)
		// {
		
	    	
    	Trajectory* s=new Trajectory;
		s->ems = new EmbeddingLayer<voc,dimension>*[T];
		s->dups = new Duplicate<dimension>*[T];
		s->lins= new LinearLayer2<dimension,dimension,2>*[T];
		s->sms= new SoftmaxLayer<2>*[T];
		s->rnns = new RNN<dimension,dimension>*[T];
		s->zeroTime = (RNN<dimension,dimension>*) rnn->duplicate();
		s->decisions = new bool[T];
		s->memory.basis = memory->basis;
		s->memory.inver = memory->inver;
		s->memory.setZero();

		int startTime = 0;
		for(int t=0;t<T;t++)
		{
			//construct next layer for the computational graph
			s->ems[t] = (EmbeddingLayer<voc,dimension>*) em->duplicate();
			s->dups[t] = (Duplicate<dimension>*) dup->duplicate();
			s->lins[t] = (LinearLayer2<dimension,dimension,2>*) lin->duplicate();
			s->sms[t] = (SoftmaxLayer<2>*) softmax->duplicate();
			s->rnns[t] = (RNN<dimension,dimension>*) rnn->duplicate();
			s->ems[t]->input = sequence[t]; // blubber
			s->ems[t]->forward();
			connectDepth(s->ems[t],s->dups[t]);
			s->dups[t]->forward();
			connectDepth11(s->dups[t],s->rnns[t]);
			connectDepth11(s->dups[t],s->lins[t]);
			if(t-1<startTime)
				connectDepth12(s->zeroTime,s->lins[t]);
			else
				connectDepth12(s->rnns[t-1],s->lins[t]);
			connectDepth(s->lins[t],s->sms[t]);
			s->lins[t]->forward();
			s->sms[t]->forward();
			if(t!=startTime && (t+1 == T || uniform(samplergen)<s->sms[t]->outputs[0](0,0))) //
			{
				s->decisions[t] = 0;
				// std::cout << "new chunk starting at " << t << std::endl;
				//save everything
				int k = s->intervals.size();
				s->intervals.push_back(std::make_tuple(startTime,t));
				VectorXd h;
				if(t-1<startTime)
				{
					std::cout << "SHIT" ;
					h = s->zeroTime->outputs[0];
				}
				else
				{
					h = s->rnns[t-1]->outputs[0];
					// std::cout << "Saving hidden state after timestep "<< t-1 << std::endl;
				}
				// std::cout << "norm_in: " << h.norm() << std::endl;
				s->memory.write(keys[k],h);
				// std::cout << h.transpose() << std::endl;
				startTime = t;
				connectTime(s->zeroTime,s->rnns[t]);
			}
			else
			{
				s->decisions[t] = 1;
				if(t-1<startTime)
					connectTime(s->zeroTime,s->rnns[t]);
				else
					connectTime(s->rnns[t-1],s->rnns[t]);
			}
			s->dups[t]->successorDeep2 = (ComputeUnit*)s->rnns[t];
			s->rnns[t]->forward();
		}

		
		// for(int i=0;i<T;i++)
		// 	std::cout << sequence[i] << " ";
		// std::cout << std::endl;
		double loss = 0;
		s->hs = new MatrixXd[s->intervals.size()];
		for(int i=0;i<s->intervals.size();i++)
		{
			auto interval = s->intervals[i];
			s->hs[i] = s->memory.read(keys[i]); 
			//MatrixXd h = s->rnns[std::get<1>(interval)-1]->outputs[0]; // TODO FIX THIS!!!!
			//h/=h.norm();
			BowWrapper<dimension>* bo = (BowWrapper<dimension>*)bow->duplicate();
			s->outputs.push_back(bo);
			bo->ll = std::get<1>(interval)-std::get<0>(interval);
			bo->y = new int[bo->ll];
			bo->inputs[0] = &(s->hs[i]); 
			// std::cout << (h.transpose() * (s->rnns[std::get<1>(interval)-1]->outputs[0])).array() / (h.norm() * s->rnns[std::get<1>(interval)-1]->outputs[0].norm()) << " ";
			for(int t = std::get<0>(interval);t<std::get<1>(interval);t++)
			{
				if(t==T-1)
				{
					bo->ll--;
					continue;
				}
				bo->y[t-std::get<0>(interval)] = sequence[t];
			}
			bo->forward();
			// std::cout << bo->loss() << " ";
			loss+=bo->loss()*bo->ll;

		}
		// std::cout << std::endl;
		// for(int i=0;i<intervals.size();i++)
		// 	std::cout << outputs[i]->loss() <<" ";
		// std::cout << std::endl;
		
		loss/=(T-1);
		// loss*=log(s->intervals.size()+1);
		//loss+=s->intervals.size();
		s->L = loss;
		s->T = T;

		// delete[] sequence;
		return s;
	}


	void backpropTrajectory(Trajectory* s)
	{
		int T = s->T;
		// for(int i=0;i<T;i++)
		// 	std::cout << s->decisions[i];
		// std::cout  << " " << s->L << std::endl;
		//OMG jetzt alles wieder rückwärts 
		for(int i=s->intervals.size()-1;i>=0;i--)
		{
			MatrixXd jac = s->outputs[i]->jacobians[0];
			VectorXd Lk = s->memory.deltaRead(keys[i],jac);
			gkeys[i]+=Lk;
		}
		//backprop for softmax and lin
		
		
		for(int t = 0;t<T;t++)
		{
			DummyJacobian<2>* jac = new DummyJacobian<2>;
			// jac->jacobians[1] = outputs[i]->jacobians[0]; //bypass holographic memory
			jac->jacobians[0] = MatrixXd::Zero(2,1);
			if((s->sms[t]->outputs[0])(s->decisions[t],0)<1e-5)
				s->sms[t]->outputs[0](s->decisions[t],0)=1e-5;
			jac->jacobians[0](s->decisions[t],0) = (s->L) * 1.0/(s->sms[t]->outputs[0](s->decisions[t],0));// 0.0001 *
			// std::cout << sms[t]->outputs[0].transpose() << " --- " << jac->jacobians[0].transpose() << std::endl;
			connectDepth(s->sms[t],jac);
			s->jacs2.push_back(jac);
			s->sms[t]->backward();
			s->lins[t]->backward();
		}

		//backprop preparations for the rnns
		
		for(int i=s->intervals.size()-1;i>=0;i--)
		{
			// std::cout << "Recovering hidden state after timestep "<< std::get<1>(intervals[i])-1 << std::endl;
			auto interval = s->intervals[i];
			VectorXd h = s->rnns[std::get<1>(interval)-1]->outputs[0];
			
			VectorXd Lk = s->memory.deltaWrite1(keys[i],h);
			gkeys[i]+=Lk;
			VectorXd Lv = s->memory.deltaWrite2(keys[i],h);
			DummyJacobian<dimension>* jac = new DummyJacobian<dimension>;
			// jac->jacobians[1] = outputs[i]->jacobians[0]; //bypass holographic memory
			jac->jacobians[1] = Lv; //holographic memory
			connectTime(s->rnns[std::get<1>(interval)-1],jac);
			s->jacs.push_back(jac);
		}
		//backprop for the rnns
		for(int i=T-1;i>=0;i--)
			s->rnns[i]->backward();
		//backprop for the duplicates
		for(int i=T-1;i>=0;i--)
			s->dups[i]->backward();
		//backprop for the embeddings
		for(int i=T-1;i>=0;i--)
			s->ems[i]->backward();

		//DELETE ALL THE SHIT YOU CREATED!!!

		for(auto jac : s->jacs)
			delete jac;
		for(auto jac : s->jacs2)
			delete jac;
		for(auto out : s->outputs)
		{
			delete[] out->y;
			delete out->innerNetwork;
			delete out;
		}
		for(int i=0;i<T;i++)
		{
			delete s->ems[i];
			delete s->dups[i];
			delete s->lins[i];
			delete s->sms[i];
			delete s->rnns[i];
		}
		delete s->zeroTime;
		delete[] s->hs;
		delete[] s->ems;
		delete[] s->dups;
		delete[] s->lins;
		delete[] s->sms;
		delete[] s->rnns;
		delete s;

		

	
	}

	void gradientStep(int n)
	{
		bow->gradientStep(0.01/n);
		em->gradientStep(0.01/n);
		rnn->gradientStep(0.01/n);
		lin->gradientStep(0.01/n);
		softmax->gradientStep(0.01/n);
		

		
		// for(int i=intervals.size()-1;i>=0;i--)
		// {
		// 	keys[i]-=0.001*gkeys[i];
		// 	// keys[i]/=keys[i].norm();
		// 	gkeys[i].setZero();
		// }
	
	 }
};