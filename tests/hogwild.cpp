
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include "linearlayer.h"
#include "relulayer.h"
#include "embedAndReduceLayer.h"
#include "crossEntropyLoss.h"
#include "softmaxLayer.h"
#include "networkExecuter.h"
#include "ogreManager.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"
#include <stdlib.h>
#include <vector>
#include <string>
#include "loadBinarySparse.h"
using Eigen::MatrixXd;
#define EIGEN_DONT_PARALLELIZE



int main()
{	
	Eigen::initParallel();
	std::cout << std::thread::hardware_concurrency() << std::endl;
	EmbedAndReduceLayer<146532,64,true> em;
	ReLuLayer<64,32> lin;
	LinearLayer<32,2> lin2;
	SoftmaxLayer<2> softmax;
	CrossEntropyLoss<2> loss;
	std::cout << "Netzwerk erstellt" << std::endl;
	
	connecty((ComputeUnit&)em, 		(ComputeUnit&)lin);
	connecty((ComputeUnit&)lin,		(ComputeUnit&)lin2);
	connecty((ComputeUnit&)lin2,	(ComputeUnit&)softmax);
	connecty((ComputeUnit&)softmax,	(ComputeUnit&)loss);

	NetworkExecuter* e = new NetworkExecuter(&em);
	e->init();


	const std::string file = "/data/d5/pfahler/USER_BIN";

	std::stringstream ws(file);


	int iter  = 0;
	double bytes = 0;


	double start = getTime();
	std::vector<int> tmp;
	std::vector<int*> dataset;

	std::ifstream docFile(file);
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		//std::cout << line << std::endl;
		//std::cout << iter << std::endl;
		if(line.size())
			dataset.push_back(count(line, tmp));

		if(iter%10000 == 0) std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ")" << std::flush;

		iter++;
	}
	docFile.close();
	std::cout << "DONE" << std::endl;
//	std::cout << "SUM = " << sum << std::endl;

//	std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ")" << std::endl;

	MemoryDataSource<int*> d(dataset,1);
	d.setNumberOfEpoches(100);

	HogwildManager<8>* h = new HogwildManager<8>(e,&d);
	h->go(0.01);
	// std::cout << "Netzwerk verbunden" << std::endl;
	
	

	// // softmax.init();
	// // loss.init();
	// std::cout << "Netzwerk initialisiert" << std::endl;

	// double averageLoss = 0;




	// EmbedAndReduceLayer<3000,64,true>* inputUnit = (EmbedAndReduceLayer<3000,64,true>*)e->layers[0];
	// CrossEntropyLoss<2>* outputUnit = (CrossEntropyLoss<2>*)e->layers[e->numLayers-1];
	// inputUnit->input = new int[1];

	// int iters=500000;
	// double stepsize= 0.01;
	// for(int i=0;i<iters;i++)
	// {
	// 	delete inputUnit->input;
	// 	inputUnit->l = 2*(rand()%10) + 1;
	// 	inputUnit->input=new int[inputUnit->l];//
	// 	int sum = 0;
	// 	for(int j=0;j<inputUnit->l;j++)
	// 	{
	// 		inputUnit->input[j]=rand()%3000;
	// 		sum += inputUnit->input[j] % 2 ? 1 : -1;
	// 	}
	// 	outputUnit->y = sum<0 ? 0 : 1;
		
	// 	e->forward();
	// 	e->backward();
	// 	e->gradientStep(stepsize);

	// 	averageLoss+=outputUnit->loss();
	// 	if(i%1000==999 && i+1<iters)
	// 	{
	// 		std::cout << "loss "<< averageLoss/1000.0 << std::endl;
	// 		averageLoss = 0;
	// 		stepsize*=0.99;
	// 	}
	// }
	// if(averageLoss/1000.0<0.01)
	// {
	// 	std::cout << "success"<<std::endl;
	// 	return 0;
	// }
	// std::cout << "failure"<<std::endl;
	// return 1;
	
}