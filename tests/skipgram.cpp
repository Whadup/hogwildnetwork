// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <stdlib.h>
#include <vector>
#include <string>
#include <algorithm>

#include "embeddingLayer.h"
#include "hierarchicalSoftmaxCrossEntropy.h"
#include "bowWrapper.h"

#include "loadBinarySparse.h"

#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"
// #include <omp.h> 

//#define FLAT
//#define LSTM
using Eigen::MatrixXd;
#define EIGEN_DONT_PARALLELIZE

//2^(890415/79078*)


int main()
{	
	const int dimension = 300;
	

	// EmbeddingLayer<58234,dimension> em;
	EmbeddingLayer<23718,dimension> em;
	HierarchicalSoftmaxCrossEntropy<dimension,142083> softmax("/data/pfahler/relnet/processed_data/jesus/posts.json_paragraphEmbedding.tree");

	// connectDepth((ComputeUnit&)em, 		(ComputeUnit&)bow);
	connectDepth((ComputeUnit&)em, 		(ComputeUnit&)softmax);


	std::cout << "Netzwerk erstellt" << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&em);
	e->init();

	int iter  = 0;
	double bytes = 0;
	double start = getTime();
	std::vector<int> tmp;
	int vocSize;
	std::vector<IntIntExample*> dataset;
	int ii=0;
	std::ifstream docFile("/data/pfahler/relnet/processed_data/jesus/posts.json_paragraphEmbedding.int");
	// std::ifstream docFile("/data/pfahler/relnet/processed_data/jesus/subforum_185/subforum_185.json_paragraphEmbedding.int");
	// std::ifstream docFile("/data/pfahler/corpora/DeepmindQA/dailymail.int");
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		if(line.size())
		{		
			// std::cout << line << std::endl;
		
			int l;
			int* a = parseSparse<false>(line,tmp,vocSize,l);
			for(int i=0;i<l;i++)
			{
				IntIntExample* e = new IntIntExample();
				e->input = ii;
				e->y = a[i];
				dataset.push_back(e);
			}
			delete[] a;
			//for(int i=0;i<e->ll;i++)
				// if(e->y[i]>4094)
				// 	e->y[i] = rand() % 4093;
			// 	std::cout << e->y[i] << " ";
			ii++;
			
			
			// for(int i=0;i<100;i++)
			
			// if(ii>10000)
			//   	break;
		}	
	}
	docFile.close();
	//
	std::random_shuffle ( dataset.begin(), dataset.end() );
	MemoryDataSource<IntIntExample> d(dataset,1);
	HogwildManager<IntIntExample,4>* h = new HogwildManager<IntIntExample,4>(e,&d);
	double stepsize = 0.025;
	for(int i=0;i<10;i++)
	{
		d.setNumberOfEpoches(1);
		h->go(stepsize);
		stepsize-=0.002;
		//std::random_shuffle ( dataset.begin(), dataset.end() );

	}
	delete h;
	// std::cout << *(rnn.Usts) << std::endl;

	std::ofstream myfile ("embeddingsLargeVoc.csv");

	std::cout << std::endl;
	for(int i=0;i<ii;i++)
		myfile << i << " " << em.W->col(i).transpose() << std::endl;
	myfile.close();
	
	
}