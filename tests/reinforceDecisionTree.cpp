#include "reinforceDecisionTree.h"
#include <stdlib.h>
#include "loadBinarySparse.h"
#include "loadData.h"
// #include <omp.h>
#include <vector>

// #define LETTER

#ifdef MNIST
	// #define HEIGHT 8//8
	#define NUMLABELS 10
	#define DIMENSION 784
	// #define N 32 //32
	// #define NUMTREES 1 // 5
	// #define ZEROONE false
#endif
#ifdef LETTER
	// #define HEIGHT 5
	#define NUMLABELS 26
	#define DIMENSION 256
	// #define N 64
	// #define NUMTREES 1
	// #define ZEROONE true
#endif
#ifdef USPS
	// #define HEIGHT 8
	#define NUMLABELS 10
	#define DIMENSION 256
	// #define N 32
	// #define NUMTREES 1
	// #define ZEROONE false
#endif
#ifdef PARTICLE
	// #define HEIGHT 12
	#define NUMLABELS 2
	#define DIMENSION 50
	// #define N 16
	// #define ZEROONE false
#endif



void computeAverages(ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>::Trajectory** trajectories, int height,	double& acc, double& L,double& minL,double& avgAgreement, double& minAgreement, int& decision,double& Lstd)
{
	double* a = new double[height];
	minAgreement = 1.0;
	avgAgreement = 0;
	L = 0;
	Lstd = 0;
	acc = 0;
	minL = 1000000000;
	int decisions[NUMLABELS];
	for(int i=0;i<NUMLABELS;i++)
		decisions[i]=0;
	for(int k=0;k<NSAMPLES;k++)
	{
		decisions[trajectories[k]->sample]++;
		// for(int kk = 0; kk<height;kk++)
		// {
		// 	std::cout << trajectories[k]->decisions[kk];
		// }
		// std::cout << trajectories[k]->sample << " " << trajectories[k]->L << std::endl;
		L+=trajectories[k]->L;
		Lstd+=1.0/NSAMPLES * trajectories[k]->L*trajectories[k]->L;

		acc+=trajectories[k]->accuracy;
		if(trajectories[k]->L < minL)
			minL = trajectories[k]->L;
	}
	decision=0;
	for(int i=1;i<NUMLABELS;i++)
		if(decisions[i]>decisions[decision])
			decision = i;
	// std::cout << "--------------------" << std::endl;
	for(int kk = 0; kk<height;kk++)
	{
		a[kk] = 0;
		for(int k=0;k<NSAMPLES;k++)
			a[kk]+=trajectories[k]->decisions[kk];
		a[kk]/=float(NSAMPLES);
		double agreement = a[kk]>0.5 ? a[kk] : 1.0-a[kk];
		if(agreement < minAgreement)
			minAgreement = agreement;
		avgAgreement += 1.0/height * agreement;
		
	}
	// for(int kk = 0; kk<height;kk++)
	// {
	// 	if(a[kk]>0.5)
	// 		std::cout << "1";
	// 	else
	// 		std::cout << "0";
	// }
	// std::cout << ".";
	delete[] a;
	
	Lstd = sqrt( (Lstd - 1/float(NSAMPLES*NSAMPLES)*L*L));
	L/=float(NSAMPLES);
	acc/=float(NSAMPLES);
}

int main()
{
	std::vector<MatrixXd> dataset;
	std::vector<int> labels;
	std::vector<MatrixXd> testset;
	std::vector<int> testLabels;
#ifdef MNIST
	loadMNistData(dataset,labels,testset,testLabels);
#endif
#ifdef LETTER
	loadLetterData(dataset,labels,testset,testLabels);
#endif
#ifdef USPS
	loadUSPSData(dataset,labels,testset,testLabels);
#endif
#ifdef PARTICLE
	loadParticleData(dataset,labels);
#endif
	ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>** forest = new ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>*[NUMTREES];
	for(int i=0;i<NUMTREES;i++)
	{
		forest[i] = new ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>(HEIGHT);
	}
	double avgLoss = 0;
	double avgAcc = 0;
	double avgAvgAgreement = 0;
	int c = 0;
	int decisions[NUMLABELS];
	for(int i=0;i<1000000;i++)
	{
		

		int j = rand() % dataset.size();
		{
			
			for(int k=0;k<NUMLABELS;k++)
				decisions[k] = 0;
			for(int tree = 0;tree<NUMTREES;tree++)
			{
				ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>* chunker = forest[tree];
				if(i%10000 == 0)
					chunker->startLogging();
				if(i%10000 == 1000)
				{
					std::ofstream log("log.csv",std::ofstream::out);
					chunker->endLoggingAndFlushLogs(log);
					log.close();
				}
				ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>::Trajectory** trajectories = new ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>::Trajectory*[NSAMPLES];
				
				#pragma omp parallel for
				for(int k=0;k<NSAMPLES;k++)
				{
					trajectories[k] = chunker->sampleTrajectory(&dataset[j],labels[j]);	
				}
				double acc = 0;
				double L = 0;
				double Lstd = 0;
				double avgAgreement = 0;
				double minAgreement = 0;
				double minL = 0;
				int decision = 0;
				computeAverages(trajectories,chunker->height,acc,L,minL,avgAgreement,minAgreement,decision,Lstd);
				decisions[decision]++;
				// std::cout << " L: "<<L<< "\t" << minL << "\t" << minAgreement << "\t" << avgAgreement <<  std::endl;
				// avgLoss += L;
				avgAcc += acc;
				avgAvgAgreement += avgAgreement;
				// std::cout << L << "±" << Lstd << " ";
				// c++;
				// if(c==999)
				// {
				// 	c++;
				// 	std::cout << "Loss " << (avgLoss / c) << "\tAccuracy " << (avgAcc / c) <<"\tAgreement " << (avgAvgAgreement / c) << std::endl;
				// 	avgLoss = 0;
				// 	avgAcc = 0;
				// 	avgAvgAgreement = 0;
				// 	c = 0;
				// }
				// int lea = 0;
				// for(int k=0;k<chunker->numNodes+1;k++)
				// 	if(chunker->used[chunker->numNodes + k])
				// 	{
				// 		std::cout << k << ",";
				// 		lea++;
				// 	}
				// std::cout << "(" << lea << ").";
				#pragma omp parallel for
				for(int k=0;k<NSAMPLES;k++)
				{
					trajectories[k]->L-=L;
					chunker->backpropTrajectory(trajectories[k],labels[j]);
				}

				#ifdef PARTICLE
					chunker->gradientStep(0.000001,NSAMPLES);	//particle
				#endif
				#ifdef MNIST
					if(c%20 == 19)chunker->gradientStep(0.01,NSAMPLES);	//mnist
				#endif
				#ifdef LETTER
					if(c%20 == 19)
						chunker->gradientStep(0.01,NSAMPLES);	//letter
				#endif
				#ifdef USPS
					if(c%20 == 19)chunker->gradientStep(0.01,NSAMPLES);	//usps
				#endif
			}
			int decision = 0;
			for(int i=1;i<NUMLABELS;i++)
				if(decisions[i]>decisions[decision])
					decision = i;
			if(decision != labels[j])
				avgLoss+=1;
			c++;
			if(c==999)
			{
				c++;
				std::cout << "Loss " << (avgLoss / c) << std::endl;
				avgLoss = 0;
				avgAvgAgreement = 0;
				c = 0;
			}
		}
	}
	double testLoss = 0;
	for(int j = 0;j<testset.size();j++)
	{
		for(int k=0;k<NUMLABELS;k++)
			decisions[k] = 0;
		for(int tree = 0;tree<NUMTREES;tree++)
		{
			ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>* chunker = forest[tree];
			ReinforceDecisionTree<DIMENSION,NUMLABELS,ZEROONE>::Trajectory* t = chunker->sampleTrajectory(&testset[j],testLabels[j]);
			decisions[t->sample]++;
			delete t;
		}
		int decision = 0;
		for(int i=1;i<NUMLABELS;i++)
			if(decisions[i]>decisions[decision])
				decision = i;
		testLoss += 1.0/testset.size() * (testLabels[j]!=decision);
	}
	std::cout << "TEST-LOSS " << testLoss << std::endl;

	return 0;
}