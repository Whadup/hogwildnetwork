#include "qlearnDecisionTree.h"
#include <stdlib.h>
#include "loadBinarySparse.h"
#include "loadData.h"
// #include <omp.h>
#include <vector>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <hyperboard.h>

bool stopTraining = 0;

void my_handler(int s){
	stopTraining = 1;
	std::cout << std::endl;
}


   
// #define LETTER 

// #ifndef NUMTREES
// 	#define NUMTREES 1
// #endif
// #ifndef NSAMPLES
// 	#define NSAMPLES 32
// #endif
// #ifndef ZEROONE
// 	#define ZEROONE 0
// #endif

#ifdef MNIST
//	#define HEIGHT 3//8
	#define NUMLABELS 10
	#define DIMENSION 784
	// #define N 16 //32
	#define CACHE 100000
	// #define NUMTREES 1 // 5
#endif
#ifdef LETTER
	// #define HEIGHT 8
	#define NUMLABELS 26
	#define DIMENSION 256
	// #define N 16
	#define CACHE 100000
	// #define NUMTREES 1
#endif
#ifdef USPS
	// #define HEIGHT 8
	#define NUMLABELS 10
	#define DIMENSION 256
	#define CACHE 100000
	// #define N 32
	// #define NUMTREES 1
	// #define ZEROONE false
#endif
#ifdef PARTICLE
	// #define HEIGHT 12
	#define NUMLABELS 2
	#define DIMENSION 50
	// #define N 16
	// #define ZEROONE false
#endif


int main()
{
	std::string loggerName = registerLogger("QLearner","Accuracy");
	std::vector<MatrixXd> dataset;
	std::vector<int> labels;
	std::vector<MatrixXd> testset;
	std::vector<int> testLabels;
	signal (SIGINT,my_handler);
#ifdef MNIST
	loadMNistData(dataset,labels,testset,testLabels);
#endif
#ifdef LETTER
	loadLetterData(dataset,labels,testset,testLabels);
#endif
#ifdef USPS
	loadUSPSData(dataset,labels,testset,testLabels);
#endif
#ifdef PARTICLE
	loadParticleData(dataset,labels);
#endif
	QLearnDecisionTree<DIMENSION,NUMLABELS,CACHE,NSAMPLES,ZEROONE>** forest = new QLearnDecisionTree<DIMENSION,NUMLABELS,CACHE,NSAMPLES,ZEROONE>*[NUMTREES];
	for(int i=0;i<NUMTREES;i++)
	{
		forest[i] = new QLearnDecisionTree<DIMENSION,NUMLABELS,CACHE,NSAMPLES,ZEROONE>(HEIGHT);
	}
	double avgLoss = 0;
	double avgAcc = 0;
	double avgAvgAgreement = 0;
	int c = 0;
	int decisions[NUMLABELS];
	double eps = 0.6;
	for(int i=0;i<5000000;i++)
	{
		if(stopTraining)
			break;
		int j = rand() % dataset.size();
		{
			
			for(int k=0;k<NUMLABELS;k++)
				decisions[k] = 0;
			#pragma omp parallel for
			for(int tree = 0;tree<NUMTREES;tree++)
			{
				
				QLearnDecisionTree<DIMENSION,NUMLABELS,CACHE,NSAMPLES,ZEROONE>* q = forest[tree];
				if(i%10000 == 0)
					q->startLogging();
				if(i%10000 == 9999)
				{
					std::ofstream log("log.csv",std::ofstream::out);
					q->endLoggingAndFlushLogs(log);
					log.close();
				}
				decisions[q->createRecord(&dataset[j],labels[j],eps)]++;
				q->learnRecord(0.01);
			}
			int decision = 0;
			for(int i=1;i<NUMLABELS;i++)
				if(decisions[i]>decisions[decision])
					decision = i;
			if(decision != labels[j])
				avgLoss+=1;
			c++;
			if(c==999)
			{
				
				c++;
				logValue(loggerName,i,(avgLoss / c));
				std::cout << "Loss " << (avgLoss / c) << " eps " << eps << std::endl;
				if(eps>0.01)
				{	if(eps<0.1)
						eps-=0.001;
					else
						eps-=0.01;
				}
				avgLoss = 0;
				avgAvgAgreement = 0;
				c = 0;
				for(int tree = 0;tree<NUMTREES;tree++)
				{
					QLearnDecisionTree<DIMENSION,NUMLABELS,CACHE,NSAMPLES,ZEROONE>* q = forest[tree];
					q->flip();
				}
			}	
		}

	}
	double testLoss = 0;
	for(int j = 0;j<testset.size();j++)
	{
		for(int k=0;k<NUMLABELS;k++)
			decisions[k] = 0;
		for(int tree = 0;tree<NUMTREES;tree++)
		{
			QLearnDecisionTree<DIMENSION,NUMLABELS,CACHE,NSAMPLES,ZEROONE>* q = forest[tree];
			decisions[q->createRecord(&testset[j],testLabels[j],0)]++;
			
		}
		int decision = 0;
		for(int i=1;i<NUMLABELS;i++)
			if(decisions[i]>decisions[decision])
				decision = i;
		testLoss += 1.0/testset.size() * (testLabels[j]!=decision);
	}
	std::cout << "TEST-LOSS " << testLoss << std::endl;

	return 0;
}