//clear && g++ -std=c++11 -O0 -I libs/eigen/ -I . tests/linearRegression.cpp && ./a.out 
#include <iostream>
#include <Eigen/Dense>
#include "linearlayer.h"
#include "l2loss.h"

using Eigen::MatrixXd;




int main()
{
	
	LinearLayer<30,10> lin;
	LinearLayer<10,1> lin2;
	L2Loss<1> loss;

	connecty((ComputeUnit&)lin, (ComputeUnit&)lin2);
	connecty((ComputeUnit&)lin2,(ComputeUnit&)loss);

	lin.init();
	lin2.init();
	
	std::cout << &loss << std::endl;
	
	MatrixXd w=MatrixXd::Random(1,30);
	for(int i=0;i<10000;i++)
	{
		MatrixXd x=MatrixXd::Random(30,1);
		x(5,0)=1;
		Matrix<double,1,1> y = w*x;
		lin.inputs[0]=&x;
		loss.y = &y;
		lin.forward();
		lin2.forward();
		loss.forward();
		// std::cout << lin2.outputs[0]<< "~" << y << std::endl;
		loss.backward();
		lin2.backward();
		lin.backward();
		lin.gradientStep(0.01);
		lin2.gradientStep(0.01);
		std::cout << "loss "<<loss.loss() << std::endl;
	}
	std::cout << w << std::endl;
	std::cout << ((*lin2.W)*(*lin.W)) << std::endl;
	auto bla = ((*lin2.W)*(*lin.W))-w;
	if(bla.dot(bla)<0.001)
	{
		std::cout << "success"<<std::endl;
		return 0;
	}
	std::cout << "failure"<<std::endl;
	return 1;
	
}