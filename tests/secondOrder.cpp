// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <stdlib.h>
#include <vector>
#include <string>

#include "embeddingLayer.h"
#include "secondOrderRnn.h"
#include "hierarchicalSoftmaxCrossEntropy.h"
#include "linearlayer.h"
#include "softmaxLayer.h"
#include "recurrentWrapper.h"
#include "crossEntropyLoss.h"


#include "loadBinarySparse.h"

#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"


//#define FLAT
//#define LSTM
using Eigen::MatrixXd;
#define EIGEN_DONT_PARALLELIZE

//2^(890415/79078*)


int main()
{	
	const int dimension = 10;
	EmbeddingLayer<9644,dimension> em;

	SecondOrderRnn<dimension,dimension> rnn1;
	SecondOrderRnn<dimension,dimension> rnn2;
	SecondOrderRnn<dimension,dimension> rnn3;
	SecondOrderRnn<dimension,dimension> rnn4;
	HierarchicalSoftmaxCrossEntropy<dimension,9644> softmax("/data/pfahler/corpora/penn/processed/penn.huffmantree");

	connectDepth((ComputeUnit&)em, 		(ComputeUnit&)rnn1);
	connectDepth((ComputeUnit&)rnn1, 		(ComputeUnit&)rnn2);
	connectDepth((ComputeUnit&)rnn2, 		(ComputeUnit&)rnn3);
	connectDepth((ComputeUnit&)rnn3, 		(ComputeUnit&)rnn4);
	connectDepth((ComputeUnit&)rnn4,	(ComputeUnit&)softmax);

	RecurrentWrapper<0> wrapper(new NetworkExecuter(&em,&softmax));

	std::cout << "Netzwerk erstellt" << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&wrapper);
	e->init();

	{
		int iter  = 0;
		double bytes = 0;
		double start = getTime();
		std::vector<int> tmp;
		std::vector<SeqSeqExample*> dataset;
		std::ifstream docFile("/data/pfahler/corpora/penn/processed/penn.int");
		while(!docFile.eof()){
			std::string line;
			getline(docFile, line);
			bytes += line.size();
			if(line.size())
			{	
				SeqSeqExample* e = new SeqSeqExample;
				e->y = parseIntegerTokens(line, tmp,e->l);
				e->ll = e->l;
				e->input = e->y;
				dataset.push_back(e);
			}	
			if(iter%30000 == 0) 
			{
					// std::cout << vocSize << std::endl;
					std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ") " << std::flush;
			}
			iter++;
		}
		docFile.close();
		std::cout << "DONE" <<  std::endl;


		MemoryDataSource<SeqSeqExample> d(dataset,1);
		d.setNumberOfEpoches(5);

		connectDepth((ComputeUnit&)em,	(ComputeUnit&)softmax);
		RecurrentWrapper<0> wrapper2(new NetworkExecuter(&em,&softmax));
		std::cout << "Netzwerk erstellt" << std::endl;
		NetworkExecuter* e2 = new NetworkExecuter(&wrapper2);
		HogwildManager<SeqSeqExample,1>* h = new HogwildManager<SeqSeqExample,1>(e2,&d);
		h->go(0.001);
		connectDepth((ComputeUnit&)em,	(ComputeUnit&)rnn1);
	}
	std::cout << "pretraining done" << std::endl;
	//em.loadEmbeddings("/data/pfahler/wordEmbeddings/googleNews/GoogleNews-vectors-negative300.bin","/data/pfahler/corpora/penn/processed/penn.voc");

	int iter  = 0;
	double bytes = 0;
	double start = getTime();
	std::vector<int> tmp;
	
	std::vector<SeqSeqExample*> dataset;
	std::ifstream docFile("/data/pfahler/corpora/penn/processed/penn.train.int");
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		if(line.size())
		{	
			SeqSeqExample* e = new SeqSeqExample;
			e->y = parseIntegerTokens(line, tmp,e->l);
			e->ll = e->l;
			e->input = new int[e->l];
			e->input[0] = 0;
			for(int i=0;i<e->ll-1;i++)
				e->input[i+1] = e->y[i];
			dataset.push_back(e);
		}	
		if(iter%30000 == 0) 
		{
				// std::cout << vocSize << std::endl;
				std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ") " << std::flush;
		}
		iter++;
	}
	docFile.close();

	std::vector<SeqSeqExample*> valset;
	std::ifstream valFile("/data/pfahler/corpora/penn/processed/penn.test.int");
	while(!valFile.eof()){
		std::string line;
		getline(valFile, line);
		bytes += line.size();
		if(line.size())
		{	

			
			SeqSeqExample* e = new SeqSeqExample;
			e->y = parseIntegerTokens(line, tmp,e->l);
			e->ll = e->l;
			e->input = new int[e->l];
			e->input[0] = 0;
			for(int i=0;i<e->ll-1;i++)
				e->input[i+1] = e->y[i];
			valset.push_back(e);
		}	
	}
	valFile.close();

	MemoryDataSource<SeqSeqExample> d(dataset,1);
	d.setNumberOfEpoches(1);
	MemoryDataSource<SeqSeqExample> d2(valset,1);
	d2.setNumberOfEpoches(1);
	


	double stepsize = 0.01;
	double lastLoss = 30000;
	for(int i=0;i<50;i++)
	{
		std::cout << "stepsize: " << stepsize << std::endl;
		HogwildManager<SeqSeqExample,1>* h = new HogwildManager<SeqSeqExample,1>(e,&d);
		h->go(stepsize);
		delete h;
		h = new HogwildManager<SeqSeqExample,1>(e,&d2);
		h->go(0);
		if(h->lastLoss/lastLoss >0.99)
			stepsize*=0.9;
		lastLoss = h->lastLoss;
		delete h;
		d.setNumberOfEpoches(1);
		d2.setNumberOfEpoches(1);
	}
	
	
}