#include "holographic/holographic.h"
#include <iostream> 
#include <vector>
#include <cmath>
#include <Eigen/Dense>
#include <algorithm>
#include <map>
#include <string> 
using Eigen::Matrix;
using Eigen::MatrixXd;
using Eigen::VectorXd;

void twoDimensionalTest()
{
	Memory<2,32> memory;
	memory.init();

	VectorXd key1(2); key1 << 1/sqrt(2), 1/sqrt(2);
	VectorXd key2(2); key2 << -0.6 , 0.8;

	VectorXd value1=VectorXd::Random(2);
	value1/=value1.norm();
	VectorXd value2=VectorXd::Random(2);
	value2/=value2.norm();

	memory.write(key1,value1);
	memory.write(key2,value2);

	// VectorXd a = VectorXd::Random(2);
	// VectorXd b = VectorXd::Random(2);
	// a/=a.norm();
	// b/=b.norm();

	// memory.write(a,b);

	for(float y = -1;y<=1;y+=0.01)
	{
		for(float x = -1;x<=1;x+=0.01)
		{
			VectorXd key(2);
			key << x , y;
			VectorXd ret = memory.read(key);
			// ret/=ret.norm();
			float dist1 = value1.dot(ret)/(ret.norm());
			float dist2 = value2.dot(ret)/(ret.norm());
			std::cout << x<< ";" << y << ";" << (dist1>dist2 ? dist1 : dist2)  << std::endl;;
		}
	}
}

void randomRecall()
{
	int const dim = 128;
	int const cap = 20;
	Memory<dim,2048> memory;
	memory.init();
	
	std::vector<VectorXd> ks;
	std::vector<VectorXd> vs;

	for (int i=0;i<cap;i++)
	{
		VectorXd k = VectorXd::Random(dim);
		VectorXd v = VectorXd::Random(dim);
		// k = MatrixXd::Random(dim,1);
		k /= k.norm();
		// v = MatrixXd::Random(dim,1);
		v/=v.norm();
		ks.push_back(k);
		vs.push_back(v);
		// for(int i=0;i<dim;i++)
		// {
		// 	k(i,0) = d(gen);
		// 	v(i,0) = d(gen);
		// }
//		std::cout << std::endl << v.transpose() << std::endl;
		memory.write(k,v);
	}

	for(int j=0;j<cap;j++)
	{
		MatrixXd k = ks[j];
		// std::cout << memory.trace.transpose() << std::endl;
		// MatrixXd e = memory.read(k).transpose();
		// std::cout << e/e.norm() << std::endl << e << std::endl;
		for(int i=0;i<cap;i++)
		{
			VectorXd v = vs[i];
			VectorXd vv = memory.read(k);
			vv/=vv.norm();
			// std::cout << v.dot(vv) << "/" << (v-vv).lpNorm<2>() << "\t";
			double cosine = v.dot(vv);
			if(cosine<0)cosine=0;
			if(((int)(10*cosine))==0) std::cout << " ";
			else std::cout << ((int)(10*cosine));
		}
		std::cout << std::endl;
	}
}

void randomRecallNoise()
{
	int const dim = 128;
	int const cap = 20;
	Memory<dim,2048> memory;
	memory.init();
	
	std::vector<VectorXd> ks;
	std::vector<VectorXd> vs;

	for (int i=0;i<cap;i++)
	{
		VectorXd k = VectorXd::Random(dim);
		VectorXd v = VectorXd::Random(dim);
		// k = MatrixXd::Random(dim,1);
		k /= k.norm();
		// v = MatrixXd::Random(dim,1);
		v/=v.norm();
		ks.push_back(k);
		vs.push_back(v);
		// for(int i=0;i<dim;i++)
		// {
		// 	k(i,0) = d(gen);
		// 	v(i,0) = d(gen);
		// }
//		std::cout << std::endl << v.transpose() << std::endl;
		memory.write(k,v);
	}

	for(int j=0;j<cap;j++)
	{
		MatrixXd k = ks[j];
		std::random_device rd;
		std::mt19937 gen(rd());
		std::normal_distribution<> normal(0,1);
		for(int j=0;j<dim;j++)
			k(j,0)+=0.1*normal(gen);
		// std::cout << memory.trace.transpose() << std::endl;
		// MatrixXd e = memory.read(k).transpose();
		// std::cout << e/e.norm() << std::endl << e << std::endl;
		for(int i=0;i<cap;i++)
		{
			VectorXd v = vs[i];
			VectorXd vv = memory.read(k);
			vv/=vv.norm();
			// std::cout << v.dot(vv) << "/" << (v-vv).lpNorm<2>() << "\t";
			double cosine = v.dot(vv);
			if(cosine<0)cosine=0;
			if(((int)(10*cosine))==0) std::cout << " ";
			else std::cout << ((int)(10*cosine));
		}
		std::cout << std::endl;
	}
}

void recallUnderNoise()
{
	int const dim = 128;
	int const cap = 20;
	Memory<dim,2048> memory;
	memory.init();
	
	std::vector<VectorXd> ks;
	std::vector<VectorXd> vs;

	for (int i=0;i<cap;i++)
	{
		VectorXd k = VectorXd::Random(dim);
		VectorXd v = VectorXd::Random(dim);
		// k = MatrixXd::Random(dim,1);
		k /= k.norm();
		// v = MatrixXd::Random(dim,1);
		v/=v.norm();
		ks.push_back(k);
		vs.push_back(v);
		// for(int i=0;i<dim;i++)
		// {
		// 	k(i,0) = d(gen);
		// 	v(i,0) = d(gen);
		// }
//		std::cout << std::endl << v.transpose() << std::endl;
		memory.write(k,v);
	}

	for(int j=0;j<cap;j++)
	{
		MatrixXd k = ks[j];
		VectorXd v = vs[j];
		for(int i=0;i<10;i++)
		{
			double stat = 0;
			for(int iters = 0;iters<20;iters++)
			{
				std::random_device rd;
				std::mt19937 gen(rd());
				std::normal_distribution<> normal(0,1);
				MatrixXd k = ks[j];
				for(int j=0;j<dim;j++)
					k(j,0)+=i/10.0*normal(gen);
				// k/=k.norm();
				VectorXd vv = memory.read(k);
				vv/=vv.norm();
				stat += v.dot(vv);
				
			}
			stat/=20;
			std::cout << stat <<"\t";//<< "/" << (v-vv).lpNorm<2>() << "\t";
			// double cosine = v.dot(vv);
			// if(cosine<0)cosine=0;
			// std::cout << ((int)(10*cosine));
		}
		std::cout << std::endl;
	}

}

void embeddingRecall()
{
	int const cap = 10;
	int const dim = 300;
	std::vector<VectorXd> ks;
	std::vector<VectorXd> vs;
	Memory<dim,4096> memory;
	memory.init();
	FILE* f;
	f = fopen("/data/pfahler/wordEmbeddings/googleNews/GoogleNews-vectors-negative300.bin", "rb");
		
	long long words;
	long long size;

	fscanf(f, "%lld", &words);
	fscanf(f, "%lld", &size);
	
	

	std::cout << "embeddings loading with "<<words << " " << size << std::endl;
	// vocab = (char *)malloc((long long)words * max_w * sizeof(char));
	// M = (float *)malloc((long long)words * (long long)size * sizeof(float));
	// if (M == NULL) {
	// 	printf("Cannot allocate memory: %lld MB    %lld  %lld\n", (long long)words * size * sizeof(float) / 1048576, words, size);
	// 	return -1;
	// }
	int hits = 0;

	int overlap = dim;

	for (int b = 0; b < words; b++)
	{
		char word[50];
		char ch;
		fscanf(f, "%s%c", &word[0], &ch);
		std::string sword(word);
		//std::transform(sword.begin(), sword.end(), sword.begin(), ::tolower);
		
		float embedding[size];
		
		for(int a = 0; a < size; a++) fread(&embedding[a], sizeof(float), 1, f);
		float len = 0;
		for (int a = 0; a < dim; a++) len += embedding[a] * embedding[a];
		len = sqrt(len);
		for (int a = 0; a < dim; a++) embedding[a] /= len;

		if (std::rand() % 1000 == 1)
		{
			std::cout << sword << std::endl;
			VectorXd W(dim);
			for(int i=0;i<dim;i++)
			{
				W(i,0) = embedding[i];
			}
			if(ks.size()<=vs.size())
				ks.push_back(W);
			else
			{
				vs.push_back(W);
				memory.write(ks[vs.size()-1],W);
			}
			if(vs.size()>=cap)
				break;
			//std::cout << sword << " has an embedding " << hits++ << std::endl;
			// std::cout << W->col(token).transpose() << std::endl;
		}
	}
	fclose(f);

	for(int j=0;j<cap;j++)
	{
		MatrixXd k = ks[j];
		std::random_device rd;
		std::mt19937 gen(rd());
		std::normal_distribution<> normal(0,1);
		for(int j=0;j<dim;j++)
			k(j,0)+=0.1*normal(gen);
		// std::cout << memory.trace.transpose() << std::endl;
		// MatrixXd e = memory.read(k).transpose();
		// std::cout << e/e.norm() << std::endl << e << std::endl;
		for(int i=0;i<cap;i++)
		{
			VectorXd v = vs[i];
			VectorXd vv = memory.read(k);
			vv/=vv.norm();
			// std::cout << v.dot(vv) << "/" << (v-vv).lpNorm<2>() << "\t";
			double cosine = v.dot(vv);
			if(cosine<0)cosine=0;
			if(((int)(10*cosine))==0) std::cout << " ";
			else std::cout << ((int)(10*cosine));
		}
		std::cout << std::endl;
	}

}

int main()
{
	for(int i=0;i<10;i++)
	{
		embeddingRecall();
		std::cout << std::endl;
	}
	randomRecall();
	std::cout << std::endl;
	randomRecallNoise();
	std::cout << std::endl;
	recallUnderNoise();
	
}

// int main()
// {
// 	int const dim = 64;
// 	int const cap = 5;
// 	Memory<dim,4096> memory;
// 	memory.init();
	
// 	std::vector<VectorXd> ks;
// 	std::vector<VectorXd> vs;

// 	for (int i=0;i<cap;i++)
// 	{
// 		VectorXd k = VectorXd::Random(dim);
// 		VectorXd v = VectorXd::Random(dim);
// 		// k = MatrixXd::Random(dim,1);
// 		k /= k.norm();
// 		// v = MatrixXd::Random(dim,1);
// 		v/=v.norm();
// 		ks.push_back(k);
// 		vs.push_back(v);
// 		// for(int i=0;i<dim;i++)
// 		// {
// 		// 	k(i,0) = d(gen);
// 		// 	v(i,0) = d(gen);
// 		// }
// //		std::cout << std::endl << v.transpose() << std::endl;
// 		memory.write(k,v);
// 	}

// 	for(int j=0;j<cap;j++)
// 	{
// 		MatrixXd k = ks[j];
// 		// std::cout << memory.trace.transpose() << std::endl;
// 		// MatrixXd e = memory.read(k).transpose();
// 		// std::cout << e/e.norm() << std::endl << e << std::endl;
// 		for(int i=0;i<cap;i++)
// 		{
// 			VectorXd v = vs[i];
// 			VectorXd vv = memory.read(k);
// 			vv/=vv.norm();
// 			std::cout << v.dot(vv) << "/" << (v-vv).lpNorm<2>() << "\t";
// 		}
// 		std::cout << std::endl;
// 	}
	
// 	return 0;
// }