
#include <iostream>
#include <Eigen/Dense>
#include "linearlayer.h"
#include "embeddingLayer.h"
#include "crossEntropyLoss.h"
#include "softmaxLayer.h"
#include <stdlib.h>
using Eigen::MatrixXd;




int main()
{	
	EmbeddingLayer<3000,64> em;
	LinearLayer<64,32> lin;
	LinearLayer<32,2> lin2;
	SoftmaxLayer<2> softmax;
	CrossEntropyLoss<2> loss;

	connecty((ComputeUnit&)em, 		(ComputeUnit&)lin);
	connecty((ComputeUnit&)lin,		(ComputeUnit&)lin2);
	connecty((ComputeUnit&)lin2,	(ComputeUnit&)softmax);
	connecty((ComputeUnit&)softmax,	(ComputeUnit&)loss);
	
	em.init();
	lin.init();
	lin2.init();
	
	double averageLoss = 0;
	int iters=1000000;
	for(int i=0;i<iters;i++)
	{
		em.input=rand()%3000;
		loss.y = em.input % 2 ? 0 : 1;
		em.forward();
		lin.forward();
		lin2.forward();
		softmax.forward();
		loss.forward();
		// std::cout << softmax.outputs[0].transpose()  << " " << loss.y << std::endl;
		// std::cout << lin2.outputs[0].transpose() << "~" << lin.outputs[0].transpose() << std::endl;
		loss.backward();
		softmax.backward();
		lin2.backward();
		lin.backward();
		em.backward();
		em.gradientStep(0.01);
		lin.gradientStep(0.01);
		lin2.gradientStep(0.01);
		averageLoss+=loss.loss();
		if(i%1000==999 && i+1<iters)
		{
			std::cout << "loss "<< averageLoss/1000.0 << std::endl;
			averageLoss = 0;
		}
	}
	if(averageLoss/1000.0<0.0001)
	{
		std::cout << "success"<<std::endl;
		return 0;
	}
	std::cout << "failure"<<std::endl;
	return 1;
	
}