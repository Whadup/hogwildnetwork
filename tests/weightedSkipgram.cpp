// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <stdlib.h>
#include <vector>
#include <string>
#include <algorithm>

#include "embeddingLayer.h"
#include "hierarchicalSoftmaxCrossEntropy.h"
#include "bowWrapper.h"

#include "loadBinarySparse.h"

#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"
// #include <omp.h> 

//#define FLAT
//#define LSTM
using Eigen::MatrixXd;
#define EIGEN_DONT_PARALLELIZE

//2^(890415/79078*)
void split(const std::string& s, int& token, double& freq)
{
	std::stringstream ss;
	ss.str(s);
	std::string item;
	std::getline(ss,item,':');
	token = atoi(item.c_str());
	std::getline(ss,item,':');
	freq = atof(item.c_str());
	
}

inline void readSparse(std::string const pPathExamples,
					   std::vector<WeightedIntIntExample*>& dataset,
					 unsigned int &DIM,
					 int ignoreFeatures,
					 bool header,
					 int& ii) {
	DIM = 0;
	std::string line;
	std::ifstream file(pPathExamples);
	if(header) std::getline(file,line); //ignore header
	std::vector<int>exampleI;
	std::vector<double>exampleD;
  
	if (file.is_open()) {
		while ( std::getline(file,line)) {
			if ( line.size() > 0) {
				std::stringstream ss(line);

				std::string entry;    
				exampleD.clear();
				exampleI.clear();
				int cnt = 0;
				while( std::getline(ss, entry,',') ) {
					if (entry.size() > 0) {
						if(cnt>=ignoreFeatures){
							int token;
							double freq;
							split(entry,token,freq);
							WeightedIntIntExample* e = new WeightedIntIntExample;
							e->input = ii;
							e->y = token;
							e->w = log(1+freq);
							if(e->w!=e->w)
								std::cout << "OMG" << ii << " " << token << " " << freq << std::endl;
							dataset.push_back(e);
							if(token>=DIM)
								DIM = token+1;
						}
						++cnt;
					}
					
				}
			}
			ii++;
		}
		file.close();
	}
}

int main()
{   
	const int dimension = 300;
	
	std::vector<WeightedIntIntExample*> dataset;
	unsigned int DIM;
	int ii=0;
	readSparse("/data/pfahler/corpora/amazonReviews/amazonReviewsLarge_TFiDF.csv",dataset,DIM,0,0,ii);
	std::cout << DIM << " " << ii << std::endl;
	// EmbeddingLayer<58234,dimension> em;
	EmbeddingLayer<201147,dimension> em;
	HierarchicalSoftmaxCrossEntropy<dimension,158909> softmax("/data/pfahler/corpora/amazonReviews/amazonReviewsLarge.huffman");//("/data/pfahler/relnet/processed_data/jesus/posts.json_paragraphEmbedding.tree");

	// connectDepth((ComputeUnit&)em,       (ComputeUnit&)bow);
	connectDepth((ComputeUnit&)em,      (ComputeUnit&)softmax);


	std::cout << "Netzwerk erstellt" << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&em);
	e->init();


	
	
	//
	// std::random_shuffle ( dataset.begin(), dataset.end() );
	MemoryDataSource<WeightedIntIntExample> d(dataset,1);
	HogwildManager<WeightedIntIntExample,8>* h = new HogwildManager<WeightedIntIntExample,8>(e,&d);
	double stepsize = 0.025;
	for(int i=0;i<10;i++)
	{
		d.setNumberOfEpoches(1);
		h->go(stepsize);
		stepsize-=0.002;
		if(stepsize<0.01)
			stepsize = 0.01;
		d.setNumberOfEpoches(1);
		h->go(stepsize);
		std::ofstream myfile ("embeddingsLargeVoc.csv");

		std::cout << std::endl;
		for(int i=0;i<ii;i++)
			myfile << i << " " << em.W->col(i).transpose() << std::endl;
		myfile.close();
		// std::random_shuffle ( dataset.begin(), dataset.end() );

	}
	delete h;
	// std::cout << *(rnn.Usts) << std::endl;

	
	
	
}
