// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <stdlib.h>
#include <vector>
#include <string>
#include <algorithm>

#include "embeddingLayer.h"
#include "hierarchicalSoftmaxCrossEntropy.h"
#include "bowWrapper.h"

#include "loadBinarySparse.h"

#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"
// #include <omp.h> 

//#define FLAT
//#define LSTM
using Eigen::MatrixXd;
#define EIGEN_DONT_PARALLELIZE

//2^(890415/79078*)



int main()
{	
	const int dimension = 64;
	HierarchicalSoftmaxCrossEntropy<dimension,5003> softmax;

	// EmbeddingLayer<58234,dimension> em;
	EmbeddingLayer<23715,dimension> em;
	BowWrapper<dimension,32> bow(new NetworkExecuter(&softmax));

	// connectDepth((ComputeUnit&)em, 		(ComputeUnit&)bow);
	connectDepth((ComputeUnit&)em, 		(ComputeUnit&)bow);


	std::cout << "Netzwerk erstellt" << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&em);
	e->init();

	int iter  = 0;
	double bytes = 0;
	double start = getTime();
	std::vector<int> tmp;
	int vocSize;
	std::vector<IntBowExample*> dataset;
	int ii=0;
	std::ifstream docFile("/data/pfahler/relnet/processed_data/jesus/posts.json_paragraphEmbedding.int");
	// std::ifstream docFile("/data/pfahler/relnet/processed_data/jesus/subforum_185/subforum_185.json_paragraphEmbedding.int");
	// std::ifstream docFile("/data/pfahler/corpora/DeepmindQA/dailymail.int");
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		if(line.size())
		{		
			// std::cout << line << std::endl;
			IntBowExample* e = new IntBowExample;
			e->y = parseSparse<false>(line,tmp,vocSize,e->ll);
			// for(int i=0;i<e->ll;i++)
			// 	std::cout << e->y[i] << " ";
			e->input = ii++;
			// std::cout << e->input << std::endl;
			
			// for(int i=0;i<100;i++)
			dataset.push_back(e);
			// if(ii>10000)
			//   	break;
		}	
	}
	docFile.close();
	//
	std::random_shuffle ( dataset.begin(), dataset.end() );
	MemoryDataSource<IntBowExample> d(dataset,1);
	HogwildManager<IntBowExample,1>* h = new HogwildManager<IntBowExample,1>(e,&d);
	
	for(int i=0;i<250;i++)
	{
		d.setNumberOfEpoches(1);
		double stepsize = 0.1;
		
		h->go(stepsize);
		
		std::random_shuffle ( dataset.begin(), dataset.end() );

	}
	delete h;
	// std::cout << *(rnn.Usts) << std::endl;

	std::ofstream myfile ("embeddings.csv");

	std::cout << std::endl;
	for(int i=0;i<ii;i++)
		myfile << i << " " << em.W->col(i).transpose() << std::endl;
	myfile.close();
	
	
}