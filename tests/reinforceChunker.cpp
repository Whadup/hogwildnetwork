#include "reinforceChunker.h"
#include <stdlib.h>
#include "loadBinarySparse.h"
#include <omp.h>


int main()
{
	static int n = 32;
	int iter  = 0;
	double bytes = 0;
	double start = getTime();
	std::vector<int> tmp;
	std::vector<SeqExample*> dataset;
	std::ifstream docFile("/data/pfahler/corpora/penn/processed/penn.train.int");
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		if(line.size())
		{	
			SeqExample* e = new SeqExample;
			e->input = parseIntegerTokens(line, tmp,e->l);
			if(e->l<20)
				dataset.push_back(e);
			else
			{	
				delete[] e->input;
				delete e;
			}

		}	
		if(iter%30000 == 0) 
		{
				// std::cout << vocSize << std::endl;
				std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ") " << std::flush;
		}
		iter++;
	}
	docFile.close();
	std::string vocabulary[10000];
	FILE* f;
	f = fopen("/data/pfahler/corpora/penn/processed/penn.voc", "rb");
	while(!feof(f)){
		
		char word[100];
		char ch;
		int token;
		int freq;
		fscanf(f, "%s%c%d%d", &word[0], &ch,&token,&freq);
		vocabulary[token]=std::string(word);
	}
	fclose(f);
	std::cout << "DONE" <<  std::endl;

	ReinforceChunker chunker;
	chunker.init();
	for(int i=0;i<100;i++)
	{
		for(int j =0;j<10000;j++)
		{
			ReinforceChunker::Trajectory** trajectories = new ReinforceChunker::Trajectory*[n];
			double L = 0;

			#pragma omp parallel for
			for(int k=0;k<n;k++)
			{
				trajectories[k] = chunker.sampleTrajectory(dataset[j]);				
			}
			double* a = new double[trajectories[0]->T];
			double minAgreement = 1.0;
			double avgAgreement = 0;
			double minL = 1000000000;
			for(int k=0;k<n;k++)
			{
				L+=trajectories[k]->L;
				if(trajectories[k]->L < minL)
					minL = trajectories[k]->L;
			}
			for(int kk = 0; kk<trajectories[0]->T;kk++)
			{
				a[kk] = 0;
				for(int k=0;k<n;k++)
					a[kk]+=trajectories[k]->decisions[kk];
				a[kk]/=float(n);
				double agreement = a[kk]>0.5 ? a[kk] : 1.0-a[kk];
				if(agreement < minAgreement)
					minAgreement = agreement;
				avgAgreement += 1.0/trajectories[0]->T * agreement;
				
			}
			for(int kk = 0; kk<trajectories[0]->T;kk++)
			{
				if(a[kk]>0.5)
					std::cout << vocabulary[dataset[j]->input[kk]] << " ";
				else
					std::cout << vocabulary[dataset[j]->input[kk]] << " | ";
			}
			delete[] a;
			L/=float(n);
			std::cout << " L: "<<L<< "\t" << minL << "\t" << minAgreement << "\t" << avgAgreement <<  std::endl;
			
			
			#pragma omp parallel for
			for(int k=0;k<n;k++)
			{
				trajectories[k]->L-=L;
				chunker.backpropTrajectory(trajectories[k]);
			}
			chunker.gradientStep(n);	
		}
	}
	return 0;
}