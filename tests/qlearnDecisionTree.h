#pragma once
#include <random>
#include <ostream>
#include "computeUnit.h"
#include "dummyJacobian.h"
#include "dataInterfaces.h"
#include "sigmoid.h"
#include "softmaxLinearLayer.h"

#include <Eigen/Dense>
using Eigen::MatrixXd;

std::mt19937 samplergen;
std::uniform_real_distribution<> uniform(0,1);

template<int dimension,int numLabels,int maxRecordSize, int sampleSize,bool zeroOne>
class QLearnDecisionTree
{
	public:
		

		SigmoidUnit<dimension,2>** inner;
		SoftmaxLinearLayer<dimension,numLabels>** leaves;
		SigmoidUnit<dimension,2>** innerHat;
		SoftmaxLinearLayer<dimension,numLabels>** leavesHat;
		int height;
		int numNodes;
		std::vector<std::tuple<MatrixXd*,int,int,double>> record; //input, node, descendant, reward

		bool logging = false;
		int** statistics;


		QLearnDecisionTree(int height)
		{
			this->height = height - 1;
			this->numNodes = (1 << this->height) - 1;
			this->inner = new SigmoidUnit<dimension,2>*[numNodes];
			this->innerHat = new SigmoidUnit<dimension,2>*[numNodes];
			this->leaves = new SoftmaxLinearLayer<dimension,numLabels>*[numNodes+1];
			this->leavesHat = new SoftmaxLinearLayer<dimension,numLabels>*[numNodes+1];
			for(int i=0;i<numNodes;i++)
			{
				this->inner[i] = new SigmoidUnit<dimension,2>();
				this->inner[i]->init();
				this->innerHat[i]=inner[i]->deepCopy();

			}
			for(int i=0;i<numNodes+1;i++)
			{
				this->leaves[i] = new SoftmaxLinearLayer<dimension,numLabels>();
				this->leaves[i]->init();
				this->leavesHat[i] = leaves[i]->deepCopy();
			}
			statistics = new int*[2*numNodes + 1];
			for(int i = 0;i< 2*numNodes+1;i++)
			{
				statistics[i] = new int[numLabels];
			}
		}

		void startLogging()
		{
			logging = true;
			for(int i = 0;i< 2*numNodes+1;i++)
				for(int j = 0;j<numLabels;j++)
					statistics[i][j] = 0;
		}

		void endLoggingAndFlushLogs(std::ostream& out)
		{
			for(int i = 0;i< 2*numNodes+1;i++)
			{
				int abs = 0;
				for(int j = 0;j<numLabels;j++)
				{
					if(j>0)
						out << "\t";
					out << statistics[i][j];
					abs+=statistics[i][j];
				}
				out << "\t" << abs << std::endl;
			}
			logging = false;
		}


		int createRecord(MatrixXd* x, int y, double eps)
		{
			int node = 0;
			for(int i=0;i<height;i++)
			{
				if(logging) statistics[node][y]++;
				if(uniform(samplergen) < eps)
				{
					if(uniform(samplergen) < 0.5)
						node = (node + 1) * 2 - 1;
					else
						node = (node + 1) * 2;
				}
				else
				{
					this->innerHat[node]->inputs[0]=x;
					this->innerHat[node]->forward();
					if(this->innerHat[node]->outputs[0](0,0) < this->innerHat[node]->outputs[0](1,0))
						node = (node + 1) * 2 - 1;
					else
						node = (node + 1) * 2;
				}
				// std::cout << node<<" " << numNodes << std::endl;
			}
			if(logging) statistics[node][y]++;
			int output;
			if(uniform(samplergen) < eps)
			{
				output = rand() % numLabels;
			}
			else
			{
				this->leavesHat[node-numNodes]->inputs[0]=x;
				this->leavesHat[node-numNodes]->forward();
				// output = leavesHat[node-numNodes]->min();
				output = leavesHat[node-numNodes]->map();
				if(!zeroOne && output != y)
				{
					this->leaves[node-numNodes]->inputs[0]=x;
					this->leaves[node-numNodes]->forward();
					double error = this->leaves[node-numNodes]->outputs[0](y,0)-1;

					DummyJacobian<numLabels> jac;
					jac.jacobians[0] = MatrixXd::Zero(numLabels,1);
					jac.jacobians[0](y,0) = 2 * error;
					connectDepth(this->leaves[node-numNodes],&jac);
					this->leaves[node-numNodes]->backward();
					this->leaves[node-numNodes]->gradientStep(0.01);
				}
			}
			double reward = output == y ? 0 : 1;
			record.push_back(std::make_tuple(x,node,output,reward));
			for(int i=height-1;i>=0;i--)
			{
				record.push_back(std::make_tuple(x,(node+1)/2 - 1,node,0)); //maybe reward = 0?
				node = (node+1)/2 - 1;
			}

			//simulate ring buffer
			while(record.size()>maxRecordSize)
			{
				record.erase(record.begin());
			}
			return output;
		}

		void learnRecord(double alpha)
		{
			double aerror = 0;
			auto iterator = record.rbegin();
			for(int i=0;i<sampleSize;i++)
			{
				std::tuple<MatrixXd*,int,int,double> r;
				if(i<height + 1)
				{
					r = *iterator;
					iterator++;
				}
				else
				{
					int j = rand() % record.size();
					r = record[j];
				}
				//DO SOME GRADIENT UPDATES!!!!!!
				int descendant = std::get<2>(r);
				int node = std::get<1>(r);
				MatrixXd* x = std::get<0>(r);
				double reward = std::get<3>(r);
				// std::cout << node << " " << descendant << " " << reward << std::endl;
				if(node < numNodes && descendant<numNodes)
				{
					// step sigmoid -> sigmoid 
					this->inner[descendant]->inputs[0] = x;
					this->inner[descendant]->forward();
					int action = this->inner[descendant]->outputs[0](0,0) < this->inner[descendant]->outputs[0](1,0);
					double y = reward + this->inner[descendant]->outputs[0](action,0);
					
					this->inner[node]->inputs[0] = x;
					this->inner[node]->forward();
					action = descendant == (node + 1) * 2 - 1 ? 0 : 1;

					//minimize L2 on y, and q(action)
					double error = y - this->inner[node]->outputs[0](action,0);
					aerror+=error;
					DummyJacobian<2> jac;
					jac.jacobians[0] = MatrixXd::Zero(2,1);
					jac.jacobians[0](action,0) = -2 * error;
					connectDepth(this->inner[node],&jac);
					this->inner[node]->backward();
					this->inner[node]->gradientStep(alpha);
				}
				else if(node < numNodes)
				{
					// step sigmoid -> softmax
					this->leaves[descendant-numNodes]->inputs[0] = x;
					this->leaves[descendant-numNodes]->forward();
					int action = this->leaves[descendant-numNodes]->min();
					double y = reward + this->leaves[descendant-numNodes]->outputs[0](action,0);

					this->inner[node]->inputs[0] = x;
					this->inner[node]->forward();
					action = descendant == (node + 1) * 2 - 1 ? 0 : 1;

					//minimize L2 on y, and q(action)
					double error = y - this->inner[node]->outputs[0](action,0);
					aerror+=error;
					DummyJacobian<2> jac;
					jac.jacobians[0] = MatrixXd::Zero(2,1);
					jac.jacobians[0](action,0) = -2 * error;
					connectDepth(this->inner[node],&jac);
					this->inner[node]->backward();
					this->inner[node]->gradientStep(alpha);
				}
				else
				{
					//step softmax -> prediction
					int action = descendant;
					double y = reward;
					this->leaves[node-numNodes]->inputs[0] = x;
					this->leaves[node-numNodes]->forward();
					double error = y - (1-this->leaves[node-numNodes]->outputs[0](action,0));
					aerror+=error;
					DummyJacobian<numLabels> jac;
					jac.jacobians[0] = MatrixXd::Zero(numLabels,1);
					jac.jacobians[0](action,0) = 2 * error;
					connectDepth(this->leaves[node-numNodes],&jac);
					this->leaves[node-numNodes]->backward();
					this->leaves[node-numNodes]->gradientStep(alpha);
				}
				
			}
			// std::cout << aerror/sampleSize << " ";
		}

		void flip()
		{
			for(int i=0;i<numNodes;i++)
			{
				*(this->innerHat[i]->Uyx) = *(this->inner[i]->Uyx);
				*(this->innerHat[i]->by) = *(this->inner[i]->by);
			}
			for(int i=0;i<numNodes + 1;i++)
			{
				// *(this->leavesHat[i]->Uyx) = *(this->leaves[i]->Uyx);
				// *(this->leavesHat[i]->by) = *(this->leaves[i]->by);
				*(this->leavesHat[i]->W) = *(this->leaves[i]->W);
				*(this->leavesHat[i]->b) = *(this->leaves[i]->b);
			}
		}

};