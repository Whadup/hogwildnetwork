// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include "linearlayer.h"
// #include "relulayer.h"
#include "oneHotLayer.h"
// #include "embedAndReduceLayer.h"
#include "crossEntropyLoss.h"
#include "softmaxLayer.h"
#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"
#include "recurrentWrapper.h"
#include <stdlib.h>
#include <vector>
#include <string>
//#include "lstmgen2.h"
#include "loadBinarySparse.h"
#include "associativeMemory.h"

using Eigen::MatrixXd;
//#define EIGEN_DONT_PARALLELIZE



int main()
{	
	Eigen::initParallel();
	std::cout << std::thread::hardware_concurrency() << std::endl;
	OneHotLayer<3> em;
	const int numberOfLabels = 3;
	const int hiddenDimension = 4;
	AssociativeMemory<3,4,20> lstm;
	const int outputDimension = 6;
	Matrix<double,outputDimension,outputDimension> complex1 = Matrix<double,outputDimension,outputDimension>::Zero();
	complex1.topLeftCorner(outputDimension>>1,outputDimension>>1).setIdentity();
	complex1.bottomLeftCorner(outputDimension>>1,outputDimension>>1).setIdentity();
	Matrix<double,outputDimension,outputDimension> complex2 = Matrix<double,outputDimension,outputDimension>::Zero();
	complex2.topRightCorner(outputDimension>>1,outputDimension>>1) = - Matrix<double,(outputDimension>>1),(outputDimension>>1)>::Identity();
	complex2.bottomRightCorner(outputDimension>>1,outputDimension>>1).setIdentity();
	std::cout << complex2 << std::endl;	
	// LSTM<hiddenDimension,hiddenDimension> lstm2;
	// LSTM<hiddenDimension,hiddenDimension> lstm3;
	// LSTM<hiddenDimension,hiddenDimension> lstm4;
	// LSTM<hiddenDimension,hiddenDimension> lstm5;
	LinearLayer<hiddenDimension,numberOfLabels> lin2;
	SoftmaxLayer<numberOfLabels> softmax;
	CrossEntropyLoss<numberOfLabels> loss;
	//LSTM<16,16> lstm3;
	connectDepth((ComputeUnit&)em, 		(ComputeUnit&)lstm);
	// connecty((ComputeUnit&)lstm, 	(ComputeUnit&)lstm2);
	// connecty((ComputeUnit&)lstm2, 	(ComputeUnit&)lstm3);
	// connecty((ComputeUnit&)lstm3, 	(ComputeUnit&)lstm4);
	// connecty((ComputeUnit&)lstm4, 	(ComputeUnit&)lstm5);
	connectDepth((ComputeUnit&)lstm,	(ComputeUnit&) lin2);
	connectDepth((ComputeUnit&)lin2,    (ComputeUnit&)softmax);
	connectDepth((ComputeUnit&)softmax,	(ComputeUnit&)loss);
	//connecty((ComputeUnit&)lstm, 		(ComputeUnit&)lstm2);
	//connecty((ComputeUnit&)lstm2, 		(ComputeUnit&)lstm3);
	//connecty((ComputeUnit&)rnn, 		(ComputeUnit&)rnn2);


	RecurrentWrapper<0> wrapper(new NetworkExecuter(&em,&loss));
	//ReLuLayer<32,128> relu;
	// ReLuLayer<64,32> relu2;
	
	std::cout << "Netzwerk erstellt" << std::endl;
	
	std::cout << (1.0 + Matrix<double,3,3>::Zero().array()).matrix() << std::endl;

	
	

	// std::cout << &wrapper << std::endl << &em << std::endl << &rnn << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&wrapper);
	
	
	std::cout << "Create Data" << std::endl;
	std::vector<SeqSeqExample*> dataset;
	for(int j=0;j<25;j++)
	{
		int a = 1 + rand() % 50; // 2*(1 + rand()% 20);
		// if(j==15) a = 50;
		std::cout << a << " ";
		//if(a>=8 && a<=11) continue;
		// a*=2;
		int* bla = new int[1+a*2];
		int* blub = new int[1+a*2];
		SeqSeqExample* ex = new SeqSeqExample();
		
		bla[0] = 0;
		for(int i=0;i<a;i++)
			bla[1+i] = 1;
		for(int i=0;i<a;i++)
			bla[1+a+i] = 2;
		for(int i=0;i<a*2;i++)
			blub[i] = bla[1+i];
		blub[2*a] = 0;
		// for(int i=0;i<=a*2;i++)
		// 	std::cout << bla[i];
		// std::cout<<std::endl;
		// for(int i=0;i<=a*2;i++)
		// 	std::cout << blub[i];
		// std::cout<<std::endl;
		
		ex->l  = a*2 + 1;
		ex->ll = a*2 + 1;
		ex->input = bla;
		ex->y = blub;

		dataset.push_back(ex);
	}
	std::cout << std::endl;
	for(int j=0;j<10000;j++)
	{
		int a = rand() % 8;
		dataset.push_back(dataset[a]);
	}
	std::vector<SeqSeqExample*> dataset2;
	for(int j=2;j<100;j++)
	{

		int a = j;
		int* bla = new int[1+a*2];
		int* blub = new int[1+a*2];
		SeqSeqExample* ex = new SeqSeqExample();
		

		bla[0] = 0;
		for(int i=0;i<a;i++)
			bla[1+i] = 1;
		for(int i=0;i<a;i++)
			bla[1+a+i] = 2;
		for(int i=0;i<a*2;i++)
			blub[i] = bla[1+i];
		blub[2*a] = 0;
		// for(int i=0;i<=a*2;i++)
		// 	std::cout << bla[i];
		// std::cout << std::endl;
		ex->l  = a*2 + 1;
		ex->ll = a*2 + 1;
		ex->input = bla;
		ex->y = blub;

		dataset2.push_back(ex);
	}
	std::cout << "das hat geklappt, jetzt ab in die memory data source"<< std::endl;
	
	
	const double stepsize = 0.000100;
	std::cout << "StepSize: "<<stepsize << std::endl;

	// ((RecurrentWrapper<32>*)f->layers[0])->l = 6;
	// ((RecurrentWrapper<32>*)f->layers[0])->input = new int[6]{1,2,3,4,5,6};
	// ((CrossEntropyLoss<5>*)f->layers[f->numLayers-1])->y = 1;
	// f->forward(); f->backward(); f->gradientStep(0.0001);
	for(int iter = 0;iter<1;iter++)
	{	
		MemoryDataSource<SeqSeqExample> d(dataset,1);
		d.setNumberOfEpoches(20);
		HogwildManager<SeqSeqExample,1>* h = new HogwildManager<SeqSeqExample,1>(e,&d);
		h->go(stepsize);
		// std::cout << "DONE" << std::endl;
		// std::cout << "DONE" << std::endl;
		// std::cout << "DONE" << std::endl;
		// std::cout << "DONE" << std::endl;

		double krawumm = 0;
		for(int i=0;i<dataset2.size();i++)
		{
			wrapper.input = dataset2[i]->input;
			wrapper.l = dataset2[i]->l;
			wrapper.y = dataset2[i]->y;
			wrapper.ll = dataset2[i]->ll;
			wrapper.forward();
			wrapper.debugOutputs();
			krawumm += wrapper.acc();
			
			// std::cout << i << " " << wrapper.acc() << " " << (1.0*krawumm/(i+1)) << std::endl;
		}
		std::cout << 1.0*krawumm/dataset2.size() << std::endl;		
	}
	
}