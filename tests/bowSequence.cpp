// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <stdlib.h>
#include <vector>
#include <string>

#include "embeddingLayer.h"
//#include "lstmgen3.h"
// #include "ann.h"
#include "rnn2.h"
// #include "lstm2.h"//graves
#include "hierarchicalSoftmaxCrossEntropy.h"
#include "bowRecurrentWrapper.h"
#include "bowWrapper.h"
#include "crossEntropyLoss.h"

#include "loadBinarySparse.h"

#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"


//#define FLAT
//#define LSTM
using Eigen::MatrixXd;
#define EIGEN_DONT_PARALLELIZE

//2^(890415/79078*)


int main()
{	
	const int dimension = 100;
	HierarchicalSoftmaxCrossEntropy<dimension,5005> softmax;

	EmbeddingLayer<3883,dimension> em;
	RNN<dimension,dimension> rnn;
	// ANN<dimension,dimension> rnn2;
	BowWrapper<dimension> bow(new NetworkExecuter(&softmax));

	// connectDepth((ComputeUnit&)em, 		(ComputeUnit&)bow);
	connectDepth((ComputeUnit&)em, 		(ComputeUnit&)rnn);
	// connectDepth((ComputeUnit&)rnn, 		(ComputeUnit&)rnn2);
	connectDepth((ComputeUnit&)rnn,	(ComputeUnit&)bow);

	BowRecurrentWrapper<0> wrapper(new NetworkExecuter(&em,&bow));

	std::cout << "Netzwerk erstellt" << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&wrapper);
	e->init();

	int iter  = 0;
	double bytes = 0;
	double start = getTime();
	std::vector<int> tmp;

	std::vector<SeqBowSeqExample*> dataset;
	std::ifstream docFile("/data/pfahler/relnet/processed_data/ahlusunnah/posts.json.int");
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		if(line.size())
		{	
			int l = toInt(line.c_str(),line.size());
			// std::cout << l << " ";

			SeqBowSeqExample* e = new SeqBowSeqExample;
			e->y = new int*[l];
			e->lll = new int[l];
			e->ll = l;
			e->l = l;
			e->input = new int[e->l];

			
			for(int i=0;i<l;i++)
			{
				getline(docFile, line);
				bytes += line.size();
				// std::cout << "raw input " <<line << std::endl;
				e->y[i] = parseIntegerTokens(line, tmp,e->lll[i]);
				e->lll[i]-=2;
				e->input[i] = e->y[i][0];
				e->y[i]++;
				// std::cout << "parse: ";
				// for(int k = 0;k < e->lll[i];k++)
				// 	std::cout << e->y[i][k] << " ";
				dataset.push_back(e);
			}
		}	
	}
	docFile.close();

	

	MemoryDataSource<SeqBowSeqExample> d(dataset,1);
	d.setNumberOfEpoches(2);
	double stepsize = 0.001;
	HogwildManager<SeqBowSeqExample,1>* h = new HogwildManager<SeqBowSeqExample,1>(e,&d);
	h->go(stepsize);
	// std::cout << *(rnn.Usts) << std::endl;
	std::cout << std::endl;
	for(int i=0;i<3882;i++)
		std::cout << i << " " << em.W->col(i).transpose() << std::endl;
	
	
}