#pragma once
void loadParticleData(std::vector<MatrixXd>& dataset, std::vector<int>& labels)
{
	std::ifstream is("/Users/lukas/Downloads/particles.csv");
	unsigned int width=50;
	  
	double d;
	int i = 0;
	
	is.ignore(1);
	int pos;
	int neg;
	is >> pos;
	is >> neg;
	std::cout << pos << " " << neg;
	MatrixXd read = MatrixXd::Zero(width,1);
	while (is >> d)
	{
		read(i % width,0) = d;
		if(i % width == width-1)
		{
			dataset.push_back(read);
			read = MatrixXd::Zero(width,1);
		//	std::cout << std::endl;
		}
		//std::cout << d << " ";
		is.ignore(1);
		i++;
	}
	std::cout << "loaded data";
	
	for(int i=0;i<pos;i++)
		labels.push_back(1);
	for(int i=0;i<neg;i++)
		labels.push_back(0);
}

void loadLetterData(std::vector<MatrixXd>& dataset, std::vector<int>& labels, std::vector<MatrixXd>& testset, std::vector<int>& testLabels)
{
	//train data
	char buffer[100];
	std::ifstream is("/data/pfahler/classification/decisiontreedatasets/letterorig_train.data");
	unsigned int width=16;
	is.getline(buffer,100);
	is.getline(buffer,100);
	is.getline(buffer,100);
	double d;
	MatrixXd mean = MatrixXd::Zero(width*width,1);
	MatrixXd read = MatrixXd::Zero(width*width,1);
	while (!is.eof())
	{
		for(int i=0;i<width;i++)
		{
			is >> d;
			read(16*i+(int)d,0) = 1;//d/15.0;
			is.ignore(1);
		}
		// std::cout << read.transpose() << std::endl;
		mean+=read;
		dataset.push_back(read);
		read = MatrixXd::Zero(width*width,1);
	}
	is.close();
	mean = mean.array() / dataset.size();
	for(int i = 0;i<dataset.size();i++)
		dataset[i]-=mean;


	// Test Data
	std::ifstream tis("/data/pfahler/classification/decisiontreedatasets/letterorig_test.data");
	tis.getline(buffer,100);
	tis.getline(buffer,100);
	tis.getline(buffer,100);
	read = MatrixXd::Zero(width*width,1);
	while (!tis.eof())
	{
		for(int i=0;i<width;i++)
		{
			tis >> d;
			read(16*i+(int)d,0) = 1;//
			//read(i,0) = d/15.0;
			tis.ignore(1);
		}
		testset.push_back(read-mean);
		read = MatrixXd::Zero(width*width,1);
	}
	tis.close();

	//train labels
	std::ifstream lis("/data/pfahler/classification/decisiontreedatasets/letterorig_train.label");
	lis.getline(buffer,100);
	lis.getline(buffer,100);
	lis.getline(buffer,100);
	while (!lis.eof())
	{
		int l;
		lis >> l;
		labels.push_back(l-1);
	}
	lis.close();

	//test labels
	std::ifstream tlis("/data/pfahler/classification/decisiontreedatasets/letterorig_test.label");
	tlis.getline(buffer,100);
	tlis.getline(buffer,100);
	tlis.getline(buffer,100);
	while (!tlis.eof())
	{
		int l;
		tlis >> l;
		testLabels.push_back(l-1);
	}
	tlis.close();
}

void loadUSPSData(std::vector<MatrixXd>& dataset, std::vector<int>& labels, std::vector<MatrixXd>& testset, std::vector<int>& testLabels)
{
	//train data
	char buffer[100];
	std::ifstream is("/data/pfahler/classification/decisiontreedatasets/usps_train.data");
	unsigned int width=256;
	is.getline(buffer,100);
	is.getline(buffer,100);
	is.getline(buffer,100);
	double d;
	MatrixXd mean = MatrixXd::Zero(width,1);
	MatrixXd read = MatrixXd::Zero(width,1);
	while (!is.eof())
	{
		for(int i=0;i<width;i++)
		{
			is >> d;
			read(i,0) = d;
			is.ignore(1);
		}
		// std::cout << read.transpose() << std::endl;
		mean+=read;
		dataset.push_back(read);
		read = MatrixXd::Zero(width,1);
	}
	is.close();
	mean = mean.array() / dataset.size();
	for(int i = 0;i<dataset.size();i++)
		dataset[i]-=mean;


	// Test Data
	std::ifstream tis("/data/pfahler/classification/decisiontreedatasets/usps_test.data");
	tis.getline(buffer,100);
	tis.getline(buffer,100);
	tis.getline(buffer,100);
	read = MatrixXd::Zero(width,1);
	while (!tis.eof())
	{
		for(int i=0;i<width;i++)
		{
			tis >> d;
			read(i,0) = d;
			tis.ignore(1);
		}
		testset.push_back(read-mean);
		read = MatrixXd::Zero(width,1);
	}
	tis.close();

	//train labels
	std::ifstream lis("/data/pfahler/classification/decisiontreedatasets/usps_train.label");
	lis.getline(buffer,100);
	lis.getline(buffer,100);
	lis.getline(buffer,100);
	while (!lis.eof())
	{
		int l;
		lis >> l;
		labels.push_back(l-1);
	}
	lis.close();

	//test labels
	std::ifstream tlis("/data/pfahler/classification/decisiontreedatasets/usps_test.label");
	tlis.getline(buffer,100);
	tlis.getline(buffer,100);
	tlis.getline(buffer,100);
	while (!tlis.eof())
	{
		int l;
		tlis >> l;
		testLabels.push_back(l-1);
	}
	tlis.close();
}

void loadMNistData(std::vector<MatrixXd>& dataset, std::vector<int>& labels, std::vector<MatrixXd>& testset, std::vector<int>& testLabels)
{
	std::ifstream is("/data/pfahler/classification/MNIST/mnist_train.csv");
	unsigned int width=28*28;
	  
	int d;
	int i = 0;
	MatrixXd mean = MatrixXd::Zero(width,1);
	MatrixXd read = MatrixXd::Zero(width,1);
	while (!is.eof())
	{
		int l=0;
		is >> l;
		// std::cout << l << " ";
		is.ignore(1);
		labels.push_back(l);
		for(int i=0;i<28*28;i++)
		{
			is >> d;
			read(i,0) = d/256.0;
			is.ignore(1);
		}
		// std::cout << read.transpose() << std::endl;
		mean+=read;
		dataset.push_back(read);
		read = MatrixXd::Zero(width,1);
	}
	is.close();
	mean = mean.array() / dataset.size();
	for(int i = 0;i<dataset.size();i++)
		dataset[i]-=mean;


	std::ifstream tis("/data/pfahler/classification/MNIST/mnist_test.csv");	  
	i = 0;
	read = MatrixXd::Zero(width,1);
	while (!tis.eof())
	{
		int l=0;
		tis >> l;
		// std::cout << l << " ";
		tis.ignore(1);
		testLabels.push_back(l);
		for(int i=0;i<28*28;i++)
		{
			tis >> d;
			read(i,0) = d/256.0;
			tis.ignore(1);
		}
		// std::cout << (read-mean).transpose() << std::endl;
		testset.push_back(read-mean);
		read = MatrixXd::Zero(width,1);
	}

	tis.close();

	
}