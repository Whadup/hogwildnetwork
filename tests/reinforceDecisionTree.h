#pragma once
#include <random>
#include <ostream>
#include "computeUnit.h"
#include "dummyJacobian.h"
#include "dataInterfaces.h"
#include "sigmoid.h"
#include "softmaxLinearLayer.h"

#include <Eigen/Dense>
using Eigen::MatrixXd;

std::mt19937 samplergen;
std::uniform_real_distribution<> uniform(0,1);

template<int dimension,int numLabels,bool oneZeroLoss>
class ReinforceDecisionTree
{
public:
	SigmoidUnit<dimension,1>** sigmoids;
	SoftmaxLinearLayer<dimension,numLabels>** leaves;
	int height;
	int numNodes;
	bool* used;
	bool logging = false;
	int** statistics;

	ReinforceDecisionTree(int h)
	{
		height = h - 1;
		numNodes = (1 << height) - 1;
		used = new bool[2*numNodes+1];
		sigmoids = new SigmoidUnit<dimension,1>*[numNodes];
		leaves = new SoftmaxLinearLayer<dimension,numLabels>*[numNodes+1];
		for(int i=0;i<numNodes;i++)
		{
			sigmoids[i] = new SigmoidUnit<dimension,1>();
			sigmoids[i]->init();
		}
		for(int i=0;i<numNodes+1;i++)
		{
			leaves[i] = new SoftmaxLinearLayer<dimension,numLabels>();
			leaves[i]->init();
		}
		statistics = new int*[2*numNodes + 1];
		for(int i = 0;i< 2*numNodes+1;i++)
		{
			statistics[i] = new int[numLabels];
		}
	}

	void startLogging()
	{
		logging = true;
		for(int i = 0;i< 2*numNodes+1;i++)
			for(int j = 0;j<numLabels;j++)
				statistics[i][j] = 0;
	}

	void endLoggingAndFlushLogs(std::ostream& out)
	{
		for(int i = 0;i< 2*numNodes+1;i++)
		{
			int abs = 0;
			for(int j = 0;j<numLabels;j++)
			{
				if(j>0)
					out << "\t";
				out << statistics[i][j];
				abs+=statistics[i][j];
			}
			out << "\t" << abs << std::endl;
		}
		logging = false;
	}

	struct Trajectory
	{
		int* decisions;
		SigmoidUnit<dimension,1>** s;
		SoftmaxLinearLayer<dimension,numLabels>* leaf;
		double L;
		double accuracy;
		int sample;
	};

	Trajectory* sampleTrajectory(MatrixXd* input, int y)
	{
		Trajectory* t = new Trajectory;
		t->decisions = new int[height];
		int node = 0;
		t->s = new SigmoidUnit<dimension,1>*[height];
		for(int i=0;i<height;i++)
		{
			t->s[i] = (SigmoidUnit<dimension,1>*) sigmoids[node]->duplicate();
			t->s[i]->inputs[0] = input;
			t->s[i]->forward();
			used[node] = 1;
			if(logging) statistics[node][y]++;
			if(uniform(samplergen)<t->s[i]->outputs[0](0,0))
			{
				node = 2 * (node + 1) - 1;
				t->decisions[i] = 0;
			}
			else
			{
				node = 2 * (node + 1);
				t->decisions[i] = 1;
			}
		}
		if(logging) statistics[node][y]++;
		used[node]=1;
		t->leaf = (SoftmaxLinearLayer<dimension,numLabels>*)leaves[node-numNodes]->duplicate();
		t->leaf->inputs[0] = input;
		t->leaf->forward();
		if(oneZeroLoss)
			t->sample = t->leaf->sample(uniform(samplergen));
		else
			t->sample = t->leaf->map();
		if(t->sample == y)
			t->accuracy = 1;
		else
			t->accuracy = 0;
		if(oneZeroLoss)
			t->L = 1-t->accuracy;
		else
			t->L = -log(t->leaf->outputs[0](y,0));
		return t;
	}	

	/*
	** perform backprop along a trajectory. deletes the trajectory.
	*/
	void backpropTrajectory(Trajectory* t, int y)
	{
		
		int node = 0;
		for(int i=0;i<height;i++)
		{
			double p;
			DummyJacobian<1> jac;
			jac.jacobians[0] = MatrixXd::Zero(1,1);
			connectDepth(t->s[i],&jac);
			if(t->decisions[i] == 0)
				p = t->s[i]->outputs[0](0,0);
			else
				p = -(1.0 - t->s[i]->outputs[0](0,0));
			
			jac.jacobians[0](0,0) = (t->L)  * 1.0/p; //dirty evil scumbag of a hack
			t->s[i]->backward();
			delete t->s[i];
		}
		DummyJacobian<numLabels> jac;
		jac.jacobians[0] = MatrixXd::Zero(numLabels,1);
		connectDepth(t->leaf,&jac);
		if(oneZeroLoss)
			jac.jacobians[0](t->sample,0) += t->L * 1/t->leaf->outputs[0](t->sample,0);
		else
			jac.jacobians[0](y,0) += -1/t->leaf->outputs[0](y,0);
		t->leaf->backward();
		delete t->leaf;
		delete[] t->s;
		delete[] t->decisions;
		delete t;
	}

	void gradientStep(double stepize, int n)
	{
		for(int i=0;i<numNodes;i++)
		{
			if(used[i])
				sigmoids[i]->gradientStep(stepize/n);
			used[i]=0;
		}
		for(int i=0;i<numNodes+1;i++)
		{
			if(used[i+numNodes])
				leaves[i]->gradientStep(stepize/n);
			used[i+numNodes]=0;
		}
	}

};