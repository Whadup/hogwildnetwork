
#include <iostream>
#include <Eigen/Dense>
#include "linearlayer.h"
#include "relulayer.h"
#include "embedAndReduceLayer.h"
#include "crossEntropyLoss.h"
#include "softmaxLayer.h"
#include <stdlib.h>
using Eigen::MatrixXd;




int main()
{	
	EmbedAndReduceLayer<140000,64,true> em;
	LinearLayer<64,32> lin;
	LinearLayer<32,2> lin2;
	SoftmaxLayer<2> softmax;
	CrossEntropyLoss<2> loss;

	connecty((ComputeUnit&)em, 		(ComputeUnit&)lin);
	connecty((ComputeUnit&)lin,		(ComputeUnit&)lin2);
	connecty((ComputeUnit&)lin2,	(ComputeUnit&)softmax);
	connecty((ComputeUnit&)softmax,	(ComputeUnit&)loss);
	
	em.init();
	lin.init();
	lin2.init();
	
	double averageLoss = 0;
	em.input = new int[1];
	int iters=500000;
	double stepsize= 0.01;
	for(int i=0;i<iters;i++)
	{
		delete em.input;
		em.l = 2*(rand()%10) + 1;
		em.input=new int[em.l];//
		int sum = 0;
		for(int j=0;j<em.l;j++)
		{
			em.input[j]=rand()%3000;
			sum += em.input[j] % 2 ? 1 : -1;
		}
		loss.y = sum < 0 ? 0 : 1;
		
		em.forward();
		lin.forward();
		lin2.forward();
		softmax.forward();
		loss.forward();
		// std::cout << softmax.outputs[0].transpose()  << " " << loss.y << std::endl;
		// std::cout << lin2.outputs[0].transpose() << "~" << lin.outputs[0].transpose() << std::endl;
		
		loss.backward();	
		softmax.backward();
		lin2.backward();
		lin.backward();
		em.backward();

		em.gradientStep(stepsize);
		lin.gradientStep(stepsize);
		lin2.gradientStep(stepsize);

		averageLoss+=loss.loss();
		if(i%1000==999 && i+1<iters)
		{
			std::cout << "loss "<< averageLoss/1000.0 << std::endl;
			averageLoss = 0;
			stepsize*=0.99;
			std::cout << stepsize << std::endl;
		}
	}
	if(averageLoss/1000.0<0.01)
	{
		std::cout << "success"<<std::endl;
		return 0;
	}
	std::cout << "failure"<<std::endl;
	return 1;
	
}