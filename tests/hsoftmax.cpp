// clang++ -std=c++11 -g -O3 -mtune=native -march=native -I libs/eigen/ -I . main.cpp -o main
//#define EIGEN_USE_MKL_ALL
#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Core>
#include <stdlib.h>
#include <vector>
#include <string>

#include "embeddingLayer.h"
#include "rnn2.h"//mikolov
//#include "lstm2.h"//graves
#include "hierarchicalSoftmaxCrossEntropy.h"
#include "linearlayer.h"
#include "softmaxLayer.h"
#include "recurrentWrapper.h"
#include "crossEntropyLoss.h"


#include "loadBinarySparse.h"

#include "networkExecuter.h"
#include "hogwildManager.h"
#include "memoryDataSource.h"


//#define FLAT
//#define LSTM
using Eigen::MatrixXd;
#define EIGEN_DONT_PARALLELIZE

//2^(890415/79078*)


int main()
{	
	const int dimension = 100;
	EmbeddingLayer<9644,dimension> em;
#ifdef LSTM
	LSTM2<dimension,dimension> lstm;
#else
	RNN<dimension,dimension> lstm;
#endif
	// LSTM2<dimension,dimension> lstm2;
	connectDepth((ComputeUnit&)em, 		(ComputeUnit&)lstm);
#ifdef FLAT
	LinearLayer<dimension,9644> lin;
	SoftmaxLayer<9644> softmax;
	CrossEntropyLoss<9644> ce;
	//HierarchicalSoftmaxCrossEntropy<dimension,9dimension4> softmax("/Users/lukas/Desktop/penn.tree.bin");

	connectDepth((ComputeUnit&)em, 		(ComputeUnit&)lstm);
	connectDepth((ComputeUnit&)lstm,	(ComputeUnit&)lin);
	// connectDepth((ComputeUnit&)lstm,	(ComputeUnit&)lstm2);
	connectDepth((ComputeUnit&)lin, 	(ComputeUnit&)softmax);
	connectDepth((ComputeUnit&)softmax,	(ComputeUnit&)ce);
	RecurrentWrapper<0> wrapper(new NetworkExecuter(&em,&ce));
#else
	HierarchicalSoftmaxCrossEntropy<dimension,9644> softmax("/data/pfahler/corpora/penn/processed/penn.huffmantree");
	connectDepth((ComputeUnit&)lstm,	(ComputeUnit&)softmax);
	RecurrentWrapper<0> wrapper(new NetworkExecuter(&em,&softmax));
#endif 
	std::cout << "Netzwerk erstellt" << std::endl;

	NetworkExecuter* e = new NetworkExecuter(&wrapper);
	e->init();
	//em.loadEmbeddings("/Users/lukas/Downloads/GoogleNews-vectors-negative300.bin","/Users/lukas/Desktop/penn.voc");

	int iter  = 0;
	double bytes = 0;
	double start = getTime();
	std::vector<int> tmp;
	std::vector<SeqSeqExample*> dataset;
	std::ifstream docFile("/data/pfahler/corpora/penn/processed/penn.train.int");
	while(!docFile.eof()){
		std::string line;
		getline(docFile, line);
		bytes += line.size();
		if(line.size())
		{	

			
			SeqSeqExample* e = new SeqSeqExample;
			e->y = parseIntegerTokens(line, tmp,e->l);
			e->ll = e->l;
			e->input = new int[e->l];
			e->input[0] = 0;
			for(int i=0;i<e->ll-1;i++)
				e->input[i+1] = e->y[i];
			dataset.push_back(e);
		}	
		if(iter%30000 == 0) 
		{
				// std::cout << vocSize << std::endl;
				std::cout << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ") " << std::flush;
		}
		iter++;
	}
	docFile.close();
	std::cout << "DONE" <<  std::endl;


	MemoryDataSource<SeqSeqExample> d(dataset,1);
	d.setNumberOfEpoches(1);

	std::vector<SeqSeqExample*> valset;
	std::ifstream valFile("/data/pfahler/corpora/penn/processed/penn.test.int");
	while(!valFile.eof()){
		std::string line;
		getline(valFile, line);
		bytes += line.size();
		if(line.size())
		{	

			
			SeqSeqExample* e = new SeqSeqExample;
			e->y = parseIntegerTokens(line, tmp,e->l);
			e->ll = e->l;
			e->input = new int[e->l];
			e->input[0] = 0;
			for(int i=0;i<e->ll-1;i++)
				e->input[i+1] = e->y[i];
			valset.push_back(e);
		}	
	}
	valFile.close();

	MemoryDataSource<SeqSeqExample> d2(valset,1);
	d2.setNumberOfEpoches(1);
	
	// softmax.y = 40;
	// e->forward();
	
	// for(int i=0;i<100;i++)
	// {
	// 	softmax.forward();
	// 	softmax.backward();
	// 	softmax.gradientStep(0.1);
	// 	std::cout << softmax.l << " " << pow(2,-softmax.l) << std::endl;;
	// }


	double stepsize = 0.05;
	double lastLoss = 30000;
	for(int i=0;i<50;i++)
	{
		std::cout << "stepsize: " << stepsize << std::endl;
		HogwildManager<SeqSeqExample,1>* h = new HogwildManager<SeqSeqExample,1>(e,&d);
		h->go(stepsize);
		delete h;
		h = new HogwildManager<SeqSeqExample,1>(e,&d2);
		h->go(0);
		if(h->lastLoss/lastLoss >0.99)
			stepsize/=2;
		lastLoss = h->lastLoss;
		delete h;
		d.setNumberOfEpoches(1);
		d2.setNumberOfEpoches(1);
	}
	
	
}