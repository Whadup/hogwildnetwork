#pragma once
#include "dataInterfaces.h"
#include "networkExecuter.h"
#include <mutex>
#include <thread>
#include "lstm2.h"
#include "loss.h"

template<int dimension,int maxSamples>
class BowWrapper : public ComputeUnit, public SequenceLabel, public Loss
{
public:
	NetworkExecuter* innerNetwork;
	NetworkExecuter** duplicates;
	BowWrapper(NetworkExecuter* e);
	virtual ~BowWrapper();
	virtual void init();
	virtual void forward();
	virtual void backward();
	int* numberOfUpdates;
	virtual void gradientStep(double a);
	virtual ComputeUnit* duplicate();
	double lossStats = 0;
	double lastAcc;
	double loss();
	double acc();
	double weight();
};

template<int dimension,int maxSamples>
BowWrapper<dimension,maxSamples>::BowWrapper(NetworkExecuter* e) : ComputeUnit(1,0,0), SequenceLabel(), innerNetwork(e)
{
	jacobians[0]=Matrix<double,dimension,1>::Zero();
	duplicates = 0;
	// std::cout << "new BowWrapper responsible for " << innerNetwork->numLayers << " nodes"<< std::endl;
}
template<int dimension,int maxSamples>
BowWrapper<dimension,maxSamples>::~BowWrapper()
{
}
template<int dimension,int maxSamples>
ComputeUnit* BowWrapper<dimension,maxSamples>::duplicate()
{
	// std::cout << "duplicate BowWrapper" << std::endl;
	BowWrapper<dimension,maxSamples>* ret = new BowWrapper<dimension,maxSamples>(innerNetwork->duplicate());
	ret->numberOfUpdates = numberOfUpdates;
	return (ComputeUnit*) ret;
}
template<int dimension,int maxSamples>
void BowWrapper<dimension,maxSamples>::init()
{
	numberOfUpdates = new int;
	*numberOfUpdates=0;
	innerNetwork->init();
}
template<int dimension,int maxSamples>
void BowWrapper<dimension,maxSamples>::forward()
{
	if(duplicates == 0)
	{
		duplicates = new NetworkExecuter*[maxSamples];
		duplicates[0] = innerNetwork;
		for(int i=1;i<maxSamples;i++)
			duplicates[i] = innerNetwork->duplicate();
	}
	lossStats = 0;
	for(int i=0;i<maxSamples;i++)
		duplicates[i]->layers[0]->inputs[0] = inputs[0];
	//innerNetwork->layers[0]->inputs[0] = inputs[0];
	jacobians[0].setZero();
	// std::cout << inputs[0]->transpose() << std::endl;
	// std::cout << "predicting " << ll <<" words" << std::endl;;
	if(ll<=maxSamples)
	{
		#pragma omp parallel for
		for(int i=0;i<ll;i++)
		{
			// std::cout << y[i] << " ";
			// dynamic_cast<IntegerLabel*>(innerNetwork->layers[innerNetwork->numLayers-1])->y = y[i];
			dynamic_cast<IntegerLabel*>(duplicates[i]->layers[innerNetwork->numLayers-1])->y = y[i];
			duplicates[i]->forward();
			duplicates[i]->backward();
		}
		for(int i=0;i<ll;i++)
		{
			lossStats += dynamic_cast<Loss*>(duplicates[i]->layers[innerNetwork->numLayers-1])->loss();
			// std::cout << dynamic_cast<Loss*>(innerNetwork->layers[innerNetwork->numLayers-1])->loss() << " ";	
			// std::cout << "jacobian from inner: " << innerNetwork->layers[0]->jacobians[0].transpose() << std::endl;
			jacobians[0] += duplicates[i]->layers[0]->jacobians[0];
		}
		// std::cout << std::endl << jacobians[0].transpose() << std::endl;
		if(ll>0)
		{
		// std::cout << std::endl;
			lossStats/=1.0*ll;
			jacobians[0] = (jacobians[0].array()/(1.0*ll)).matrix();	
			(*numberOfUpdates)+=ll;
		}
	}
	else
	{
		for(int i=0;i<maxSamples;i++)
		{
			int j=rand() % ll;
			dynamic_cast<IntegerLabel*>(duplicates[i]->layers[innerNetwork->numLayers-1])->y = y[j];
		}
		#pragma omp parallel for
		for(int i=0;i<maxSamples;i++)
		{
			duplicates[i]->forward();
			duplicates[i]->backward();
		}
		for(int i=0;i<maxSamples;i++)
		{
			lossStats += dynamic_cast<Loss*>(duplicates[i]->layers[innerNetwork->numLayers-1])->loss();
			// std::cout << dynamic_cast<Loss*>(innerNetwork->layers[innerNetwork->numLayers-1])->loss() << " ";	
			// std::cout << "jacobian from inner: " << innerNetwork->layers[0]->jacobians[0].transpose() << std::endl;
			jacobians[0] += duplicates[i]->layers[0]->jacobians[0];
		}
		// std::cout << std::endl << jacobians[0].transpose() << std::endl;
		if(maxSamples>0)
		{
		// std::cout << std::endl;
			lossStats/=1.0*maxSamples;
			jacobians[0] = (jacobians[0].array()/(1.0*maxSamples)).matrix();	
			(*numberOfUpdates)+=maxSamples;
		}
	}
	
}
template<int dimension,int maxSamples>
void BowWrapper<dimension,maxSamples>::backward()
{
	// for(int i = l;i>0;i--)
	// {
	// 	e[i]->backward();
	// }
}
template<int dimension,int maxSamples>
void BowWrapper<dimension,maxSamples>::gradientStep(double alpha)
{
	//TODO THIS IS WRONG: GRADIENT UPDATES SHOULD BE SCALED LOCALLY NOT GLOBALLY
	if(*numberOfUpdates>0)
		innerNetwork->gradientStep(alpha/(*numberOfUpdates));
	// std::cout << *numberOfUpdates << std::endl;
	*numberOfUpdates = 0;
}
template<int dimension,int maxSamples>
double BowWrapper<dimension, maxSamples>::loss()
{
	return lossStats;
}
template<int dimension,int maxSamples>
double BowWrapper<dimension,maxSamples>::acc()
{
	return 0;
}
template<int dimension,int maxSamples>
double BowWrapper<dimension,maxSamples>::weight()
{
	return 1;
}

