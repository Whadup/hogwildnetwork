#pragma once
#include "dataInterfaces.h"
#include "networkExecuter.h"
#include <mutex>
#include <thread>
#include <iomanip>  
#include "lstm2.h"
#include "loss.h"
template<int outputDimension>
class BowRecurrentWrapper : public ComputeUnit, public SequenceInput, public BowSequenceLabel, public Loss
{
public:
	NetworkExecuter* innerNetwork;
	NetworkExecuter** e;
	int maxL;
	BowRecurrentWrapper(NetworkExecuter* e);
	virtual ~BowRecurrentWrapper();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double a);
	virtual ComputeUnit* duplicate();
	double lossStats = 0;
	double lastAcc;
	double loss();
	double acc();
	double weight();
	static std::mutex lock;
	void debugOutputs();
};

template<int outputDimension>
std::mutex BowRecurrentWrapper<outputDimension>::lock;

template<int outputDimension>
BowRecurrentWrapper<outputDimension>::BowRecurrentWrapper(NetworkExecuter* e) : ComputeUnit(0,0,outputDimension>0 ? 1 : 0), SequenceInput(), innerNetwork(e)
{
	maxL = 0;
	e = 0;
	if(outputDimension>0)
		outputs[0]=Matrix<double,outputDimension,1>::Zero();
	std::cout << "new BowRecurrentWrapper responsible for " << innerNetwork->numLayers << " nodes"<< std::endl;
}
template<int outputDimension>
BowRecurrentWrapper<outputDimension>::~BowRecurrentWrapper()
{
	if(e!=0 && maxL>0)
	{
		for(int i = maxL;i>=0;i--)
			delete e[i];
		delete[] e;
	}
}

template<int outputDimension>
ComputeUnit* BowRecurrentWrapper<outputDimension>::duplicate()
{
	std::cout << "duplicate BowRecurrentWrapper" << std::endl;
	BowRecurrentWrapper<outputDimension>* ret = new BowRecurrentWrapper<outputDimension>(innerNetwork);
	return (ComputeUnit*) ret;
}

template<int outputDimension>
void BowRecurrentWrapper<outputDimension>::init()
{
	if(e!=0 && maxL>0)
	{
		for(int i = maxL;i>=0;i--)
			delete e[i];
		delete[] e;
	}
	maxL = 0;
	e = 0;
	innerNetwork->init();
}

template<int outputDimension>
void BowRecurrentWrapper<outputDimension>::forward()
{
	//resize if needed
	if(l > maxL)
	{
		if(e!=0 && maxL>0)
		{
			for(int i = maxL;i>=0;i--)
				delete e[i];
			delete[] e;
		}
		maxL = l;

		e = new NetworkExecuter*[l+1];

		for(int i=0;i<l+1;i++)
		{
			e[i] = innerNetwork->duplicate();
			// std::cout << ".";
		}
	}
	//connect in time
	for(int i=1;i<l+1;i++)
	{
		dynamic_cast<IntegerInput*>(e[i]->layers[0])->input = input[i-1];
		e[i]->layers[innerNetwork->numLayers-1]->successorDeep = 0;
		for(int j=0;j<innerNetwork->numLayers;j++)
		{
			connectTime(e[i-1]->layers[j],e[i]->layers[j]);
			e[i]->layers[j]->successorTime = 0; //important!

			//set labels
			if(outputDimension==0 && innerNetwork->layers[j]->noutputs==0)
			{
				SequenceLabel* outputUnit = dynamic_cast<SequenceLabel*>(e[i]->layers[j]);
				outputUnit->y = this->y[i-1];
				outputUnit->ll = this->lll[i-1];
			}
		}
	}

	for(int i=1;i<l+1;i++)
		e[i]->forward();
	if(outputDimension>0)
	{
		e[l]->layers[innerNetwork->numLayers-1]->successorDeep = this->successorDeep;
		outputs[0] = e[l]->layers[e[l]->numLayers-1]->outputs[0];
	}
	else
	{
		double d = 0;
		double ac = 0;
		int f=0;
		for(int i=1;i<l+1;i++)
		{
			Loss* lossUnit = dynamic_cast<Loss*>(e[i]->layers[innerNetwork->numLayers-1]);
			d += lossUnit->loss();

			ac += lossUnit->acc();
			f++;

		}
		lastAcc = ac;
		lossStats = d;
	}
}
template<int outputDimension>
void BowRecurrentWrapper<outputDimension>::debugOutputs()
{
	// std::cout << l << std::endl;
	// for(int i=1;i<l+1;i++)
	// {
	// 	CrossEntropyLoss<3>* lossUnit = dynamic_cast<CrossEntropyLoss<3>*>(e[i]->layers[innerNetwork->numLayers-1]);
	// 	std::cout << lossUnit->pred;// << " \t\t";
	// }
	// std::cout << std::endl;
	// for(int i=1;i<l+1;i++)
	// {
	// 	CrossEntropyLoss<3>* lossUnit = dynamic_cast<CrossEntropyLoss<3>*>(e[i]->layers[innerNetwork->numLayers-1]);
	// 	std::cout << lossUnit->y;// << " \t\t";
	// }
	// std::cout << std::endl;
	// for(int j=0;j<3;j++)
	// {
	// 	for(int i=1;i<l+1;i++)
	// 	{
	// 		CrossEntropyLoss<3>* lossUnit = dynamic_cast<CrossEntropyLoss<3>*>(e[i]->layers[innerNetwork->numLayers-1]);
	// 		std::cout << std::fixed << ((*lossUnit->inputs[0])(j,0)*100) << " \t";
	// 	}
	// 	std::cout << " " << std::endl;
	// }
	// for(int j=0;j<2;j++)
	// {
	// 	for(int i=1;i<l+1;i++)
	// 	{
	// 		LSTM2<3,2>* unit = dynamic_cast<LSTM2<3,2>*>(e[i]->layers[1]);
	// 		std::cout << std::fixed << ((unit->outputs[2])(j,0)*100) << " \t";
	// 	}
	// 	std::cout << " " << std::endl;
	// }
	// for(int k=0;k<3;k++)
	// {
	// 	for(int i=1;i<l+1;i++)
	// 	{
	// 		CrossEntropyLoss<3>* lossUnit = dynamic_cast<CrossEntropyLoss<3>*>(e[i]->layers[innerNetwork->numLayers-1]);
	// 		std::cout << std::fixed << std::setprecision(1) << (*lossUnit->inputs[0])(k,0) << " ";
	// 	}
	// 	std::cout << std::endl;
	// }
}
template<int outputDimension>
void BowRecurrentWrapper<outputDimension>::backward()
{
	for(int i = l;i>0;i--)
	{
		e[i]->backward();
	}
}
template<int outputDimension>
void BowRecurrentWrapper<outputDimension>::gradientStep(double alpha)
{
	// std::cout << "GRADIENT UPDATE" << std::endl;
	innerNetwork->gradientStep(alpha);
}

template<int outputDimension>
double BowRecurrentWrapper<outputDimension>::loss()
{
	return lossStats;
}

template<int outputDimension>
double BowRecurrentWrapper<outputDimension>::acc()
{
	return lastAcc;
}

template<int outputDimension>
double BowRecurrentWrapper<outputDimension>::weight()
{
	return (double)l;
}

