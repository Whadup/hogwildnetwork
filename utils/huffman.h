#include <stdio.h>
#include <string.h>
#include <fstream>
#pragma once


template<int voc_size>
class Huffman
{
public:

	typedef struct node_t {
		struct node_t *left, *right;
		int freq;
		int token;
	} *node;
	 
	//struct node_t pool[voc_size<<1] = {{0}};
	node qqq[voc_size<<1], *q = qqq - 1;
	int n_nodes = 0, qend = 1;
	int code[voc_size] = {0};
	int freq[voc_size] = {0};

	node new_node(int freq, int token, node a, node b)
	{
		node n = new node_t;
		if (freq) n->token = token, n->freq = freq;
		else {
			n->left = a, n->right = b;
			n->freq = a->freq + b->freq;
			n->token = -1;
			// printf("neue freq: %d\n", n->freq);
		}
		return n;
	}
	 
	/* priority queue */
	void qinsert(node n)
	{
		int j, i = qend++;
		while ((j = i / 2)) {
			if (q[j]->freq <= n->freq) break;
			q[i] = q[j], i = j;
		}
		q[i] = n;
		// for(int i=1;i<qend;i++)
		// 	printf("%d ", q[i]->freq);
		// printf("\n");
	}
	 
	node qremove()
	{
		int i, l;
		i=1;
		node n = q[i];
	 // 	for(int i=1;i<qend;i++)
		// 	printf("%d ", q[i]->freq);
		// printf("\n");
		if (qend < 2) return 0;
		qend--;
		
		
		while ((l = i * 2) < qend) {
			if (l + 1 < qend && q[l + 1]->freq < q[l]->freq) l++;
			if (q[qend]->freq < q[l]->freq) break;
			q[i] = q[l],i = l;
		}
		q[i] = q[qend]; 

		// printf("returne %d\n", n->freq);
		return n;
	}
	 
	/* walk the tree and put 0s and 1s */
	void build_code(node n, int c, int base)
	{
		if (n->token != -1) {
			code[n->token] = c;//|base;
			// printf("'%d': %d\n", n->token, c);
			return;
		}
		// printf("%d:%d\n", c,base);
		build_code(n->left,  c, base <<  1);
		build_code(n->right, base | c, base << 1);
	}
	void count(const int* s,int l)
	{
		for(int i=0;i<l;i++) freq[s[i]]++; //not so magic anymore
	}
	void compute()
	{
		 
		for(int i = 0; i < voc_size; i++)
			if(freq[i]) 
			{
				//printf("'%d': %d\n", i, freq[i]);
				qinsert(new_node(freq[i], i, 0, 0));
			}
		// for(int i=1;i<qend;i++)
		// 	printf("%d ", q[i]->freq);
	 	// printf("\n");
		while (qend > 2) 
			qinsert(new_node(0, 0, qremove(), qremove()));
		build_code(q[1], 0, 1);
		printf("\n");
		for(int i = 0; i < voc_size; i++)
			printf("%d:%d\n",i,code[i]);
	}

	void serialize(node n,std::ofstream& file)
	{
		file.write(reinterpret_cast<const char *>(&n->token), sizeof(int));
		file.write(reinterpret_cast<const char *>(&n->freq), sizeof(int));
		if(n->token==-1)
		{
			serialize(n->left,file);
			serialize(n->right,file);
		}

	}
};


