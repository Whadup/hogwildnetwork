#include <stdlib.h>
#include <vector>
#include "huffman.h"
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <utility>
#include <sys/time.h>
#include "loadBinarySparse.h"

// inline double getTime(){
// 	struct timeval tv;
// 	if (gettimeofday(&tv, NULL)==0)
// 		return tv.tv_sec+((double)(tv.tv_usec))/1e6;
// 	else
// 		return 0.0;
// }

// unsigned int toDigit(const char& c){
// 	if(c==49) return 1;
// 	else if(c==50) return 2;
// 	else if(c==51) return 3;
// 	else if(c==52) return 4;
// 	else if(c==53) return 5;
// 	else if(c==54) return 6;
// 	else if(c==55) return 7;
// 	else if(c==56) return 8;
// 	else if(c==57) return 9;
// 	return 0;
// }

// int toInt(const char* c, const int& n){
// 	int r = 0;
// 	int b = 1;
// 	for(int i=n-1; i>=0; --i){
// 		r += b * toDigit(c[i]);
// 		b *= 10;
// 	}
// 	return r;
// }

int* count(const std::string& in,std::vector<int>& ref,int& l){

	const char* raw = in.c_str();
	const int n  = in.size();


	int pos = 0;
	ref.clear();
	for(unsigned int i=0; i<n; ++i){

		if(raw[i] == ' ' || raw[i]=='\n'){
			if(raw[i]=='\n')
				std::cerr << "SUCCESS";
			ref.push_back(toInt(raw+pos, i-pos));
			// std::cout << app << std::endl;
			pos = i+1;

		}
	}
	if(pos<n)
	{
		// std::cerr <<  << " " << in << std::endl;
		ref.push_back(toInt(raw+pos, n-pos));
	}
	int* a = new int[ref.size()+1];
	l = ref.size()+1;
	a[ref.size()] = 0;
	// if(gender<=3)
	// 	a[1] = 0;
	// else
	// 	a[1] = 1; 
	// std::cout << a[1] << std::endl;
	for(int i=0;i<ref.size();i++)
		a[i] = ref[i];
	return a;
	
}


int main(int argc,  char **argv)
{
	
	Huffman<158909> huff;
	std::vector<int>ref;
	std::vector<int> tmp;
	std::vector<int*> dataset;
	int vocSize = 0;
	const std::string file = argv[1];
	std::ifstream docFile(file);

	int iter  = 0;
	double bytes = 0;
	double start = getTime();
	int largest = 0;

	while(!docFile.eof())
	{
		std::string line;
		getline(docFile, line);
		//std::cout << line << std::endl;
		bytes += line.size();
		int l = 0;
		int* a = parseSparse<false>(line,ref,largest,l);
		// int* a = count(line,ref,l);
		//std::cout << l << std::endl;
		huff.count(a,l);
		if(iter%10000 == 0) 
		{
				// std::cout << vocSize << std::endl;
			std::cerr << '\r' << "SPEED = " << std::fixed << std::setprecision(4) << (bytes/(1024*1024))/(getTime()-start) << " MiB/s (rows=" << iter << ") " << std::flush;
			//break;
		}
		iter++;
	}
	huff.compute();
	std::ofstream out("tree.bin",std::ofstream::binary);
	huff.serialize(huff.q[1],out);
	out.close();
	return 0;
}