#pragma once
// #ifndef _LINEARLAYER_
// #define _LINEARLAYER_
#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
#include <random>
using Eigen::Matrix;

template<int inputDimension,int hiddenDimension>
class ReLuLayer: public ComputeUnit
{
public:
	Matrix<double,hiddenDimension,inputDimension,Eigen::RowMajor>* W = 0;
	Matrix<double,hiddenDimension,inputDimension,Eigen::RowMajor>* gW = 0;
	ReLuLayer();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
	
};


template<int inputDimension,int hiddenDimension>
ReLuLayer<inputDimension,hiddenDimension>::ReLuLayer()  : ComputeUnit(1,0,1)
{
	outputs[0]=Matrix<double,hiddenDimension,1>::Zero();
	jacobians[0]=Matrix<double,inputDimension,1>::Zero();	
}

template<int inputDimension,int hiddenDimension>
void ReLuLayer<inputDimension,hiddenDimension>::init()
{
	if(W!=0) delete W;
	W = new Matrix<double,hiddenDimension,inputDimension,Eigen::RowMajor>;//::Zero();
	gW = new Matrix<double,hiddenDimension,inputDimension,Eigen::RowMajor>;//::Zero();
	gW->setZero();
	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<double> initializer(0,1);
	for(int i = 0;i<hiddenDimension;i++)
		for(int j=0;j<inputDimension;j++)
			(*W)(i,j) = initializer(gen);
}

template<int inputDimension,int hiddenDimension>
ComputeUnit* ReLuLayer<inputDimension,hiddenDimension>::duplicate()
{
	ReLuLayer<inputDimension,hiddenDimension>* l = new ReLuLayer();
	l->W = W;
	l->gW = gW;
	return (ComputeUnit*)l;
}

template<int inputDimension,int hiddenDimension>
void ReLuLayer<inputDimension,hiddenDimension>::forward()
{
	outputs[0]= ((*W) * (*inputs[0])).cwiseMax(0);
	// std::cout << "relu layer "<<this << " " << outputs[0].transpose()<<std::endl;
}

template<int inputDimension,int hiddenDimension>
void ReLuLayer<inputDimension,hiddenDimension>::backward()
{
	Matrix<double,hiddenDimension,1> jac = successorDeep->jacobians[0];
	// std::cout << jac << std::endl;
	// std::cout << *jac << std::endl;
	// std::cout << this->W.transpose().rows() << "x" << this->W.transpose().cols() << std::endl;
	// std::cout << jac->rows() << "x" << jac->cols() << std::endl;
	MatrixXd blub = this->W->transpose();
	for(int i=0;i<hiddenDimension;i++)
		if(outputs[0](i,0)<=0)
			blub.col(i)=Matrix<double,inputDimension,1>::Zero();
	// std::cout << blub << std::endl;
	for(int j=0;j<hiddenDimension;j++)
	{
		if(outputs[0](j,0)>0)
			gW->row(j)+= ((jac)(j,0)) * inputs[0]->transpose();
	}
	jacobians[0] = blub * jac;

	// std::cout << "linear layer "<<this << " " << jacobians[0].transpose()<<std::endl;
}

template<int inputDimension,int hiddenDimension>
void ReLuLayer<inputDimension,hiddenDimension>::gradientStep(double alpha)
{
	// MatrixXd* jac = &successors[0]->jacobians[0];
// 	Matrix<double,inputDimension,hiddenDimension> jacobianW;
// //	std::cout << std::endl << inputs[0]->transpose() << std::endl;
// 	for(int i=0;i<hiddenDimension;i++)
// 	{
// 		jacobianW.col(i) = *inputs[0];//->transpose();
// 	}
	//Matrix<double,inputDimension,1> delta = alpha * (*inputs[0] * (*jac).sum());//jacobianW * (*jac);
	*W-= alpha * *gW;
	gW->setZero();


	// std::cout << W << std::endl;
}

// #endif

// #endif outputs[0]=(this->W * (*inputs[0])).cwiseMax(0);