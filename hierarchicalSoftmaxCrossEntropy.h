#pragma once

#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>
#include <cmath>
#include "dataInterfaces.h"
#include "loss.h"
#include <vector>
#include <mutex> 
#include <string>         // std::mutex
// #include <hyperboard.h>
using Eigen::Matrix;
using Eigen::VectorXd;

template<int dimension,int vocabularySize>
class HierarchicalSoftmaxCrossEntropy: public ComputeUnit, public IntegerLabel, public Loss
{
public:
	typedef struct node
	{
		struct node *left, *right;
		int freq;
		int token;
		//calculate statistics here left right average
		int count;
		double avgAbs;
		MatrixXd w;
		MatrixXd gw;
	} Node;

	double l;
	Node* root;
	std::mutex* mtx;
	std::vector<Node*>* updates;
	int* codes;
	int* dumm;
	MatrixXd* deltaAvg;
	std::string names[3];


	HierarchicalSoftmaxCrossEntropy() : ComputeUnit(1,0,0), IntegerLabel()
	{		
		jacobians[0]=Matrix<double,dimension,1>::Zero();
		codes = new int[vocabularySize];
		root = randomTree(0,vocabularySize,0,1);
		// for(int i=0;i<vocabularySize;i++)
		// 	std::cout << i << " " << codes[i]<<std::endl;
	}

	HierarchicalSoftmaxCrossEntropy(const char* huffmanTree) : ComputeUnit(1,0,0), IntegerLabel()
	{
		std::ifstream file(huffmanTree,std::ifstream::binary);	
		codes = new int[vocabularySize];
		root = read(file,0,1);
		jacobians[0]=Matrix<double,dimension,1>::Zero();
	}

	void init()
	{
		updates = new std::vector<Node*>;
		mtx = new std::mutex;
		dumm = new int(0);
		deltaAvg = new MatrixXd(dimension,1);
		deltaAvg->setZero();
		// names[0] = registerLogger("Softmax Linear Part Avg","activation");
		// names[1] = "";//registerLogger("Softmax Linear Part Right","activation");
		// names[2] = registerLogger("Softmax Delta Norm","delta norm");

	}
	
	virtual void forward()
	{
		// std::cout << inputs[0]->transpose() << std::endl;
		int code = codes[y];
		Node* n = root;
		double p = 0;
		while(n->token!=y)
		{
			// if(y==0)
			// 	std::cout << "y=0 " << n << " ";
			int dec = code & 1;
			code = code >> 1;
			// std::cout << n->w.transpose() << std::endl;
			if(dec)
			{


				double d = -(n->w.transpose() * (*inputs[0]))(0,0);
				n = n->right;
				// if(d<-6 || d>6)
				// 	continue;
				double pp = 1.0 / (1.0 + exp(d));
				// std::cout << pp << " ";
				p += log2(pp);
				
			}
			else
			{

				double d = (n->w.transpose() * (*inputs[0]))(0,0);
				// std::cout << d << " ";
				n = n->left;
				// if(d<-6 || d>6)
				// 	continue;
				double pp = 1.0 / (1.0 + exp(d));
				// std::cout << pp << " ";
				p += log2(pp);
				
			}
			
		}
		l = -p;
		// std::cout << pow(2,p) << " "; 
		// std::cout << y << " " << l << " " << exp(-l) << std::endl;

		if(l!=l)
		{
			std::cerr << "NAN|INF "<< y << "->" << l << " " << inputs[0]->transpose() << std::endl;
			Node* n = root;
			double p = 0;
			code = codes[y];
			while(n->token!=y)
			{
				if(n->token!=-1)
					std::cerr << "OMG" << std::endl;
				// if(y==0)
				// 	std::cout << "y=0 " << n << " ";
				//std::cout << code << ":";
				int dec = code & 1;
				code = code >> 1;
				double d = (n->w.transpose() * (*inputs[0]))(0,0);
				// std::cout << n->w.transpose() << std::endl;

				if(dec)
				{
					n = n->right;
					if(d<-6 || d>6)
						continue;
					double pp = 1.0 / (1.0 + exp(-d));
					std::cerr << pp << " " << log2(pp) << " " << d << std::endl;
					p += log2(pp);
					
				}
				else
				{
					n = n->left;
					if(d<-6 || d>6)
						continue;
					double pp = 1.0 / (1.0 + exp(d));
					// std::cout << pp << " ";
					std::cerr << pp << " " << log2(pp) << " " << d << std::endl;
					p += log2(pp);
					
				}
				
			}
			l = -p;
			std::cout << std::endl;
			((ComputeUnit*)0)->forward();
		}
	}

	virtual void backward()
	{
		int code = codes[y];
		// std::cout << "code" << code << std::endl;
		Node* n = root;
		double p;
		jacobians[0]=Matrix<double,dimension,1>::Zero();
		
		// std::cout << jacobians[0].transpose() << std::endl;
		while(n->token!=y)
		{
			if(n->token!=-1)
					std::cerr << "OMG" << std::endl;
			// mtx->lock();
			updates->push_back(n);
			// mtx->unlock();
			int dec = code & 1;
			code = code >> 1;
			double d = (n->w.transpose() * (*inputs[0]))(0,0);
			n->count++;
			n->avgAbs+= (d<0 ? -d : d);
			// std::cout << d << " ";
			if(dec)
			{
				p = 1.0 / (1.0 + exp(-d))-1;
				if(d<-6 || d>6)
				{
					n = n->right;
					// std::cout << d << " is kacke" << std::endl;
					continue;
				}
				jacobians[0] += p * n->w;
				n->gw += p * *inputs[0];
				n = n->right;
				//n->w -= alpha * p * *inputs[0];
				
			}
			else
			{
				p = 1.0 / (1.0 + exp(-d));
				
				if(d<-6 || d>6)
				{
					// std::cout << d << " is kacke" << std::endl;
					n = n->left;
					continue;
				}
				jacobians[0] += p * n->w;
				n->gw += p * *inputs[0];
				n = n->left;
				//n->w -= alpha * p * *inputs[0];
				
			}
			// std::cout << d << " is gut" << std::endl;
		}

		// std::cout << jacobians[0].transpose() << std::endl;
	}
	
	void recurse(Node* root,double& avg, double& avgToTwo,int& c)
	{
		c++;
		if(root->left == 0)
		{
			if(root->count>0)
			{
				double d = root->avgAbs / root->count;
				root->count=0;
				root->avgAbs = 0;
				avg+=d;
				avgToTwo+= d*d;
			}
		}
		else
		{
			if(root->count>0)
			{
				double d = root->avgAbs / root->count;
				root->count=0;
				root->avgAbs = 0;
				avg+=d;
				avgToTwo+= d*d;
			}
			recurse(root->left,avg,avgToTwo,c);
			recurse(root->right,avg,avgToTwo,c);
		}
	}

	virtual void gradientStep(double alpha)
	{
		// std::cout << "GRADIENT IM hSM "<< alpha << std::endl;
		// mtx->lock();
		*deltaAvg+=jacobians[0];
		// double nd = jacobians[0].norm();
		// *normAvg+=nd;
		// (*dumm)++;
		// if(*dumm >= 1000)
		// {
		// 	double avg = 0.0;
		// 	double avgToTwo = 0.0;
		// 	int n = 0;
		// 	recurse(root,avg,avgToTwo,n);
		// 	std::cout << avg/n << std::endl;
		// 	logValue(names[0],1,avg/n);
		// 	*deltaAvg/=(*dumm);
		// 	std::cout << deltaAvg->norm() << "ADFAS" << std::endl;

		// 	logValue(names[2],1,deltaAvg->norm());
		// 	deltaAvg->setZero();
		// 	*dumm = 0; 
		// }
		for(int i=0;i<updates->size();i++)
		{
			Node* n = (*updates)[i];
			n->w -= alpha * n->gw;
			// std::cout << n->gw.transpose() << std::endl;
			n->gw.setZero();
		}
		updates->clear();
		// mtx->unlock();
	}

	void deleteTree(Node* root)
	{
		if(root->left == 0)
			delete root;
		else
		{
			deleteTree(root->left);
			deleteTree(root->right);
			delete root;
		}
	}


	virtual ComputeUnit* duplicate()
	{
		#warning "screwed up duplicate() in hierarchicalSoftmaxCrossEntropy.h"
		HierarchicalSoftmaxCrossEntropy* h = new HierarchicalSoftmaxCrossEntropy();
		delete[] h->codes;
		deleteTree(h->root);
		h->root = root;
		h->codes = codes;
		h->updates = new std::vector<Node*>();
		h->mtx = mtx;
		h->deltaAvg = deltaAvg;
		h->names[0] = names[0];
		h->names[1] = names[1];
		h->names[2] = names[2];
		h->dumm = dumm;
		return (ComputeUnit*) h;
	}
	
	double loss()
	{
		return l;
	}

	double acc()
	{
		return -1;
	}

	Node* randomTree(int start, int end,int code, int base)
	{
		Node* n = new Node;
		if(start+1>=end)
		{
			codes[start]=code;
			n->token = start;
			n->freq = 1;
			n->left = n->right = 0;
		}
		else
		{
			n->freq = 1;
			n->token = -1;
			n->left = randomTree(start, start + (end-start)/2,code,base<<1);
			n->right = randomTree(start + (end-start)/2,end,code|base,base<<1);
			n->w = MatrixXd::Zero(dimension,1);
			n->gw = MatrixXd::Zero(dimension,1);
		}
		return n;
	}

	Node* read(std::ifstream& in,int code, int base)
	{
		static std::random_device rd;
		static std::mt19937 gen(rd());
		static std::normal_distribution<double> initializer(0,0.001);

		Node* n = new Node;

		char buf[8];
		in.read(buf,sizeof(buf));
		// for(int i=0;i<8;i++)
		// 	printf("%#010x\n", buf[i]);  
		n->token = *((int*)buf);
		n->freq = *((int*)&buf[4]);
		// if(n->token==0) 
		std::cout << n->token << " " << n->freq << std::endl;
		if(n->token==-1)
		{
			n->left  = read(in, code,      base<<1);
			n->right = read(in, code|base, base<<1);
			n->w = MatrixXd::Zero(dimension,1);
			n->gw = MatrixXd::Zero(dimension,1);

			// for(int i=0;i<dimension;i++)
			// {
			// 	n->w(i,0) = initializer(gen);
			// }
		}
		else
		{
			codes[n->token] = code;
			n->left = n->right = 0;
			// std::cout << n->token << ":" << code << std::endl;
		}
		return n;
	}
};
