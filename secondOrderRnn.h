
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<double> initializer(0,0.1);

#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;

#include "computeUnit.h" 
template <int inputDimension, int outputDimension>
class SecondOrderRnn: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,outputDimension,1> out;
	Matrix<double,outputDimension,1> st;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> s;

	MatrixXd** U = 0;
	MatrixXd** gU = 0;

	SecondOrderRnn();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension, int outputDimension>
SecondOrderRnn<inputDimension, outputDimension>::SecondOrderRnn() : ComputeUnit(1,1,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Constant(1);
	outputs[1] = Matrix<double,outputDimension,1>::Constant(1);
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,outputDimension,1>::Zero();
}

template<int inputDimension, int outputDimension>
void SecondOrderRnn<inputDimension, outputDimension>::init() 
{
	U = new MatrixXd*[outputDimension];
	gU = new MatrixXd*[outputDimension];
	for(int i=0;i<outputDimension;i++)
	{	
		U[i] = new MatrixXd(outputDimension,inputDimension);
		gaussianInitialization(U[i]);
		U[i]->diagonal(i).array() = 1;
		U[i]->diagonal(-i).array() = 1;
		
		gU[i] = new MatrixXd(outputDimension,inputDimension);
		zeroInitialization(gU[i]);
	}


}
template<int inputDimension, int outputDimension>
ComputeUnit* SecondOrderRnn<inputDimension, outputDimension>::duplicate() 
{
	SecondOrderRnn<inputDimension, outputDimension>* l = new SecondOrderRnn<inputDimension, outputDimension>();
	l->U = U;
	l->gU = gU;
	return (ComputeUnit*) l;
}
template<int inputDimension, int outputDimension>
void SecondOrderRnn<inputDimension, outputDimension>::forward()
{
	x = *inputs[0];
	s = *inputs[1];
	for(int i=0;i<outputDimension;i++)
		st(i,0) = (s.transpose() * *(U[i]) * x)(0,0);
	st = st.array().tanh().matrix();
#ifdef DEBUG
	std::cout << "f " << st.transpose() << std::endl;
#endif
	outputs[1] = st;
	outputs[0] = st;
}
template<int inputDimension, int outputDimension>
void SecondOrderRnn<inputDimension, outputDimension>::backward()
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lst = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ls = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];};
	if(successorTime!=0) {Lst += this->successorTime->jacobians[1];};
	Lst += Lout;
	
	for(int i=0;i<outputDimension;i++)
	{
		Lx+=Lst(i,0) * (1-st(i,0)) * (1-st(i,0)) * U[i]->transpose() * s;
		Ls+=Lst(i,0) * (1-st(i,0)) * (1-st(i,0)) * (*U[i]) * x;
		*(gU[i]) += Lst(i,0) * (1-st(i,0)) * (1-st(i,0)) * s * x.transpose();
	}
	if(Ls.norm()>1)
		Ls/=Ls.norm();
	if(Lx.norm()>1)
		Lx/=Lx.norm();
	jacobians[1] = Ls;
	jacobians[0] = Lx;
	
#ifdef DEBUG
	std::cout << "b " << Ls.transpose() << std::endl;
#endif
	//*gUstx-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((st.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -st).array()).matrix()) * Lst * x.transpose());	
}

template<int inputDimension, int outputDimension>
void SecondOrderRnn<inputDimension, outputDimension>::gradientStep(double alpha)
{
	//*mUsts = 0.9 * (*mUsts) - alpha * (*gUsts);
	//*mbst  = 0.9 * (*mbst) - alpha * (*gbst);

	// *Usts += *mUsts;
	// *bst += *mbst;
	for(int i=0;i<outputDimension;i++)
	{	
		*(U[i]) -= alpha * *(gU[i]);
		gU[i]->setZero();

	}

}
