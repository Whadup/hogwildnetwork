
#pragma once
#include <iostream> 
#include <Eigen/Dense>
#include <cmath>
using Eigen::Matrix;
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<double> initializer(0,0.1);

#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;

#include "computeUnit.h" 
template <int inputDimension, int outputDimension, int redundancy>
class AssociativeMemory: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,outputDimension,1> rhat;
	Matrix<double,(outputDimension>>1),1> i;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,outputDimension,1> rbound;
	Matrix<double,outputDimension,1> oo;
	Matrix<double,outputDimension,1> tmp2;
	Matrix<double,outputDimension,1> vbound;
	Matrix<double,outputDimension,1> ff;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> c;
	Matrix<double,outputDimension,1> tmp1;
	Matrix<double,outputDimension,1> m;
	Matrix<double,(outputDimension>>1),1> o;
	Matrix<double,outputDimension,1> ii;
	Matrix<double,outputDimension,1> out;
	Matrix<double,outputDimension,1> ht;
	Matrix<double,outputDimension,1> v;
	Matrix<double,outputDimension,1> b;
	Matrix<double,outputDimension,1> w;
	Matrix<double,outputDimension,1> vhat;
	Matrix<double,(outputDimension>>1),1> f;
	Matrix<double,outputDimension,1> ct;
	Matrix<double,outputDimension,1> wbound;
	Matrix<double,outputDimension,1> a;
	Matrix<double,outputDimension,1> what;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> h;
	Matrix<double,outputDimension,1> tmp3;
	Matrix<double,outputDimension,1> r;
	Matrix<double,outputDimension,outputDimension> complex1 = Matrix<double,outputDimension,outputDimension>::Zero();
	Matrix<double,outputDimension,outputDimension> complex2 = Matrix<double,outputDimension,outputDimension>::Zero();
	Matrix<double,(outputDimension>>1),outputDimension>* Ufh = 0;
	Matrix<double,outputDimension,inputDimension>* Urhatx = 0;
	Matrix<double,(outputDimension>>1),outputDimension>* Uov = 0;
	Matrix<double,outputDimension,inputDimension>* Uwhatx = 0;
	Matrix<double,(outputDimension>>1),1>* bf = 0;
	Matrix<double,(outputDimension>>1),inputDimension>* Uox = 0;
	Matrix<double,outputDimension,outputDimension>* Ucttmp2 = 0;
	Matrix<double,outputDimension,outputDimension>* Uwhatm = 0;
	Matrix<double,(outputDimension>>1),outputDimension>* Uim = 0;
	Matrix<double,(outputDimension>>1),outputDimension>* Uih = 0;
	Matrix<double,outputDimension,outputDimension>* Urhath = 0;
	Matrix<double,outputDimension,outputDimension>* Uvhatm = 0;
	Matrix<double,outputDimension,inputDimension>* Uvhatx = 0;
	Matrix<double,outputDimension,1>* bvhat = 0;
	Matrix<double,outputDimension,outputDimension>* Ucttmp1 = 0;
	Matrix<double,(outputDimension>>1),1>* bi = 0;
	Matrix<double,outputDimension,1>* bwhat = 0;
	Matrix<double,(outputDimension>>1),inputDimension>* Uix = 0;
	Matrix<double,(outputDimension>>1),outputDimension>* Uoh = 0;
	Matrix<double,(outputDimension>>1),1>* bo = 0;
	Matrix<double,outputDimension,1>* bct = 0;
	Matrix<double,outputDimension,outputDimension>* Uctc = 0;
	Matrix<double,outputDimension,outputDimension>* Uwhath = 0;
	Matrix<double,outputDimension,outputDimension>* Uvhath = 0;
	Matrix<double,outputDimension,1>* brhat = 0;
	Matrix<double,(outputDimension>>1),inputDimension>* Ufx = 0;
	Matrix<double,(outputDimension>>1),outputDimension>* Ufm = 0;
	AssociativeMemory();
	virtual void init();
	virtual void forward();
	virtual void backwardGradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension, int outputDimension, int redundancy>
AssociativeMemory<inputDimension, outputDimension, redundancy>::AssociativeMemory() : ComputeUnit(1,2,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Zero();
	outputs[1] = Matrix<double,outputDimension,1>::Zero();
	outputs[2] = Matrix<double,outputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,outputDimension,1>::Zero();
	jacobians[2] = Matrix<double,outputDimension,1>::Zero();
	complex1.topLeftCorner(outputDimension/2,outputDimension/2).setIdentity();
	complex1.bottomLeftCorner(outputDimension/2,outputDimension/2).setIdentity();
	complex2.bottomRightCorner(outputDimension/2,outputDimension/2).setIdentity();
	complex2.topRightCorner(outputDimension/2,outputDimension/2).setIdentity();
	complex2.topRightCorner(outputDimension/2,outputDimension/2) = -complex2.topRightCorner(outputDimension/2,outputDimension/2);
}

template<int inputDimension, int outputDimension, int redundancy>
void AssociativeMemory<inputDimension, outputDimension, redundancy>::init() 
{
	Ufh = new Matrix<double,(outputDimension>>1),outputDimension>();
	gaussianInitialization(Ufh);
	Urhatx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Urhatx);
	Uov = new Matrix<double,(outputDimension>>1),outputDimension>();
	gaussianInitialization(Uov);
	Uwhatx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uwhatx);
	bf = new Matrix<double,(outputDimension>>1),1>();
	zeroInitialization(bf);
	Uox = new Matrix<double,(outputDimension>>1),inputDimension>();
	gaussianInitialization(Uox);
	Ucttmp2 = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Ucttmp2);
	Uwhatm = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uwhatm);
	Uim = new Matrix<double,(outputDimension>>1),outputDimension>();
	gaussianInitialization(Uim);
	Uih = new Matrix<double,(outputDimension>>1),outputDimension>();
	gaussianInitialization(Uih);
	Urhath = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Urhath);
	Uvhatm = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uvhatm);
	Uvhatx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uvhatx);
	bvhat = new Matrix<double,outputDimension,1>();
	zeroInitialization(bvhat);
	Ucttmp1 = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Ucttmp1);
	bi = new Matrix<double,(outputDimension>>1),1>();
	zeroInitialization(bi);
	bwhat = new Matrix<double,outputDimension,1>();
	zeroInitialization(bwhat);
	Uix = new Matrix<double,(outputDimension>>1),inputDimension>();
	gaussianInitialization(Uix);
	Uoh = new Matrix<double,(outputDimension>>1),outputDimension>();
	gaussianInitialization(Uoh);
	bo = new Matrix<double,(outputDimension>>1),1>();
	zeroInitialization(bo);
	bct = new Matrix<double,outputDimension,1>();
	zeroInitialization(bct);
	Uctc = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uctc);
	Uwhath = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uwhath);
	Uvhath = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uvhath);
	brhat = new Matrix<double,outputDimension,1>();
	zeroInitialization(brhat);
	Ufx = new Matrix<double,(outputDimension>>1),inputDimension>();
	gaussianInitialization(Ufx);
	Ufm = new Matrix<double,(outputDimension>>1),outputDimension>();
	gaussianInitialization(Ufm);
}
template<int inputDimension, int outputDimension, int redundancy>
ComputeUnit* AssociativeMemory<inputDimension, outputDimension, redundancy>::duplicate() 
{
	AssociativeMemory<inputDimension, outputDimension, redundancy>* l = new AssociativeMemory<inputDimension, outputDimension, redundancy>();
	l->Ufh = Ufh;
	l->Urhatx = Urhatx;
	l->Uov = Uov;
	l->Uwhatx = Uwhatx;
	l->bf = bf;
	l->Uox = Uox;
	l->Ucttmp2 = Ucttmp2;
	l->Uwhatm = Uwhatm;
	l->Uim = Uim;
	l->Uih = Uih;
	l->Urhath = Urhath;
	l->Uvhatm = Uvhatm;
	l->Uvhatx = Uvhatx;
	l->bvhat = bvhat;
	l->Ucttmp1 = Ucttmp1;
	l->bi = bi;
	l->bwhat = bwhat;
	l->Uix = Uix;
	l->Uoh = Uoh;
	l->bo = bo;
	l->bct = bct;
	l->Uctc = Uctc;
	l->Uwhath = Uwhath;
	l->Uvhath = Uvhath;
	l->brhat = brhat;
	l->Ufx = Ufx;
	l->Ufm = Ufm;
	return (ComputeUnit*) l;
}
template<int inputDimension, int outputDimension, int redundancy>
void AssociativeMemory<inputDimension, outputDimension, redundancy>::forward()
{
	x = *inputs[0];
	c = *inputs[1];
	h = *inputs[2];
	rhat = (((*Urhatx) * x) + ((*Urhath) * h) + (*brhat));
	{
		Matrix<double,outputDimension,1> tmpd;
		for(int i=0;i<outputDimension>>1;i++) 
			tmpd[i] = tmpd[i+(outputDimension>>1)] = rhat(i,0)*rhat(i,0) + rhat(i+(outputDimension>>1),0)*rhat(i+(outputDimension>>1),0);
		tmpd = tmpd.array().sqrt().max(1).inverse().matrix();
		rbound=tmpd;
		// std::cout << rbound.transpose() << std::endl;
	}
	r = (rhat.array() * rbound.array()).matrix();
	m = (((complex1 * r).array() * c.array()).matrix() + (( -complex2 * r).array() * c.array()).matrix());
	f = (1.0 + (-(((*Ufx) * x) + ((*Ufh) * h) + ((*Ufm) * m) + (*bf))).array().exp()).inverse().matrix();;
	what = (((*Uwhatx) * x) + ((*Uwhath) * h) + ((*Uwhatm) * m) + (*bwhat));
	i = (1.0 + (-(((*Uix) * x) + ((*Uih) * h) + ((*Uim) * m) + (*bi))).array().exp()).inverse().matrix();;
	{
		Matrix<double,outputDimension,1> tmpd;
		for(int i=0;i<outputDimension>>1;i++) 
			tmpd[i] = tmpd[i+(outputDimension>>1)] = what(i,0)*what(i,0) + what(i+(outputDimension>>1),0)*what(i+(outputDimension>>1),0);
		tmpd = tmpd.array().sqrt().max(1).inverse().matrix();
		wbound=tmpd;
		// std::cout << wbound.transpose() << std::endl;
	}
	{
		Matrix<double,(outputDimension>>1)+(outputDimension>>1),1> tmp;
		tmp.block(0,0,(outputDimension>>1),1) = f;
		tmp.block((outputDimension>>1),0,(outputDimension>>1),1) = f;
		ff=tmp;
	}
	vhat = (((*Uvhatx) * x) + ((*Uvhath) * h) + ((*Uvhatm) * m) + (*bvhat));
	{
		Matrix<double,(outputDimension>>1)+(outputDimension>>1),1> tmp;
		tmp.block(0,0,(outputDimension>>1),1) = i;
		tmp.block((outputDimension>>1),0,(outputDimension>>1),1) = i;
		ii=tmp;
	}
	{
		Matrix<double,outputDimension,1> tmpd;
		for(int i=0;i<outputDimension>>1;i++) 
			tmpd[i] = tmpd[i+(outputDimension>>1)] = vhat(i,0)*vhat(i,0) + vhat(i+(outputDimension>>1),0)*vhat(i+(outputDimension>>1),0);
		tmpd = tmpd.array().sqrt().max(1).inverse().matrix();
		vbound=tmpd;
		// std::cout << tmpd.transpose() << std::endl;
	}
	v = (vhat.array() * vbound.array()).matrix();
	b = (((complex1 * r).array() * m.array()).matrix() + ((complex2 * r).array() * m.array()).matrix());
	w = (what.array() * wbound.array()).matrix();
	a = (((complex1 * w).array() * v.array()).matrix() + ((complex2 * w).array() * v.array()).matrix());
	tmp3 = (v).array().tanh().matrix();
	tmp2 = (ff.array() * b.array()).matrix();
	tmp1 = (ii.array() * a.array()).matrix();
	o = (1.0 + (-(((*Uox) * x) + ((*Uoh) * h) + ((*Uov) * v) + (*bo))).array().exp()).inverse().matrix();;
	ct = c + tmp1;// - tmp2; //(((*Uctc) * c) + ((*Ucttmp1) * tmp1) + ((*Ucttmp2) * tmp2) + (*bct));
	outputs[1] = ct;
	{
		Matrix<double,(outputDimension>>1)+(outputDimension>>1),1> tmp;
		tmp.block(0,0,(outputDimension>>1),1) = o;
		tmp.block((outputDimension>>1),0,(outputDimension>>1),1) = o;
		oo=tmp;
	}
	ht = (tmp3.array() * oo.array()).matrix();
	for(int i=0;i<outputDimension;i++)
	{
		if(ht(i,0)!=ht(i,0) || ct(i,0)!=ct(i,0))
		{
			std::cout << "x" << "\t" << x.transpose() << std::endl;
			std::cout << "h" << "\t" << h.transpose() << std::endl;
			std::cout << "c" << "\t" << c.transpose() << std::endl;
			std::cout << "r" << "\t" << r.transpose() << " lin " << rhat.transpose() << std::endl;
			std::cout << "m" << "\t" << m.transpose() << std::endl;
			std::cout << "v" << "\t" << v.transpose() << " lin " << vhat.transpose() << std::endl;
			std::cout << "w" << "\t" << w.transpose() << " lin " << what.transpose() << std::endl;
			std::cout << "ff" << "\t" << ff.transpose() << std::endl;
			std::cout << "ii" << "\t" << ii.transpose() << std::endl;
			std::cout << "a" << "\t" << a.transpose() << std::endl;
			std::cout << "b" << "\t" << b.transpose() << std::endl;
			std::cout << "oo" << "\t" << oo.transpose() << std::endl;
			std::cout << "ct" << "\t" << ct.transpose() << std::endl;
			std::cout << "ht" << "\t" << ht.transpose() << std::endl;
			((LinearLayer<5,5>*)0)->forward();
			break;
		}
	}
	outputs[2] = ht;
	outputs[0] = ht;
//	std::cout << ht.transpose() << std::endl;
}
template<int inputDimension, int outputDimension, int redundancy>
void AssociativeMemory<inputDimension, outputDimension, redundancy>::backwardGradientStep(double alpha)
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lht = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Loo = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lct = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,(outputDimension>>1),1> Lo = Matrix<double,(outputDimension>>1),1>::Zero();
	Matrix<double,outputDimension,1> Ltmp1 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp2 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp3 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> La = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lw = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lb = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lv = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lvbound = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lii = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lvhat = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lff = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lwbound = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,(outputDimension>>1),1> Li = Matrix<double,(outputDimension>>1),1>::Zero();
	Matrix<double,outputDimension,1> Lwhat = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,(outputDimension>>1),1> Lf = Matrix<double,(outputDimension>>1),1>::Zero();
	Matrix<double,outputDimension,1> Lm = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lr = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lrbound = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lrhat = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lh = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lc = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];
// 	std::cout << "back" << std::endl;
// std::cout << Lout.transpose()<< std::endl;
}
	if(successorTime!=0) {Lct += this->successorTime->jacobians[1];}
	if(successorTime!=0) {Lht += this->successorTime->jacobians[2];}

	Lht += Lout;
//	std::cout << "b" << Lht.transpose() << std::endl;
	Loo += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp3) * Lht);
	Lo += Loo.block(0,0,(outputDimension>>1),1);
	Lo += Loo.block((outputDimension>>1),0,(outputDimension>>1),1);
	Ltmp1 += Lct; //((*Ucttmp1).transpose() * Lct);
	//Ltmp2 -= Lct; //((*Ucttmp2).transpose() * Lct);
	Ltmp3 += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(oo) * Lht);
	La += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(ii) * Ltmp1);
	Lw += ((complex1.transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(v) * La) + (complex2.transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(v) * La));
	Lb += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(ff) * Ltmp2);
	Lv += ((*Uov).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((o.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -o).array()).matrix()) * Lo);
	Lv += ((Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((complex1 * w)) * La) + (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((complex2 * w)) * La));
	Lv += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -tmp3.array().cwiseAbs2().matrix())) * Ltmp3);
	Lvbound += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(vhat) * Lv);
	Lii += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(a) * Ltmp1);
	{
		//vhat
		Matrix<double,outputDimension,outputDimension> s = Matrix<double,outputDimension,outputDimension>::Zero();
		int d = outputDimension/2;
		for(int i=0;i<d;i++)
		{
			float q = vhat(i,0)*vhat(i,0) + vhat(i+d,0)*vhat(i+d,0);
			if(q>1)
			{
				float p = std::pow(vhat(i,0)*vhat(i)+vhat(i+d,0)*vhat(i+d,0),1.5);
				s(i,i) = -vhat(i,0) / p;
				s(i+d,i) = -vhat(i+d,0) / p;
			}
			s(i,i+d) = s(i,i);
			s(i+d,i+d) = s(i+d,i);
		}
		// std::cout << "SMatrix" << std::endl;
		// std::cout << s << std::endl;
		Lvhat += (s * Lvbound);

	}
	Lvhat += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(vbound) * Lv);
	Lff += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(b) * Ltmp2);
	Lwbound += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(what) * Lw);
	Li += Lii.block(0,0,(outputDimension>>1),1);
	Li += Lii.block((outputDimension>>1),0,(outputDimension>>1),1);
	// Lwhat += (BOUNDINGVECTOR * Lwbound);
	{
		//what
		Matrix<double,outputDimension,outputDimension> s = Matrix<double,outputDimension,outputDimension>::Zero();
		int d = outputDimension/2;
		for(int i=0;i<d;i++)
		{
			float q = what(i,0)*what(i,0) + what(i+d,0) * what(i+d,0);
			if(q>1)
			{
				float p = std::pow(what(i,0)*what(i)+what(i+d,0)*what(i+d,0),1.5);
				s(i,i) = -what(i,0) / p;
				s(i+d,i) = -what(i+d,0) / p;
			}
			s(i,i+d) = s(i,i);
			s(i+d,i+d) = s(i+d,i);
		}
		Lwhat += (s * Lwbound);
	}
	Lwhat += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(wbound) * Lw);
	Lf += Lff.block(0,0,(outputDimension>>1),1);
	Lf += Lff.block((outputDimension>>1),0,(outputDimension>>1),1);
	Lm += ((*Uim).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((i.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lm += ((*Uvhatm).transpose() * Lvhat);
	Lm += ((Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((complex1 * r)) * Lb) + (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((complex2 * r)) * Lb));
	Lm += ((*Ufm).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((f.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -f).array()).matrix()) * Lf);
	Lm += ((*Uwhatm).transpose() * Lwhat);
	Lr += ((complex1.transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(m) * Lb) + (complex2.transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(m) * Lb));
	Lr += ((complex1.transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(c) * Lm) + ( -complex2.transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(c) * Lm));
	Lrbound += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(rhat) * Lr);
	{
		//rhat
		Matrix<double,outputDimension,outputDimension> s = Matrix<double,outputDimension,outputDimension>::Zero();
		int d = outputDimension/2;
		for(int i=0;i<d;i++)
		{
			float q = rhat(i,0)*rhat(i,0) + rhat(i+d,0) * rhat(i+d,0);
			if(q>1)
			{
				float p = std::pow(rhat(i,0)*rhat(i)+rhat(i+d,0)*rhat(i+d,0),1.5);
				s(i,i) = -rhat(i,0) / p;
				s(i+d,i) = -rhat(i+d,0) / p;
			}
			s(i,i+d) = s(i,i);
			s(i+d,i+d) = s(i+d,i);
		}
		Lrhat += (s * Lrbound);

	}
	Lrhat += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(rbound) * Lr);
	Lh += ((*Urhath).transpose() * Lrhat);
	Lh += ((*Uih).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((i.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lh += ((*Uvhath).transpose() * Lvhat);
	Lh += ((*Uoh).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((o.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -o).array()).matrix()) * Lo);
	Lh += ((*Ufh).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((f.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -f).array()).matrix()) * Lf);
	Lh += ((*Uwhath).transpose() * Lwhat);
	jacobians[2] = Lh;
	Lc += ((Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((complex1 * r)) * Lm) + (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(( -complex2 * r)) * Lm));
	Lc += Lct; //((*Uctc).transpose() * Lct);
	jacobians[1] = Lc;
	Lx += ((*Urhatx).transpose() * Lrhat);
	Lx += ((*Uix).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((i.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lx += ((*Uvhatx).transpose() * Lvhat);
	Lx += ((*Uox).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((o.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -o).array()).matrix()) * Lo);
	Lx += ((*Ufx).transpose() * Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((f.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -f).array()).matrix()) * Lf);
	Lx += ((*Uwhatx).transpose() * Lwhat);
	jacobians[0] = Lx;

	for(int i=0;i<outputDimension;i++)
	{
		if(Lh(i,0)!=Lh(i,0) || Lc(i,0)!=Lc(i,0))
		{
			std::cout << "FEHLER IM BACKWARD PASS" << std::endl;
			std::cout << "x" << "\t" << x.transpose() << std::endl;
			std::cout << "h" << "\t" << h.transpose() << std::endl;
			std::cout << "c" << "\t" << c.transpose() << std::endl;
			std::cout << "r" << "\t" << r.transpose() << " lin " << rhat.transpose() << std::endl;
			std::cout << "m" << "\t" << m.transpose() << std::endl;
			std::cout << "v" << "\t" << v.transpose() << std::endl;
			std::cout << "w" << "\t" << w.transpose() << std::endl;
			std::cout << "ff" << "\t" << ff.transpose() << std::endl;
			std::cout << "ii" << "\t" << ii.transpose() << std::endl;
			std::cout << "a" << "\t" << a.transpose() << std::endl;
			std::cout << "b" << "\t" << b.transpose() << std::endl;
			std::cout << "oo" << "\t" << oo.transpose() << std::endl;
			std::cout << "ct" << "\t" << ct.transpose() << std::endl;
			std::cout << "ht" << "\t" << ht.transpose() << std::endl;
			((LinearLayer<5,5>*)0)->forward();
			break;
		}
	}

	double d = jacobians[1].norm();
	if(d>1)
	{
		// std::cout << "C";// << std::endl;
		jacobians[1]*=1/d;
	}
	d = jacobians[0].norm();
	if(d>1)
	{
		// std::cout << "C";// << std::endl;
		jacobians[0]*=1/d;
		
	}
	d = jacobians[2].norm();
	if(d>1)
	{
		// std::cout << "C";// << std::endl;
		jacobians[2]*=1/d;
		
	}

	*Urhatx-= alpha * (Lrhat * x.transpose());
	*Urhath-= alpha * (Lrhat * h.transpose());
	*brhat-= alpha * Lrhat;
	*Uix-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((i.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -i).array()).matrix()) * Li * x.transpose());
	*Uih-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((i.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -i).array()).matrix()) * Li * h.transpose());
	*Uim-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((i.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -i).array()).matrix()) * Li * m.transpose());
	*bi-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((i.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -i).array()).matrix()) * Li);
	*Uvhatx-= alpha * (Lvhat * x.transpose());
	*Uvhath-= alpha * (Lvhat * h.transpose());
	*Uvhatm-= alpha * (Lvhat * m.transpose());
	*bvhat-= alpha * Lvhat;
	*Uox-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((o.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -o).array()).matrix()) * Lo * x.transpose());
	*Uoh-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((o.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -o).array()).matrix()) * Lo * h.transpose());
	*Uov-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((o.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -o).array()).matrix()) * Lo * v.transpose());
	*bo-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((o.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -o).array()).matrix()) * Lo);
	*Ufx-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((f.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -f).array()).matrix()) * Lf * x.transpose());
	*Ufh-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((f.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -f).array()).matrix()) * Lf * h.transpose());
	*Ufm-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((f.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -f).array()).matrix()) * Lf * m.transpose());
	*bf-= alpha * (Eigen::DiagonalMatrix<double,(outputDimension>>1),(outputDimension>>1)>((f.array() * (Matrix<double,(outputDimension>>1),1>::Constant(1) +  -f).array()).matrix()) * Lf);
	*Uctc-= alpha * (Lct * c.transpose());
	*Ucttmp1-= alpha * (Lct * tmp1.transpose());
	*Ucttmp2-= alpha * (Lct * tmp2.transpose());
	*bct-= alpha * Lct;
	*Uwhatx-= alpha * (Lwhat * x.transpose());
	*Uwhath-= alpha * (Lwhat * h.transpose());
	*Uwhatm-= alpha * (Lwhat * m.transpose());
	*bwhat-= alpha * Lwhat;
	
}
