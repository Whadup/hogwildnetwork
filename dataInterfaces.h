#pragma once

class IntegerInput
{
public:
	int input;
};

class IntegerLabel
{
public:
	int y;
};

class SequenceInput
{
public:
	int* input;
	int l;
};

class SequenceLabel
{
public:
	int* y;
	int ll;
};

class BowSequenceLabel
{
public:
	int** y;
	int* lll;
	int ll;
};


class Example
{
};


class SeqExample : public Example, public IntegerLabel, public SequenceInput
{

};

class SeqSeqExample : public Example, public SequenceLabel, public SequenceInput
{

};

class SeqBowSeqExample : public Example, public BowSequenceLabel, public SequenceInput
{

};
class IntIntExample: public Example,public IntegerInput, public IntegerLabel
{

};
class WeightedIntIntExample: public Example,public IntegerInput, public IntegerLabel
{
public:
	double w=1.0;
};
class IntBowExample: public Example,public IntegerInput, public SequenceLabel
{

};
