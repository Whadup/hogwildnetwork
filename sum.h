#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;
#include "computeUnit.h" 

template <int inputDimension>
class Sum: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,inputDimension,1> out;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> y;
	Matrix<double,inputDimension,1> z;
	Sum();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension>
Sum<inputDimension>::Sum() : ComputeUnit(2,0,1) 
{
	outputs[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,inputDimension,1>::Zero();
}

template<int inputDimension>
void Sum<inputDimension>::init() 
{
}
template<int inputDimension>
ComputeUnit* Sum<inputDimension>::duplicate() 
{
	Sum<inputDimension>* l = new Sum<inputDimension>();
	return (ComputeUnit*) l;
}
template<int inputDimension>
void Sum<inputDimension>::forward()
{
	x = *inputs[0];
	y = *inputs[1];
	z = (x + y);
	outputs[0] = z;
}
template<int inputDimension>
void Sum<inputDimension>::backward()
{
	Matrix<double,inputDimension,1> Lout = Matrix<double,inputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lz = Matrix<double,inputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Ly = Matrix<double,inputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];}
	Lz += Lout;
	Ly += Lz;
	jacobians[1] = Ly;
	Lx += Lz;
	jacobians[0] = Lx;
}
template<int inputDimension>
void Sum<inputDimension>::gradientStep(double alpha)
{
}
