// <unit name="LinearLayer2">
// 	<parameter name="inputDimension" type="int"/>
// 	<parameter name="inputDimension2" type="int"/>
// 	<parameter name="outputDimension" type="int"/>
// 	<input id="x" dimension="inputDimension" />
// 	<input id="x2" dimension="inputDimension2" />
// 	<sum id="y" dimension="outputDimension">
// 		<in key="x" />
// 		<in key="x2" />
// 	</sum>
// 	<output id="out" dimension="outputDimension">
// 		<in key="y" />
// 	</output>
// </unit>
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;
#ifndef NORMAL
#define NORMAL
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<> d(0,1/10.0);
std::normal_distribution<> initializer(0,0.1);
#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;
#endif 
#include "computeUnit.h" 
template <int inputDimension, int inputDimension2, int outputDimension>
class LinearLayer2: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,outputDimension,1> y;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x2;
	Matrix<double,outputDimension,1> out;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,outputDimension,1>* by = 0;
	Matrix<double,outputDimension,1>* gby = 0;
	Matrix<double,outputDimension,inputDimension>* Uyx = 0;
	Matrix<double,outputDimension,inputDimension>* gUyx = 0;
	Matrix<double,outputDimension,inputDimension2>* Uyx2 = 0;
	Matrix<double,outputDimension,inputDimension2>* gUyx2 = 0;
	LinearLayer2();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension, int inputDimension2, int outputDimension>
LinearLayer2<inputDimension, inputDimension2, outputDimension>::LinearLayer2() : ComputeUnit(2,0,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,inputDimension2,1>::Zero();
}

template<int inputDimension, int inputDimension2, int outputDimension>
void LinearLayer2<inputDimension, inputDimension2, outputDimension>::init() 
{
	by = new Matrix<double,outputDimension,1>();
	zeroInitialization(by);
	gby = new Matrix<double,outputDimension,1>();
	gby->setZero();
	Uyx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uyx);
	gUyx = new Matrix<double,outputDimension,inputDimension>();
	gUyx->setZero();
	Uyx2 = new Matrix<double,outputDimension,inputDimension2>();
	gaussianInitialization(Uyx2);
	gUyx2 = new Matrix<double,outputDimension,inputDimension2>();
	gUyx2->setZero();
}
template<int inputDimension, int inputDimension2, int outputDimension>
ComputeUnit* LinearLayer2<inputDimension, inputDimension2, outputDimension>::duplicate() 
{
	LinearLayer2<inputDimension, inputDimension2, outputDimension>* l = new LinearLayer2<inputDimension, inputDimension2, outputDimension>();
	l->by = by;
	l->gby = gby;
	l->Uyx = Uyx;
	l->gUyx = gUyx;
	l->Uyx2 = Uyx2;
	l->gUyx2 = gUyx2;
	return (ComputeUnit*) l;
}
template<int inputDimension, int inputDimension2, int outputDimension>
void LinearLayer2<inputDimension, inputDimension2, outputDimension>::forward()
{
	x = *inputs[0];
	x2 = *inputs[1];
	y = (((*Uyx) * x) + ((*Uyx2) * x2) + (*by));
	outputs[0] = y;
}
template<int inputDimension, int inputDimension2, int outputDimension>
void LinearLayer2<inputDimension, inputDimension2, outputDimension>::backward()
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ly = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension2,1> Lx2 = Matrix<double,inputDimension2,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];;;}
	Ly += Lout;
	Lx2 += ((*Uyx2).transpose() * Ly);
	jacobians[1] = Lx2;
	Lx += ((*Uyx).transpose() * Ly);
	jacobians[0] = Lx;
	*gUyx += (Ly * x.transpose());
	*gUyx2 += (Ly * x2.transpose());
	*gby += Ly;
}
template<int inputDimension, int inputDimension2, int outputDimension>
void LinearLayer2<inputDimension, inputDimension2, outputDimension>::gradientStep(double alpha)
{
	*by -= alpha * (*gby);
	gby->setZero();
	*Uyx -= alpha * (*gUyx);
	gUyx->setZero();
	*Uyx2 -= alpha * (*gUyx2);
	gUyx2->setZero();
}
