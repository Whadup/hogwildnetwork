#pragma once
#include <iostream>
#include <Eigen/Dense>
using Eigen::Matrix;
#include "computeUnit.h"

template <int inputDimension, int hiddenDimension,int outputDimension>
class RNN : public ComputeUnit
{
public:
	//Matrix<double,hiddenDimension,1> h;
	Matrix<double,hiddenDimension,inputDimension,Eigen::RowMajor>* U = 0;
	Matrix<double,outputDimension,hiddenDimension,Eigen::ColMajor>* V = 0;
	Matrix<double,outputDimension,1>* c = 0;
	Matrix<double,hiddenDimension,hiddenDimension,Eigen::RowMajor>* W = 0;
	Matrix<double,hiddenDimension,1>* b = 0;
	RNN();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
	
};


template <int inputDimension, int hiddenDimension,int outputDimension>
RNN<inputDimension,hiddenDimension,outputDimension>::RNN()  : ComputeUnit(2,2)
{
	outputs[0]=Matrix<double,outputDimension,1>::Zero();
	outputs[1]=Matrix<double,hiddenDimension,1>::Zero();
	//h = Matrix<double,hiddenDimension,1>::Zero();
	jacobians[0]=Matrix<double,inputDimension,1>::Zero();	
	jacobians[1]=Matrix<double,hiddenDimension,1>::Zero();
}

template<int inputDimension,int hiddenDimension, int outputDimension>
void RNN<inputDimension,hiddenDimension,outputDimension>::init()
{
	if(W!=0) delete W;
	W = new Matrix<double,hiddenDimension,hiddenDimension,Eigen::RowMajor>;
	V = new Matrix<double,outputDimension,hiddenDimension,Eigen::ColMajor>;
	U = new Matrix<double,hiddenDimension,inputDimension,Eigen::RowMajor>;
	c = new Matrix<double,outputDimension,1>;
	b = new Matrix<double,hiddenDimension,1>;

	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<double> initializer(0,1);
	for(int i = 0;i<hiddenDimension;i++)
	{	
		(*b)(i,0) = 0.0;
		for(int j=0;j<inputDimension;j++)
			(*U)(i,j) = initializer(gen);
		for(int j=0;j<hiddenDimension;j++)
			(*W)(i,j) = initializer(gen);
		for(int j=0;j<outputDimension;j++)
			(*V)(j,i) = initializer(gen);
	}
	for(int j=0;j<outputDimension;j++)
		(*c)(j,0) = 0.0;
}

template<int inputDimension,int hiddenDimension, int outputDimension>
ComputeUnit* RNN<inputDimension,hiddenDimension,outputDimension>::duplicate()
{
	// std::cout << "duplicate RNN "<< std::endl;
	RNN<inputDimension,hiddenDimension,outputDimension>* l = new RNN<inputDimension,hiddenDimension,outputDimension>();
	l->W = W;
	l->V = V;
	l->U = U;
	l->c = c;
	l->b = b;
	return (ComputeUnit*)l;
}

template<int inputDimension,int hiddenDimension, int outputDimension>
void RNN<inputDimension,hiddenDimension,outputDimension>::forward()
{
	outputs[1] = ((*W) * *inputs[1] + (*U) * *inputs[0] + (*b)).array().tanh().matrix();
	outputs[0] = (*V) * outputs[1] + (*c);
	// std::cout << "relu layer "<<this << " " << outputs[0].transpose()<<std::endl;
}

template<int inputDimension,int hiddenDimension, int outputDimension>
void RNN<inputDimension,hiddenDimension,outputDimension>::backward()
{
	// std::cout << "backward " << successors[0] << " " << successors[1] << std::endl;
	if(successors[0]==0 && successors[1]!=0)
	{
		//not the last timestamp, outputs are irrellevant
		MatrixXd* jach = &successors[1]->jacobians[1];
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
		// std::cout << diag << std::endl;
		// std::cout << diag.diagonal().transpose() << std::endl;
		// std::cout << jach->rows() << " " << jach->cols() << std::endl;
		// std::cout << W->transpose().rows() << " " << W->transpose().cols() << std::endl;
		jacobians[1] = W->transpose() * diag * (*jach);
		jacobians[0] = U->transpose() * diag * (*jach);
	}
	else if(successors[0]!=0 && successors[1]!=0)
	{
		//not the last timestamp, outputs are rellevant
		MatrixXd* jac = &successors[0]->jacobians[0];
		MatrixXd* jach = &successors[1]->jacobians[1];
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
		jacobians[1] = W->transpose() * diag * (*jach) + V->transpose() * (*jac);
		jacobians[0] = U->transpose() * diag * (*jach) + U->transpose() * diag * V->transpose() * (*jac);
	}
	else
	{
		MatrixXd* jac = &successors[0]->jacobians[0];
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
		//last timestamp, outputs are always relevant
		jacobians[1] = V->transpose() * (*jac);
		jacobians[0] = U->transpose() * diag * V->transpose() * (*jac);
	}
	double d = jacobians[1].norm();
	if(d>10)
	{
		// std::cout << "C";// << std::endl;
		jacobians[1]*=10/d;
	}
}

template<int inputDimension,int hiddenDimension, int outputDimension>
void RNN<inputDimension,hiddenDimension,outputDimension>::gradientStep(double alpha)
{
	if(successors[0]==0 && successors[1]!=0)
	{
		//not the last timestamp, outputs are irrellevant
		MatrixXd* jach = &successors[1]->jacobians[1];
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
		// std::cout << "jac " << jach->rows() << " " << jach->cols() << std::endl;
		// std::cout << "inp " << inputs[1]->rows() << " " << inputs[1]->cols() << std::endl;
		// std::cout << "dia " << diag.rows() << " " << diag.cols() << std::endl;
		// std::cout << "W   " << W->rows() << " " << W->cols() << std::endl;
		// std::cout << "U   " << U->rows() << " " << U->cols() << std::endl;
		Matrix<double,hiddenDimension,1> tmp = alpha * diag * (*jach);
		*b -= tmp;
		*W -= tmp * inputs[1]->transpose();
		*U -= tmp * inputs[0]->transpose();
		// std::cout << diag << std::endl;
		// std::cout << diag.diagonal().transpose() << std::endl;
		
		// std::cout << W->transpose().rows() << " " << W->transpose().cols() << std::endl;
	}
	else if(successors[0]!=0 && successors[1]!=0)
	{
		//not the last timestamp, outputs are rellevant
		MatrixXd* jac  = &successors[0]->jacobians[0];
		MatrixXd* jach = &successors[1]->jacobians[1];
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
		Matrix<double,hiddenDimension,1> tmp = alpha * diag * (*jach);
		*c -= alpha * (*jac);
		*b -= tmp;
		*V -= alpha * (*jac) * outputs[1].transpose();
		*W -= alpha * diag * V->transpose() * (*jac) * inputs[1]->transpose() + tmp * inputs[1]->transpose();;
		*U -= alpha * diag * V->transpose() * (*jac) * inputs[0]->transpose() + tmp * inputs[0]->transpose();
		//*W -= diag *jach * inputs[1].transpose();		
	}
	else
	{
		MatrixXd* jac = &successors[0]->jacobians[0];
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
		//last timestamp, outputs are always relevant
		*V -= alpha * (*jac) * outputs[1].transpose();
		*c -= alpha * (*jac);
		*W -= alpha * diag * V->transpose() * (*jac) * inputs[1]->transpose();
		*U -= alpha * diag * V->transpose() * (*jac) * inputs[0]->transpose();
	}
}


// #endif

// #endif outputs[0]=(this->W * (*inputs[0])).cwiseMax(0);