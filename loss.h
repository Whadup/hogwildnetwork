#pragma once
class Loss
{
public: 
	virtual double loss()=0;
	virtual double acc()=0;
	virtual double weight(){return 1;}
};