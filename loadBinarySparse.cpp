#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include <utility>
#include <sys/time.h>

inline double getTime(){
	struct timeval tv;
	if (gettimeofday(&tv, NULL)==0)
		return tv.tv_sec+((double)(tv.tv_usec))/1e6;
	else
		return 0.0;
}

unsigned int toDigit(const char& c){
	if(c==49) return 1;
	else if(c==50) return 2;
	else if(c==51) return 3;
	else if(c==52) return 4;
	else if(c==53) return 5;
	else if(c==54) return 6;
	else if(c==55) return 7;
	else if(c==56) return 8;
	else if(c==57) return 9;
	return 0;
}

int toInt(const char* c, const int& n){
	int r = 0;
	int b = 1;
	for(int i=n-1; i>=0; --i){
		r += b * toDigit(c[i]);
		b *= 10;
	}
	return r;
}

int* count(const std::string& in,std::vector<int>& ref, int& length){

	const char* raw = in.c_str();
	const int n  = in.size();

	int app   = 0;
	int count = 0;

	int pos = -1;
	int state = 0;
	int gender = 0;
	ref.clear();
	for(unsigned int i=0; i<n; ++i){

		if(raw[i] == ':'){
			app = toInt(raw+pos, i-pos);
			ref.push_back(app);
			pos = i+2;

		}else if(raw[i] == '\t'){
			if(state == 0)
			{
				gender = toInt(raw+pos, i-pos);
				state = 1;
			}
			else
			{
				app = toInt(raw+pos,i-pos);
				
			}
			pos = i + 1;
		}
	}
	int* a = new int[ref.size()+2];
	length = ref.size();
	a[0] = ref.size();
	a[1] = gender;
	for(int i=0;i<ref.size();i++)
		a[2+i] = ref[i];
	return a;
	
}
