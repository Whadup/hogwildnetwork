#pragma once
#include <iostream>
#include <Eigen/Dense>
using Eigen::Matrix;
#include "computeUnit.h"

template <int inputDimension, int hiddenDimension>
class LSTM : public ComputeUnit
{
public:
	//Matrix<double,hiddenDimension,1> h;
	Matrix<double,hiddenDimension,inputDimension>* Uq = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Wq = 0;
	Matrix<double,hiddenDimension,1>* bq = 0;

	Matrix<double,hiddenDimension,inputDimension>* Uf = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Wf = 0;
	Matrix<double,hiddenDimension,1>* bf = 0;

	Matrix<double,hiddenDimension,inputDimension>* Ug = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* Wg = 0;
	Matrix<double,hiddenDimension,1>* bg = 0;
	
	Matrix<double,hiddenDimension,inputDimension>* U = 0;
	Matrix<double,hiddenDimension,hiddenDimension>* W = 0;
	Matrix<double,hiddenDimension,1>* b = 0;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,hiddenDimension,1> f;
	Matrix<double,hiddenDimension,1> g;
	Matrix<double,hiddenDimension,1> z;
	Matrix<double,hiddenDimension,1> q;

	Matrix<double,hiddenDimension,1> Lf;
	Matrix<double,hiddenDimension,1> Lg;
	Matrix<double,hiddenDimension,1> Lz;
	Matrix<double,hiddenDimension,1> Lq;
	Matrix<double,hiddenDimension,1> Ls;

	LSTM();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
	
};


template <int inputDimension, int hiddenDimension>
LSTM<inputDimension,hiddenDimension>::LSTM()  : ComputeUnit(2,2)
{
	outputs[0]=Matrix<double,hiddenDimension,1>::Zero();
	outputs[1]=Matrix<double,hiddenDimension,1>::Zero();
	//h = Matrix<double,hiddenDimension,1>::Zero();
	jacobians[0]=Matrix<double,inputDimension,1>::Zero();	
	jacobians[1]=Matrix<double,hiddenDimension,1>::Zero();
	
	f = Matrix<double,hiddenDimension,1>::Zero();
	g = Matrix<double,hiddenDimension,1>::Zero();
	z = Matrix<double,hiddenDimension,1>::Zero();
	q = Matrix<double,hiddenDimension,1>::Zero();
}

template<int inputDimension,int hiddenDimension>
void LSTM<inputDimension,hiddenDimension>::init()
{
	U = new Matrix<double,hiddenDimension,inputDimension>();
	W = new Matrix<double,hiddenDimension,hiddenDimension>();
	b = new Matrix<double,hiddenDimension,1>();

	Uf = new Matrix<double,hiddenDimension,inputDimension>();
	Wf = new Matrix<double,hiddenDimension,hiddenDimension>();
	bf = new Matrix<double,hiddenDimension,1>();

	Ug = new Matrix<double,hiddenDimension,inputDimension>();
	Wg = new Matrix<double,hiddenDimension,hiddenDimension>();
	bg = new Matrix<double,hiddenDimension,1>();

	Uq = new Matrix<double,hiddenDimension,inputDimension>();
	Wq = new Matrix<double,hiddenDimension,hiddenDimension>();
	bq = new Matrix<double,hiddenDimension,1>();

	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<double> initializer(0,0.1);
	for(int i = 0;i<hiddenDimension;i++)
	{	
		(*b)(i,0) = 0.0;
		(*bf)(i,0) =  2.0;
		(*bg)(i,0) = -1.0;
		(*bq)(i,0) = -2.0;
		for(int j=0;j<inputDimension;j++)
		{
			(*U)(i,j)  = initializer(gen);
			(*Uf)(i,j) = initializer(gen);
			(*Ug)(i,j) = initializer(gen);
			(*Uq)(i,j)  = initializer(gen);
		}
		for(int j=0;j<hiddenDimension;j++)
		{
			(*W)(i,j)  = initializer(gen);
			(*Wf)(i,j) = initializer(gen);
			(*Wg)(i,j) = initializer(gen);
			(*Wq)(i,j) = initializer(gen);	
		}
	}
}

template<int inputDimension,int hiddenDimension>
ComputeUnit* LSTM<inputDimension,hiddenDimension>::duplicate()
{
	// std::cout << "duplicate LSTM "<< std::endl;
	LSTM<inputDimension,hiddenDimension>* l = new LSTM<inputDimension,hiddenDimension>();
	l->U = U;
	l->W = W;
	l->b = b;

	l->Uf = Uf;
	l->Wf = Wf;
	l->bf = bf;

	l->Ug = Ug;
	l->Wg = Wg;
	l->bg = bg;

	l->Uq = Uq;
	l->Wq = Wq;
	l->bq = bq;

	return (ComputeUnit*)l;
}

template<int inputDimension,int hiddenDimension>
void LSTM<inputDimension,hiddenDimension>::forward()
{

	f = ((*bf) + (*Uf) * (*inputs[0]) + (*Wf) * (*inputs[1]));
	g = ((*bg) + (*Ug) * (*inputs[0]) + (*Wg) * (*inputs[1]));
	z = ((*b ) + (*U ) * (*inputs[0]) + (*W ) * (*inputs[1]));
	q = ((*bq) + (*Uq) * (*inputs[0]) + (*Wq) * (*inputs[1]));

	// std::cout << z.transpose()<<" => ";

	f = (Matrix<double,hiddenDimension,1>::Constant(1) + (-f).array().exp().matrix()).array().cwiseInverse().matrix();
	g = (Matrix<double,hiddenDimension,1>::Constant(1) + (-g).array().exp().matrix()).array().cwiseInverse().matrix();
	z = (Matrix<double,hiddenDimension,1>::Constant(1) + (-z).array().exp().matrix()).array().cwiseInverse().matrix();
	q = (Matrix<double,hiddenDimension,1>::Constant(1) + (-q).array().exp().matrix()).array().cwiseInverse().matrix();

	// std::cout << z.transpose() << std::endl;
	outputs[1] = (f.array() * inputs[1]->array()).matrix()
				+(g.array() * z.array()).matrix();
	outputs[0] = (outputs[1].array().tanh() * q.array()).matrix();
	// std::cout << "lstm layer "<<this << " " << outputs[0].transpose()
		// << " | " << outputs[1].transpose()
		// << " | " << f.transpose()
		// << " | " << g.transpose()
		// << " | " << q.transpose()
		// << " | " << z.transpose()
		// <<std::endl;
}

template<int inputDimension,int hiddenDimension>
void LSTM<inputDimension,hiddenDimension>::backward()
{
	// std::cout << "backward " << successors[0] << " " << successors[1] << std::endl;
	Ls = Matrix<double,hiddenDimension,1>::Zero();
	if(successors[0]!=0)
	{	
		MatrixXd* jac = &successors[0]->jacobians[0];
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].array().tanh().cwiseAbs2().matrix());
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag2(q);
		Ls += diag2 * (diag * (*jac));
	}
	if(successors[1]!=0)
	{
		Ls += successors[1]->jacobians[1];
	}
	

	Lq = Matrix<double,hiddenDimension,1>::Zero();
	if(successors[0]!=0)
	{
		MatrixXd* jac = &successors[0]->jacobians[0];
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(outputs[1].array().tanh().matrix());
		Lq = diag * (*jac);
	}
	Lf = Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension>(*inputs[1]) * Ls;
	Lg = Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension>(z) * Ls;
	Lz = Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension>(g) * Ls;

	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((g.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-g).array()).matrix());
		jacobians[0] = Ug->transpose() * diag * Lg;
		jacobians[1] = Wg->transpose() * diag * Lg;
	}
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((f.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-f).array()).matrix());
		jacobians[0] += Uf->transpose() * diag * Lf;
		jacobians[1] += Wf->transpose() * diag * Lf;
	}
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((q.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-q).array()).matrix());
		jacobians[0] += Uq->transpose() * diag * Lq;
		jacobians[1] += Wq->transpose() * diag * Lq;
	}
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((z.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-z).array()).matrix());
		jacobians[0] += U->transpose() * diag * Lz;
		jacobians[1] += W->transpose() * diag * Lz;
	}
	jacobians[1] += Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension>(f) * Ls;
	double d = jacobians[1].norm();
	if(d>10)
	{
		// std::cout << "C";// << std::endl;
		jacobians[1]*=10/d;
	}
	// std::cout << jacobians[1].transpose() << std::endl;
}

template<int inputDimension,int hiddenDimension>
void LSTM<inputDimension,hiddenDimension>::gradientStep(double alpha)
{
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((z.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-z).array()).matrix());
		*U -= alpha * diag * Lz * inputs[0]->transpose();
		*W -= alpha * diag * Lz * inputs[1]->transpose();
		*b -= alpha * diag * Lz;
	}
	
	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((f.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-f).array()).matrix());
		*Uf -= alpha * diag * Lf * inputs[0]->transpose();
		*Wf -= alpha * diag * Lf * inputs[1]->transpose();
		*bf -= alpha * diag * Lf;
	}

	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((g.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-g).array()).matrix());
		*Ug -= alpha * diag * Lg * inputs[0]->transpose();
		*Wg -= alpha * diag * Lg * inputs[1]->transpose();
		*bg -= alpha * diag * Lg;
	}

	{
		Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag((q.array() * (Matrix<double,hiddenDimension,1>::Constant(1)-q).array()).matrix());
		*Uq -= alpha * diag * Lq * inputs[0]->transpose();
		*Wq -= alpha * diag * Lq * inputs[1]->transpose();
		*bq -= alpha * diag * Lq;
	}
	// if(successors[0]==0 && successors[1]!=0)
	// {
	// 	//not the last timestamp, outputs are irrellevant
	// 	MatrixXd* jach = &successors[1]->jacobians[1];
	// 	Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
	// 	// std::cout << "jac " << jach->rows() << " " << jach->cols() << std::endl;
	// 	// std::cout << "inp " << inputs[1]->rows() << " " << inputs[1]->cols() << std::endl;
	// 	// std::cout << "dia " << diag.rows() << " " << diag.cols() << std::endl;
	// 	// std::cout << "W   " << W->rows() << " " << W->cols() << std::endl;
	// 	// std::cout << "U   " << U->rows() << " " << U->cols() << std::endl;
	// 	Matrix<double,hiddenDimension,1> tmp = alpha * diag * (*jach);
	// 	*b -= tmp;
	// 	*W -= tmp * inputs[1]->transpose();
	// 	*U -= tmp * inputs[0]->transpose();
	// 	// std::cout << diag << std::endl;
	// 	// std::cout << diag.diagonal().transpose() << std::endl;
		
	// 	// std::cout << W->transpose().rows() << " " << W->transpose().cols() << std::endl;
	// }
	// else if(successors[0]!=0 && successors[1]!=0)
	// {
	// 	//not the last timestamp, outputs are rellevant
	// 	MatrixXd* jac  = &successors[0]->jacobians[0];
	// 	MatrixXd* jach = &successors[1]->jacobians[1];
	// 	Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
	// 	Matrix<double,hiddenDimension,1> tmp = alpha * diag * (*jach);
	// 	*c -= alpha * (*jac);
	// 	*b -= tmp;
	// 	*V -= alpha * (*jac) * outputs[1].transpose();
	// 	*W -= alpha * diag * V->transpose() * (*jac) * inputs[1]->transpose() + tmp * inputs[1]->transpose();;
	// 	*U -= alpha * diag * V->transpose() * (*jac) * inputs[0]->transpose() + tmp * inputs[0]->transpose();
	// 	//*W -= diag *jach * inputs[1].transpose();		
	// }
	// else
	// {
	// 	MatrixXd* jac = &successors[0]->jacobians[0];
	// 	Eigen::DiagonalMatrix<double,hiddenDimension,hiddenDimension> diag(Matrix<double,hiddenDimension,1>::Constant(1)-outputs[1].cwiseAbs2());
	// 	//last timestamp, outputs are always relevant
	// 	*V -= alpha * (*jac) * outputs[1].transpose();
	// 	*c -= alpha * (*jac);
	// 	*W -= alpha * diag * V->transpose() * (*jac) * inputs[1]->transpose();
	// 	*U -= alpha * diag * V->transpose() * (*jac) * inputs[0]->transpose();
	// }
}


// #endif

// #endif outputs[0]=(this->W * (*inputs[0])).cwiseMax(0);