
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;

#ifndef NORMAL
#define NORMAL
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<> d(0,1/10.0);
std::normal_distribution<> initializer(0,0.1);
#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;
#endif 

#include "computeUnit.h" 
template <int inputDimension, int outputDimension>
class SigmoidUnit: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,outputDimension,1> y;
	Matrix<double,outputDimension,1> out;
	Matrix<double,outputDimension,inputDimension>* Uyx = 0;
	Matrix<double,outputDimension,inputDimension>* gUyx = 0;
	Matrix<double,outputDimension,1>* by = 0;
	Matrix<double,outputDimension,1>* gby = 0;
	SigmoidUnit();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();

	SigmoidUnit<inputDimension,outputDimension>* deepCopy()
	{
		SigmoidUnit<inputDimension,outputDimension>* l = new SigmoidUnit<inputDimension,outputDimension>();
		l->gby = new Matrix<double,outputDimension,1>;
		l->gby->setZero();
		l->gUyx = new Matrix<double,outputDimension,inputDimension>;
		l->gUyx->setZero();
		l->Uyx = new Matrix<double,outputDimension,inputDimension>;
		*l->Uyx = *Uyx;
		l->by = new Matrix<double,outputDimension,1>;
		*l->by = *by;
		return l;
	}
	int min()
	{
		int min = 0;
		for(int i=1;i<outputDimension;i++)
			if(outputs[0](i,0) < outputs[0](min,0))
				min = i;
		return min;
	}
		
};
			
template<int inputDimension, int outputDimension>
SigmoidUnit<inputDimension, outputDimension>::SigmoidUnit() : ComputeUnit(1,0,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
}

template<int inputDimension, int outputDimension>
void SigmoidUnit<inputDimension, outputDimension>::init() 
{
	Uyx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uyx);
	*Uyx = (Uyx->array()/inputDimension).matrix();
	zeroInitialization(Uyx);
	gUyx = new Matrix<double,outputDimension,inputDimension>();
	gUyx->setZero();
	by = new Matrix<double,outputDimension,1>();
	zeroInitialization(by);
	gby = new Matrix<double,outputDimension,1>();
	gby->setZero();
}
template<int inputDimension, int outputDimension>
ComputeUnit* SigmoidUnit<inputDimension, outputDimension>::duplicate() 
{
	SigmoidUnit<inputDimension, outputDimension>* l = new SigmoidUnit<inputDimension, outputDimension>();
	l->Uyx = Uyx;
	l->gUyx = gUyx;
	l->by = by;
	l->gby = gby;
	return (ComputeUnit*) l;
}
template<int inputDimension, int outputDimension>
void SigmoidUnit<inputDimension, outputDimension>::forward()
{
	x = *inputs[0];
	// std::cout << x.transpose() << std::endl;
	y = (1.0 + (-(((*Uyx) * x) + (*by))).array().exp()).inverse().matrix();;
	// std::cout << y << " " << (((*Uyx) * x) + (*by)) << std::endl;
	outputs[0] = y;
}
template<int inputDimension, int outputDimension>
void SigmoidUnit<inputDimension, outputDimension>::backward()
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ly = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];;;}
	Ly += Lout;
	Lx += ((*Uyx).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((y.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -y).array()).matrix()) * Ly);
	jacobians[0] = Lx;
	*gUyx += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((y.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -y).array()).matrix()) * Ly * x.transpose());
	*gby += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((y.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -y).array()).matrix()) * Ly);
}
template<int inputDimension, int outputDimension>
void SigmoidUnit<inputDimension, outputDimension>::gradientStep(double alpha)
{
	*Uyx -= alpha * (*gUyx);
	gUyx->setZero();
	*by -= alpha * (*gby);
	gby->setZero();
}
