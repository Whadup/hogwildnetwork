#pragma once
// #ifndef _LINEARLAYER_
// #define _LINEARLAYER_
#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
#include <random>
#include "dataInterfaces.h"
#include <algorithm>
#include <map>
#include <string> 
#include <mutex>
using Eigen::Matrix;

template<int vocabularySize,int embeddingDimension>
class EmbeddingLayer: public ComputeUnit, public IntegerInput
{
public:
	EmbeddingLayer();
	MatrixXd* W;//(embeddingDimension,vocabularySize);
	MatrixXd* gW;
	std::vector<int>* updates;
	std::mutex* mtx;
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual void init();
	virtual ComputeUnit* duplicate();
	void loadEmbeddings(const char* fileName,const char* vocFile)
	{

		FILE* f;
		f = fopen(vocFile, "rb");
		std::map<std::string,int> vocabulary;

		while(!feof(f))
		{
			char word[100];
			char ch;
			int token;
			int freq;
			fscanf(f, "%s%c%d%d", &word[0], &ch,&token,&freq);
			std::string sword(word);
			vocabulary[sword] = token;
			// std::cout << word << " "<< token << " " << freq << " ";
		}

		fclose(f);
		f = fopen(fileName, "rb");
		
		long long words;
		long long size;

		fscanf(f, "%lld", &words);
		fscanf(f, "%lld", &size);
		
		

		std::cout << "embeddings loading with "<<words << " " << size << std::endl;
		// vocab = (char *)malloc((long long)words * max_w * sizeof(char));
		// M = (float *)malloc((long long)words * (long long)size * sizeof(float));
		// if (M == NULL) {
		// 	printf("Cannot allocate memory: %lld MB    %lld  %lld\n", (long long)words * size * sizeof(float) / 1048576, words, size);
		// 	return -1;
		// }
		int hits = 0;

		int overlap = embeddingDimension;
		if(size<embeddingDimension) overlap = size;

		for (int b = 0; b < words; b++)
		{
			char word[50];
			char ch;
			fscanf(f, "%s%c", &word[0], &ch);
			std::string sword(word);
			//std::transform(sword.begin(), sword.end(), sword.begin(), ::tolower);
			
			float embedding[size];
			
			for(int a = 0; a < size; a++) fread(&embedding[a], sizeof(float), 1, f);
			float len = 0;
			for (int a = 0; a < size; a++) len += embedding[a] * embedding[a];
			len = sqrt(len);
			for (int a = 0; a < size; a++) embedding[a] /= len;
	
  			if (vocabulary.count(sword)>0)
  			{
  				int token = vocabulary[sword];
  				for(int i=0;i<overlap;i++)
  				{
  					(*W)(i,token) = embedding[i];
  				}
  				std::cout << sword << " has an embedding " << hits++ << std::endl;
  				// std::cout << W->col(token).transpose() << std::endl;
  			}
		}
		fclose(f);
	}
	
};
	



template<int vocabularySize,int embeddingDimension>
EmbeddingLayer<vocabularySize,embeddingDimension>::EmbeddingLayer() : ComputeUnit(0,0,1), IntegerInput()
{
	outputs[0]=MatrixXd(embeddingDimension,1);
}

template<int vocabularySize,int embeddingDimension>
void EmbeddingLayer<vocabularySize,embeddingDimension>::init()
{
	W = new MatrixXd(embeddingDimension,vocabularySize);
	W->setRandom();
	*W/=(2*embeddingDimension);
	// std::random_device rd;
	// std::mt19937 gen(rd());
	// std::normal_distribution<double> initializer(0,1);
	// for(int i = 0;i<embeddingDimension;i++)
	// 	for(int j=0;j<vocabularySize;j++)
	// 		(*W)(i,j) = initializer(gen)/(embeddingDimension/64);

	// W->setZero(); // OMG DONT DO THAT
	gW = new MatrixXd(embeddingDimension,vocabularySize);
	gW->setZero();
	updates = new std::vector<int>;
	mtx = new std::mutex;
}

template<int vocabularySize,int embeddingDimension>
ComputeUnit* EmbeddingLayer<vocabularySize,embeddingDimension>::duplicate()
{
	#warning "screwed up duplicate() in embeddingLayer.h"
	// std::cout << "duplicating EmbeddingLayer" << std::endl;
	EmbeddingLayer<vocabularySize,embeddingDimension>* l = new EmbeddingLayer();
	l->W = W;
	// l->gW = gW;
	l->gW = new MatrixXd(embeddingDimension,vocabularySize);
	l->gW->setZero();
	// l->updates = updates;
	l->updates = new std::vector<int>();
	l->mtx = mtx;
	return (ComputeUnit*)l;
}

template<int vocabularySize,int embeddingDimension>
void EmbeddingLayer<vocabularySize,embeddingDimension>::forward()
{
	outputs[0]=this->W->col(input);
}

template<int vocabularySize,int embeddingDimension>
void EmbeddingLayer<vocabularySize,embeddingDimension>::backward()
{

	MatrixXd* jac = &successorDeep->jacobians[this->connectedToSlot];	
	gW->col(input)+= (*jac);
	// mtx->lock();
	updates->push_back(input);
	// mtx->unlock();
}

template<int vocabularySize,int embeddingDimension>
void EmbeddingLayer<vocabularySize,embeddingDimension>::gradientStep(double alpha)
{
	
	// double delta = (*jac).sum();//jacobianW * (*jac);
	// mtx->lock();
	for(int i=0;i<updates->size();i++)
	{
		int j = (*updates)[i];
		W->col(j) -= alpha * gW->col(j);
		gW->col(j).setZero();
	}
	updates->clear();
	// mtx->unlock();
	// for(int j=0;j<embeddingDimension;j++)
	// {
	// 	(*W)(j,input)-=alpha * (*jac)(j,0);
	// }

}
