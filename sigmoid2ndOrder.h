
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;

#ifndef NORMAL
#define NORMAL
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<> d(0,1/10.0);
std::normal_distribution<> initializer(0,0.1);
#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;
#endif 

#include "computeUnit.h" 
template <int inputDimension>
class Sigmoid2ndOrder: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,1,1> y;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> u;
	Matrix<double,1,1> out;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> t;
	Matrix<double,1,1>* by = 0;
	Matrix<double,1,1>* gby = 0;
	Sigmoid2ndOrder();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension>
Sigmoid2ndOrder<inputDimension>::Sigmoid2ndOrder() : ComputeUnit(2,0,1) 
{
	outputs[0] = Matrix<double,1,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,inputDimension,1>::Zero();
}

template<int inputDimension>
void Sigmoid2ndOrder<inputDimension>::init() 
{
	by = new Matrix<double,1,1>();
	zeroInitialization(by);
	gby = new Matrix<double,1,1>();
	gby->setZero();
}
template<int inputDimension>
ComputeUnit* Sigmoid2ndOrder<inputDimension>::duplicate() 
{
	Sigmoid2ndOrder<inputDimension>* l = new Sigmoid2ndOrder<inputDimension>();
	l->by = by;
	l->gby = gby;
	return (ComputeUnit*) l;
}
template<int inputDimension>
void Sigmoid2ndOrder<inputDimension>::forward()
{
	u = *inputs[0];
	t = *inputs[1];
	// std::cout << u.transpose() << " " << t.transpose() << "  => ";
	y = (1.0 + (-(u.transpose() * t + (*by))).array().exp()).inverse().matrix();
	// std::cout << y.transpose() << std::endl;
	outputs[0] = y;
}
template<int inputDimension>
void Sigmoid2ndOrder<inputDimension>::backward()
{
	Matrix<double,1,1> Lout = Matrix<double,1,1>::Zero();
	Matrix<double,1,1> Ly = Matrix<double,1,1>::Zero();
	Matrix<double,inputDimension,1> Lt = Matrix<double,inputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lu = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];;;}
	Ly += Lout;
	Lt += u * Eigen::DiagonalMatrix<double,1,1>((y.array() * (Matrix<double,1,1>::Constant(1) +  -y).array()).matrix()) * Ly;
	jacobians[1] = Lt;
	Lu += t * Eigen::DiagonalMatrix<double,1,1>((y.array() * (Matrix<double,1,1>::Constant(1) +  -y).array()).matrix()) * Ly;
	jacobians[0] = Lu;
	*gby += (Eigen::DiagonalMatrix<double,1,1>((y.array() * (Matrix<double,1,1>::Constant(1) +  -y).array()).matrix()) * Ly);
}
template<int inputDimension>
void Sigmoid2ndOrder<inputDimension>::gradientStep(double alpha)
{
	*by -= alpha * (*gby);
	gby->setZero();
}
