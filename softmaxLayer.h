#pragma once
// #ifndef _L2LOSS_
// #define _L2LOSS_
#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
using Eigen::Matrix;

template<int dimension>
class SoftmaxLayer: public ComputeUnit
{
public:
	SoftmaxLayer();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha){};
	virtual ComputeUnit* duplicate();
};

template<int dimension>
SoftmaxLayer<dimension>::SoftmaxLayer()  : ComputeUnit(1,0,1)
{
	jacobians[0]=Matrix<double,dimension,1>::Zero();

}

template<int dimension>
ComputeUnit* SoftmaxLayer<dimension>::duplicate()
{
	return (ComputeUnit*)new SoftmaxLayer();
}

template<int dimension>
void SoftmaxLayer<dimension>::forward()
{
	/*
	if (exponentialTerms.length == 0) {
			return Double.NEGATIVE_INFINITY;
		}
//		if(exponentialTerms.length == 2 && exponentialTerms[1]==0)
//			return exponentialTerms[0];
		double maxTerm = Double.NEGATIVE_INFINITY;
		for (double d : exponentialTerms) {
			if (d > maxTerm) {
				maxTerm = d;
			}
		}

		if (maxTerm == Double.NEGATIVE_INFINITY) {
			return Double.NEGATIVE_INFINITY;
		}

		double sum = 0.;
		for (double d : exponentialTerms) {
			sum += Math.exp(d - maxTerm);
		}

		return maxTerm + Math.log(sum);
		*/
	
	//std::cout << inputs[0]->transpose() << std::endl;
	double maxTerm = (*inputs[0])(0,0);
	for(int i=0; i< dimension; i++)
		if((*inputs[0])(i,0)>maxTerm)
			maxTerm=(*inputs[0])(i,0);
	double logSum = 0;
	for(int i=0;i<dimension;i++)
		logSum += exp((*inputs[0])(i,0) - maxTerm);
	logSum = maxTerm + log(logSum);
	outputs[0] = ((*inputs[0])-Matrix<double,dimension,1>::Constant(logSum)).array().exp().matrix();

	// Matrix<double,dimension,1> logits = inputs[0]->array().exp().matrix();
	// double s = logits.sum();
	// if(s!=s)
	// {
	// 	std::cout << "NANA "<< s << " " << inputs[0]->transpose()<< std::endl;
	// }
	// outputs[0] = logits / s;
	// if(s<2e-8)
	// 	std::cout << "no denominator " << s <<std::endl;
}

template<int dimension>
void SoftmaxLayer<dimension>::backward()
{
	MatrixXd* jac = &successorDeep->jacobians[0];
	MatrixXd g = -1.0 * (outputs[0]*outputs[0].transpose());
	for(int i=0;i<dimension;i++)
		g(i,i)=outputs[0](i,0)*(1-outputs[0](i,0));
	jacobians[0] = g * (*jac);
}
