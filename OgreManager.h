#pragma once
#include <thread>
#include <atomic>
#include <mutex>
#include <iostream>
#include <condition_variable>
#include "networkExecuter.h"
#include "dataSource.h"

template<int batchSize,int numberOfThreads>
class OgreManager
{
public:
	NetworkExecuter** networkCopies;
	DataSource* dataSource;
	std::thread* threads;

	std::atomic_int layer;
	std::atomic_int nextCopy;
	std::atomic_int readyCounter;
	std::mutex mtx;
	std::condition_variable cv;
	int len;

	OgreManager(NetworkExecuter* e,DataSource* d);
	virtual void feedData(int i,void* d);
	void endlessLoop(int i);
	void go();
};


template<int batchSize,int numberOfThreads>
OgreManager<batchSize,numberOfThreads>::OgreManager(NetworkExecuter* e,DataSource* d)
{
	
	dataSource = d;
	networkCopies = new NetworkExecuter*[batchSize];
	networkCopies[0]=e;
	for(int i=1;i<batchSize;i++)
	{
		networkCopies[i] = e->duplicate();
	}
}

template<int batchSize,int numberOfThreads>
void OgreManager<batchSize,numberOfThreads>::go()
{
	layer = 0;
	nextCopy = 0;
	readyCounter = 0;
	std::cout << "lese daten" << std::endl;
	void** data = new void*[batchSize];
	len = dataSource->nextItem(batchSize,data);
	for(int i=0;i<len;i++)
		feedData(i,data[i]);

	threads = new std::thread[numberOfThreads];
	std::cout << "go " << numberOfThreads << std::endl;
	for(int i=0;i<numberOfThreads;i++)
		threads[i] = std::thread(&OgreManager<batchSize,numberOfThreads>::endlessLoop,this,i);
	for(int i=0;i<numberOfThreads;i++)
		threads[i].join();
}

template<int batchSize,int numberOfThreads>
void OgreManager<batchSize,numberOfThreads>::endlessLoop(int threadId)
{
	std::cout << threadId << std::endl;
	while(true)
	{
		if(len==0)
			return;

		int ll;
		int current;
		ll = layer;
		current = nextCopy++;

		if(current<len && ll<2*networkCopies[0]->numLayers)
		{
			NetworkExecuter* e = networkCopies[current];
			// std::cout << (threadId ? "b" : "B") << l << std::endl;
			// std::cout << "B" << threadId<<" " << current << std::endl;
			if(ll<e->numLayers)
				e->layers[ll]->forward();				
			else
			{
				e->layers[2*e->numLayers-ll-1]->backward();
				e->layers[2*e->numLayers-ll-1]->gradientStep(0.01);
			}
		}
		else
		{
			std::lock_guard<std::mutex> lck(mtx);
			// std::cout << (threadId ? "a" : "A") << std::endl;
			if(nextCopy>=len)
			{
				layer++;
				if(layer==networkCopies[0]->numLayers)
				{
					double l = 0;
					for(int i=0;i<len;i++)
						l+=((CrossEntropyLoss<2>*)networkCopies[i]->layers[layer-1])->loss();
					l/=len;
					// std::cout << l << " in the last "<<len << " data points" << std::endl;
				}
				if(layer>=2*networkCopies[0]->numLayers)
				{
					void** data = new void*[batchSize];
					len = dataSource->nextItem(batchSize,data);
					for(int i=0;i<len;i++)
						feedData(i,data[i]);
					layer = 0;
					// if(len==0)
					// {
					// 	// lck.unlock();
					// 	return;
					// }
				}

				nextCopy = 0;
			}
			//lck.unlock();
		}
	}
}



// template<int batchSize,int numberOfThreads>
// void OgreManager<batchSize,numberOfThreads>::endlessLoop(int threadId)
// {
	
// 	while(true)
// 	{
// 		if(len==0)
// 			return;
// 		int current = nextCopy++;
// 		if(current<len)
// 		{
// 			NetworkExecuter* e = networkCopies[current];
// 			// std::cout << (threadId ? "b" : "B") << std::endl;
// 			// std::cout << "B" << threadId<<" " << current << std::endl;
// 			if(layer<e->numLayers)
// 				e->layers[layer]->forward();				
// 			else
// 			{
// 				e->layers[2*e->numLayers-layer-1]->backward();
// 				e->layers[2*e->numLayers-layer-1]->gradientStep(0.01);
// 			}
// 		}
// 		else
// 		{
// 			// std::cout << "A" << threadId<< " " << current << std::endl;
// 			int a = ++readyCounter;
// 			if(a==numberOfThreads)
// 			{
// 				std::unique_lock<std::mutex> lck(mtx);
// 				// std::cout << (threadId ? "c" : "C") << std::endl;
// 				layer++;
// 				if(layer==networkCopies[0]->numLayers)
// 				{
// 					double l = 0;
// 					for(int i=0;i<len;i++)
// 						l+=((CrossEntropyLoss<2>*)networkCopies[i]->layers[layer-1])->loss();
// 					l/=len;
// 					// std::cout << l << " in the last "<<len << " data points" << std::endl;
// 				}
// 				if(layer>=2*networkCopies[0]->numLayers)
// 				{
// 					layer = 0;
// 					void** data = new void*[batchSize];
// 					len = dataSource->nextItem(batchSize,data);
// 					for(int i=0;i<len;i++)
// 						feedData(i,data[i]);
// 				}
				
// 				nextCopy = 0;
// 				readyCounter--;
// 				// std::cout << nextCopy << " " << readyCounter << " " << len << std::endl;
// 				cv.notify_all();
// 				lck.unlock();
// 			}
// 			else
// 			{
// 				std::unique_lock<std::mutex> lck(mtx);
// 				// std::cout << (threadId ? "d" : "D") << std::endl;
// 				// cv.wait(lck,[this]{return nextCopy<len;});
// 				cv.wait(lck);
// 				while(nextCopy >= len && len>0) cv.wait(lck);
// 				// 	std::cout << (threadId ? "f" : "F") << std::endl;
// 				// 	//std::cout << "F"  << threadId << nextCopy << " " << readyCounter << " " << len << std::endl;
					
					
// 				// }
// 				readyCounter--;
// 				lck.unlock();
// 				// std::cout << (threadId ? "e" : "E") << std::endl;
// 			}
// 		}

// 	}
// }


template<int batchSize,int numberOfThreads>
void OgreManager<batchSize,numberOfThreads>::feedData(int i,void* d)
{
	int* datenDing = (int*) d;
	int l = datenDing[0];
	int y = datenDing[1];
	int* x = &datenDing[2];
	EmbedAndReduceLayer<3000,64,true>* inputUnit = (EmbedAndReduceLayer<3000,64,true>*)networkCopies[i]->layers[0];
	CrossEntropyLoss<2>* outputUnit = (CrossEntropyLoss<2>*)networkCopies[i]->layers[networkCopies[i]->numLayers-1];
	inputUnit->l = l;
	inputUnit->input = x;
	outputUnit->y = y;
}
