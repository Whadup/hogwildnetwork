
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;

#ifndef NORMAL
#define NORMAL
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<> d(0,1/10.0);
std::normal_distribution<> initializer(0,0.1);
#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;
#endif 
#include "computeUnit.h" 
template <int inputDimension, int outputDimension>
class RNN: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,outputDimension,1> out;
	Matrix<double,outputDimension,1> st;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> s;

	Matrix<double,outputDimension,1>* bst = 0;
	MatrixXd* Usts = 0;
	MatrixXd* Ustx = 0;

	Matrix<double,outputDimension,1>* gbst = 0;
	MatrixXd* gUsts = 0;
	MatrixXd* gUstx = 0;

	Matrix<double,outputDimension,1>* mbst = 0;
	MatrixXd* mUsts = 0;
	MatrixXd* mUstx = 0;

	RNN();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension, int outputDimension>
RNN<inputDimension, outputDimension>::RNN() : ComputeUnit(1,1,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Constant(1);
	outputs[1] = Matrix<double,outputDimension,1>::Constant(1);
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,outputDimension,1>::Zero();
}

template<int inputDimension, int outputDimension>
void RNN<inputDimension, outputDimension>::init() 
{
	bst = new Matrix<double,outputDimension,1>();
	zeroInitialization(bst);
	Usts = new MatrixXd(outputDimension,outputDimension);
	gaussianInitialization(Usts);
	Ustx = new MatrixXd(outputDimension,inputDimension);
	Ustx->setIdentity(); //gaussianInitialization(Ustx);

	gbst = new Matrix<double,outputDimension,1>();
	gUsts = new MatrixXd(outputDimension,outputDimension);
	gUstx = new MatrixXd(outputDimension,inputDimension);
	gbst->setZero();
	gUsts->setZero();
	gUstx->setZero();

	mbst = new Matrix<double,outputDimension,1>();
	mUsts = new MatrixXd(outputDimension,outputDimension);
	mUstx = new MatrixXd(outputDimension,inputDimension);
	mbst->setZero();
	mUsts->setZero();
	mUstx->setZero();

}
template<int inputDimension, int outputDimension>
ComputeUnit* RNN<inputDimension, outputDimension>::duplicate() 
{
	RNN<inputDimension, outputDimension>* l = new RNN<inputDimension, outputDimension>();
	l->bst = bst;
	l->Usts = Usts;
	l->Ustx = Ustx;
	l->gUstx = gUstx;
	l->gUsts = gUsts;
	l->gbst = gbst;
	l->mUstx = mUstx;
	l->mUsts = mUsts;
	l->mbst =  mbst;

	return (ComputeUnit*) l;
}
template<int inputDimension, int outputDimension>
void RNN<inputDimension, outputDimension>::forward()
{
	x = *inputs[0];
	s = *inputs[1];
	st = (1.0 + (-(((*Ustx) * x) + ((*Usts) * s) + (*bst))).array().exp()).inverse().matrix();;
	outputs[1] = st;
	outputs[0] = st;
}
template<int inputDimension, int outputDimension>
void RNN<inputDimension, outputDimension>::backward()
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lst = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ls = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];};
	if(successorTime!=0) {Lst += this->successorTime->jacobians[1];};
	Lst += Lout;
	Ls += ((*Usts).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((st.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -st).array()).matrix()) * Lst);
	jacobians[1] = Ls;
	Lx += ((*Ustx).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((st.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -st).array()).matrix()) * Lst);
	jacobians[0] = Lx;
	//*gUstx-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((st.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -st).array()).matrix()) * Lst * x.transpose());
	*gUsts += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((st.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -st).array()).matrix()) * Lst * s.transpose());
	*gUsts += (0.000001 * Usts->array()).matrix();
	*gbst  += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((st.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -st).array()).matrix()) * Lst);
}

template<int inputDimension, int outputDimension>
void RNN<inputDimension, outputDimension>::gradientStep(double alpha)
{
	//*mUsts = 0.9 * (*mUsts) - alpha * (*gUsts);
	//*mbst  = 0.9 * (*mbst) - alpha * (*gbst);

	// *Usts += *mUsts;
	// *bst += *mbst;

	*Usts -= alpha * *gUsts;
	*bst -= alpha * *gbst;

	gbst->setZero();
	gUsts->setZero();
}
