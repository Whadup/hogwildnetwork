#pragma once
// #ifndef _LINEARLAYER_
// #define _LINEARLAYER_
#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
#include <random>
using Eigen::Matrix;

template<int inputDimension,int hiddenDimension>
class LinearLayer: public ComputeUnit
{
public:
	MatrixXd* W = 0;
	MatrixXd* gW = 0;
	Matrix<double,hiddenDimension,1>* b = 0;
	Matrix<double,hiddenDimension,1>* gb = 0;
	LinearLayer();
	virtual void init();
	virtual void forward();
	virtual void backward();
	virtual void gradientStep(double alpha);
	virtual ComputeUnit* duplicate();
	
};


template<int inputDimension,int hiddenDimension>
LinearLayer<inputDimension,hiddenDimension>::LinearLayer()  : ComputeUnit(1,0,1)
{
	outputs[0]=Matrix<double,hiddenDimension,1>::Zero();
	jacobians[0]=Matrix<double,inputDimension,1>::Zero();	
}

template<int inputDimension,int hiddenDimension>
void LinearLayer<inputDimension,hiddenDimension>::init()
{
	// if(W!=0) delete W;
	W = new MatrixXd(hiddenDimension,inputDimension);//::Zero();
	gW = new MatrixXd(hiddenDimension,inputDimension);//::Zero();
	b = new Matrix<double,hiddenDimension,1>;//::Zero();
	gb = new Matrix<double,hiddenDimension,1>;//::Zero();
	gW->setZero();
	gb->setZero();
	std::random_device rd;
	std::mt19937 gen(rd());
	std::normal_distribution<double> initializer(0,1);
	for(int i = 0;i<hiddenDimension;i++)
	{	for(int j=0;j<inputDimension;j++)
			(*W)(i,j) = initializer(gen);
		(*b)(i,0)=initializer(gen);
	}
	std::cout << "LINEAR LAYER W" << std::endl;
	//std::cout << *W << std::endl;
	std::cout << std::endl;
}

template<int inputDimension,int hiddenDimension>
ComputeUnit* LinearLayer<inputDimension,hiddenDimension>::duplicate()
{
	LinearLayer<inputDimension,hiddenDimension>* l = new LinearLayer();
	l->W = W;
	l->b = b;
	l->gW = gW;
	l->gb = gb;
	return (ComputeUnit*)l;
}

template<int inputDimension,int hiddenDimension>
void LinearLayer<inputDimension,hiddenDimension>::forward()
{
	//std::cout << "linear layer "<<this << " " << inputs[0]->transpose()<<std::endl;
	outputs[0]= (*W) * (*inputs[0]) + *b;
	//std::cout << "linear layer "<<this << " " << outputs[0].transpose()<<std::endl;
}

template<int inputDimension,int hiddenDimension>
void LinearLayer<inputDimension,hiddenDimension>::backward()
{
	MatrixXd* jac = &successorDeep->jacobians[0];
	// std::cout << jac << std::endl;
	// std::cout << *jac << std::endl;
	// std::cout << this->W.transpose().rows() << "x" << this->W.transpose().cols() << std::endl;
	// std::cout << jac->rows() << "x" << jac->cols() << std::endl;
	jacobians[0] = this->W->transpose() * *jac;
// 	Matrix<double,inputDimension,hiddenDimension> jacobianW;
	// std::cout << "X" << inputs[0]->transpose() << std::endl;
	// std::cout << "J" << jac->transpose() << std::endl;
// 	for(int i=0;i<hiddenDimension;i++)
// 	{
// 		jacobianW.col(i) = *inputs[0];//->transpose();
// 	}
	//Matrix<double,inputDimension,1> delta = alpha * (*inputs[0] * jac->sum());//jacobianW * (*jac);
	for(int j=0;j<hiddenDimension;j++)
	{		
		//W->row(j)-= delta;
		// std::cout << "W"<<j << W->row(j) << std::endl;
		gW->row(j)+= ((*jac)(j,0)) * inputs[0]->transpose();
	}
	*gb += (*jac);


	// std::cout << W << std::endl;
}

template<int inputDimension,int hiddenDimension>
void LinearLayer<inputDimension,hiddenDimension>::gradientStep(double alpha)
{
	*W -= alpha * *gW;
	*b -= alpha * *gb;
	gb->setZero();
	gW->setZero();
}

// #endif
