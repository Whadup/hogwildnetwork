#pragma once
// #ifndef _L2LOSS_
// #define _L2LOSS_
#include "computeUnit.h"
#include <Eigen/Dense>
#include <iostream>
#include <math.h>
#include "dataInterfaces.h"
#include "loss.h"
using Eigen::Matrix;

template<int dimension>
class CrossEntropyLoss: public ComputeUnit, public IntegerLabel, public Loss
{
public:
	CrossEntropyLoss();
	double l;
	int ac;
	int pred;
	int ones = 0;
	virtual void forward();
	virtual void backwardGradientStep(double alpha);
	virtual ComputeUnit* duplicate();
	double loss();
	double acc();
};

template<int dimension>
CrossEntropyLoss<dimension>::CrossEntropyLoss() : ComputeUnit(1,0,0), IntegerLabel()
{
	jacobians[0]=Matrix<double,dimension,1>::Zero();
}

template<int dimension>
ComputeUnit* CrossEntropyLoss<dimension>::duplicate()
{
	return (ComputeUnit*) new CrossEntropyLoss();
}

template<int dimension>
void CrossEntropyLoss<dimension>::forward()
{
	
	if((*inputs[0])(y,0)<1e-5)
		(*inputs[0])(y,0)=1e-5;
	l = -log((*inputs[0])(y,0));
	if(l!=l)
	{
		std::cout << "NAN "<< l << " " << inputs[0]->transpose() << std::endl;
		((ComputeUnit*)0)->forward();
	}
	pred = 0;
	for(int i=1;i<dimension;i++)
	{
		if((*inputs[0])(i,0) > (*inputs[0])(pred,0))
			pred = i;
	}
	if(y) ones++;
	// std::cout << pred;
	// std::cout << y << std::endl;
	if(pred==y)
		ac = 1;
	else
		ac = 0;
}

template<int dimension>
double CrossEntropyLoss<dimension>::loss()
{
	return l;
}

template<int dimension>
double CrossEntropyLoss<dimension>::acc()
{
	return 1.0*ac;
}

template<int dimension>
void CrossEntropyLoss<dimension>::backwardGradientStep(double stepSize)
{
	jacobians[0]=Matrix<double,dimension,1>::Zero();
	jacobians[0](y,0)=-1/((*inputs[0])(y,0));
	// std::cout << jacobians[0] << std::endl;
	// std::cout << "loss layer "<<this << " " << jacobians[0].transpose()<<std::endl;
	//std::cout << jacobians[0].rows() << "x" << jacobians[0].cols() << std::endl;
	//jacobians[0]=Matrix<double,dimension,1>::Zero();
}

// #endif