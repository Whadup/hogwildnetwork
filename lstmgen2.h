
#pragma once
#include <iostream> 
#include <Eigen/Dense>
using Eigen::Matrix;
std::random_device rd;
std::mt19937 gen(rd());
std::normal_distribution<double> initializer(0,0.1);

#define gaussianInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = initializer(gen);
#define zeroInitialization(M) for(int _i=0;_i<M->rows();_i++) for(int _j=0;_j<M->cols();_j++) (*M)(_i,_j) = 0;

#include "computeUnit.h" 
template <int inputDimension, int outputDimension>
class LSTM: public ComputeUnit
{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Matrix<double,outputDimension,1> tmp1;
	Matrix<double,outputDimension,1> out;
	Matrix<double,outputDimension,1> i;
	Matrix<double,outputDimension,1> u;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> x;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> c;
	Matrix<double,outputDimension,1> f;
	Matrix<double,outputDimension,1> o;
	Matrix<double,Eigen::Dynamic,Eigen::Dynamic> h;
	Matrix<double,outputDimension,1> tmp3;
	Matrix<double,outputDimension,1> tmp2;
	Matrix<double,outputDimension,1> ht;
	Matrix<double,outputDimension,1> ct;
	Matrix<double,outputDimension,1>* bi = 0;
	Matrix<double,outputDimension,outputDimension>* Uih = 0;
	Matrix<double,outputDimension,inputDimension>* Ufx = 0;
	Matrix<double,outputDimension,outputDimension>* Uoct = 0;
	Matrix<double,outputDimension,1>* bo = 0;
	Matrix<double,outputDimension,outputDimension>* Uuc = 0;
	Matrix<double,outputDimension,inputDimension>* Uox = 0;
	Matrix<double,outputDimension,outputDimension>* Uoh = 0;
	Matrix<double,outputDimension,1>* bf = 0;
	Matrix<double,outputDimension,outputDimension>* Ufh = 0;
	Matrix<double,outputDimension,inputDimension>* Uux = 0;
	Matrix<double,outputDimension,inputDimension>* Uix = 0;
	Matrix<double,outputDimension,outputDimension>* Uic = 0;
	Matrix<double,outputDimension,outputDimension>* Ufc = 0;
	Matrix<double,outputDimension,1>* bu = 0;
	LSTM();
	virtual void init();
	virtual void forward();
	virtual void backwardGradientStep(double alpha);
	virtual ComputeUnit* duplicate();
		
};
			
template<int inputDimension, int outputDimension>
LSTM<inputDimension, outputDimension>::LSTM() : ComputeUnit(1,2,1) 
{
	outputs[0] = Matrix<double,outputDimension,1>::Zero();
	outputs[1] = Matrix<double,outputDimension,1>::Zero();
	outputs[2] = Matrix<double,outputDimension,1>::Zero();
	jacobians[0] = Matrix<double,inputDimension,1>::Zero();
	jacobians[1] = Matrix<double,outputDimension,1>::Zero();
	jacobians[2] = Matrix<double,outputDimension,1>::Zero();
}

template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::init() 
{
	bi = new Matrix<double,outputDimension,1>();
	zeroInitialization(bi);
	Uih = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uih);
	Ufx = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Ufx);
	Uoct = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uoct);
	bo = new Matrix<double,outputDimension,1>();
	zeroInitialization(bo);
	Uuc = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uuc);
	Uox = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uox);
	Uoh = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uoh);
	bf = new Matrix<double,outputDimension,1>();
	zeroInitialization(bf);
	Ufh = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Ufh);
	Uux = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uux);
	Uix = new Matrix<double,outputDimension,inputDimension>();
	gaussianInitialization(Uix);
	Uic = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Uic);
	Ufc = new Matrix<double,outputDimension,outputDimension>();
	gaussianInitialization(Ufc);
	bu = new Matrix<double,outputDimension,1>();
	zeroInitialization(bu);
}
template<int inputDimension, int outputDimension>
ComputeUnit* LSTM<inputDimension, outputDimension>::duplicate() 
{
	LSTM<inputDimension, outputDimension>* l = new LSTM<inputDimension, outputDimension>();
	l->bi = bi;
	l->Uih = Uih;
	l->Ufx = Ufx;
	l->Uoct = Uoct;
	l->bo = bo;
	l->Uuc = Uuc;
	l->Uox = Uox;
	l->Uoh = Uoh;
	l->bf = bf;
	l->Ufh = Ufh;
	l->Uux = Uux;
	l->Uix = Uix;
	l->Uic = Uic;
	l->Ufc = Ufc;
	l->bu = bu;
	return (ComputeUnit*) l;
}
template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::forward()
{
	x = *inputs[0];
	c = *inputs[1];
	h = *inputs[2];
	i = (1.0 + (-(((*Uix) * x) + ((*Uic) * c) + ((*Uih) * h) + (*bi))).array().exp()).inverse().matrix();;
	u = (((*Uux) * x) + ((*Uuc) * c) + (*bu)).array().tanh().matrix();
	f = (1.0 + (-(((*Ufx) * x) + ((*Ufc) * c) + ((*Ufh) * h) + (*bf))).array().exp()).inverse().matrix();;
	tmp2 = (i.array() * u.array()).matrix();
	tmp1 = (f.array() * c.array()).matrix();
	ct = (tmp1 + tmp2);
	outputs[1] = ct;
	tmp3 = (ct).array().tanh().matrix();
	o = (1.0 + (-(((*Uox) * x) + ((*Uoct) * ct) + ((*Uoh) * h) + (*bo))).array().exp()).inverse().matrix();;
	ht = (tmp3.array() * o.array()).matrix();
	outputs[2] = ht;
	outputs[0] = ht;
}
template<int inputDimension, int outputDimension>
void LSTM<inputDimension, outputDimension>::backwardGradientStep(double alpha)
{
	Matrix<double,outputDimension,1> Lout = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lht = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lo = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp3 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lct = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp1 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Ltmp2 = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lf = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lu = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Li = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lh = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,outputDimension,1> Lc = Matrix<double,outputDimension,1>::Zero();
	Matrix<double,inputDimension,1> Lx = Matrix<double,inputDimension,1>::Zero();
	if(successorDeep!=0) {Lout += this->successorDeep->jacobians[0];};
	if(successorTime!=0) {Lct += this->successorTime->jacobians[1];};
	if(successorTime!=0) {Lht += this->successorTime->jacobians[2];};
	Lht += Lout;
	Lo += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(tmp3) * Lht);
	Ltmp3 += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(o) * Lht);
	Lct += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -tmp3.array().cwiseAbs2().matrix())) * Ltmp3);
	Lct += ((*Uoct).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo);
	Ltmp1 += Lct;
	Ltmp2 += Lct;
	Lf += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(c) * Ltmp1);
	Lu += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(i) * Ltmp2);
	Li += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(u) * Ltmp2);
	Lh += ((*Uih).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lh += ((*Ufh).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf);
	Lh += ((*Uoh).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo);
	jacobians[2] = Lh;
	Lc += (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>(f) * Ltmp1);
	Lc += ((*Uic).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lc += ((*Uuc).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu);
	Lc += ((*Ufc).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf);
	jacobians[1] = Lc;
	Lx += ((*Uix).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li);
	Lx += ((*Uux).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu);
	Lx += ((*Ufx).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf);
	Lx += ((*Uox).transpose() * Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo);
	jacobians[0] = Lx;
	*Uix-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li * x.transpose());
	*Uic-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li * c.transpose());
	*Uih-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((i.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -i).array()).matrix()) * Li * h.transpose());
	*bi-= alpha * Li;
	*Uux-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu * x.transpose());
	*Uuc-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((Matrix<double,outputDimension,1>::Constant(1) +  -u.array().cwiseAbs2().matrix())) * Lu * c.transpose());
	*bu-= alpha * Lu;
	*Ufx-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf * x.transpose());
	*Ufc-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf * c.transpose());
	*Ufh-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((f.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -f).array()).matrix()) * Lf * h.transpose());
	*bf-= alpha * Lf;
	*Uox-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo * x.transpose());
	*Uoct-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo * ct.transpose());
	*Uoh-= alpha * (Eigen::DiagonalMatrix<double,outputDimension,outputDimension>((o.array() * (Matrix<double,outputDimension,1>::Constant(1) +  -o).array()).matrix()) * Lo * h.transpose());
	*bo-= alpha * Lo;
}
